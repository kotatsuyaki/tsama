struct CameraUniform {
    view_proj: mat4x4<f32>,
};

struct PivotUniform {
    point: vec2<i32>,
};

struct BufferOriginUniform {
    point: vec2<i32>,
};

@group(0) @binding(0)
var<uniform> camera: CameraUniform;

@group(1) @binding(0)
var<uniform> pivot: PivotUniform;

@group(2) @binding(0)
var<uniform> buffer_origin: BufferOriginUniform;

@group(3) @binding(0)
var merged_texture: texture_2d<f32>;

@group(3) @binding(1)
var merged_sampler: sampler;

struct VertexInput {
    @location(0) position: vec3<f32>,
    @location(1) tint: vec3<f32>,
    @location(2) tex_coords: vec2<f32>,
};

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) tint: vec3<f32>,
    @location(1) tex_coords: vec2<f32>,
};

@vertex
fn main_vertex(
    in: VertexInput,
) -> VertexOutput {
    var out: VertexOutput;
    let offset = buffer_origin.point - pivot.point;
    let offsetted_position = vec3<f32>(f32(offset.x), 0.0, f32(offset.y)) + in.position;
    out.clip_position = camera.view_proj * vec4<f32>(offsetted_position, 1.0);
    out.tint = in.tint * 0.5;
    out.tex_coords = in.tex_coords;
    return out;
}

@fragment
fn main_fragment(in: VertexOutput) -> @location(0) vec4<f32> {
    return textureSample(merged_texture, merged_sampler, in.tex_coords) * vec4<f32>(in.tint, 1.0);
}
