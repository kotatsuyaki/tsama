struct ViewportUniform {
    size: vec2<i32>,
};

@group(0) @binding(0)
var<uniform> viewport: ViewportUniform;

@group(1) @binding(0)
var merged_texture: texture_2d<f32>;

@group(1) @binding(1)
var merged_sampler: sampler;

struct VertexInput {
    @location(0) position: vec3<i32>,
    @location(1) tint: vec3<f32>,
    @location(2) tex_coords: vec2<f32>,
};

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) tint: vec3<f32>,
    @location(1) tex_coords: vec2<f32>,
};

@vertex
fn main_vertex(
    in: VertexInput,
) -> VertexOutput {
    var out: VertexOutput;
    var box_size = vec3<f32>(vec2<f32>(viewport.size), 100.0) * 0.5;
    var position = vec3<f32>(in.position);
    out.clip_position = vec4<f32>((position / box_size) + vec3<f32>(-1.0, 1.0, 0.0), 1.0);
    out.tint = in.tint;
    out.tex_coords = in.tex_coords;
    return out;
}

@fragment
fn main_fragment(in: VertexOutput) -> @location(0) vec4<f32> {
    return textureSample(merged_texture, merged_sampler, in.tex_coords) * vec4<f32>(in.tint, 1.0);
}
