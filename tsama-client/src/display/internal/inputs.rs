use tracing::warn;
use winit::{
    event::{ElementState, KeyEvent, MouseButton},
    keyboard::KeyCode,
};

use crate::display::InputSource;

use super::Inputs;
use std::collections::HashSet;

impl Inputs {
    pub(super) fn new() -> Self {
        Self {
            presesd_keys: HashSet::new(),
            last_pressed_keys: HashSet::new(),
            pressed_mouse_buttons: HashSet::new(),
            last_pressed_mouse_buttons: HashSet::new(),
            cursor_delta: (0.0, 0.0),
        }
    }

    pub(super) fn process_key_event(&mut self, event: &KeyEvent) {
        let winit::keyboard::PhysicalKey::Code(key) = event.physical_key else {
            warn!("Unknown physical key {:?}", event.physical_key);
            return;
        };

        match event.state {
            ElementState::Pressed => self.presesd_keys.insert(key),
            ElementState::Released => self.presesd_keys.remove(&key),
        };
    }

    pub(super) fn process_mouse_event(&mut self, button: MouseButton, state: ElementState) {
        match state {
            ElementState::Pressed => self.pressed_mouse_buttons.insert(button),
            ElementState::Released => self.pressed_mouse_buttons.remove(&button),
        };
    }

    pub(super) fn add_cursor_delta(&mut self, delta: (f64, f64)) {
        self.cursor_delta = (self.cursor_delta.0 + delta.0, self.cursor_delta.1 + delta.1);
    }
}

impl InputSource for Inputs {
    fn is_key_held(&self, key: KeyCode) -> bool {
        self.presesd_keys.contains(&key)
    }

    fn is_key_down(&self, key: KeyCode) -> bool {
        self.presesd_keys.contains(&key) && self.last_pressed_keys.contains(&key) == false
    }

    fn is_mouse_held(&self, button: MouseButton) -> bool {
        self.pressed_mouse_buttons.contains(&button)
    }

    fn is_mouse_down(&self, button: MouseButton) -> bool {
        self.pressed_mouse_buttons.contains(&button) == true
            && self.last_pressed_mouse_buttons.contains(&button) == false
    }

    fn cursor_delta(&self) -> (f64, f64) {
        self.cursor_delta
    }

    fn clear_key_downs(&mut self) {
        self.last_pressed_keys = self.presesd_keys.clone();
        self.last_pressed_mouse_buttons = self.pressed_mouse_buttons.clone();
    }

    fn clear_cursor_delta(&mut self) {
        self.cursor_delta = (0.0, 0.0);
    }
}
