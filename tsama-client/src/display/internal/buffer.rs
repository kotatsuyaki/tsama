use std::cmp::Reverse;

use super::{
    FrameVertex, NormQuad, Quad, RenderedBlockBuffer, RenderedEntityBuffer,
    RenderedEntityDebugBuffer, RenderedUiBuffer, UiQuad,
};
use glam::{ivec2, IVec2, Vec2Swizzles, Vec3, Vec3Swizzles};
use ordered_float::OrderedFloat;
use tracing::info_span;
use wgpu::util::DeviceExt;

pub struct EntityDebugVertexBuffer {
    pub(super) vertices: Vec<FrameVertex>,
    pub(super) hardware_buffer: Option<wgpu::Buffer>,
}

impl EntityDebugVertexBuffer {
    pub(super) fn new() -> Self {
        Self {
            vertices: Vec::new(),
            hardware_buffer: None,
        }
    }

    pub(super) fn write_vertex_buffer(&mut self, device: &wgpu::Device) {
        if self.hardware_buffer.is_none() {
            let hardware_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: None,
                contents: bytemuck::cast_slice(&self.vertices),
                usage: wgpu::BufferUsages::VERTEX,
            });
            self.hardware_buffer = Some(hardware_buffer);
        }
    }

    pub(super) fn hardware_buffer_slice(&self) -> Option<wgpu::BufferSlice<'_>> {
        self.hardware_buffer.as_ref().map(|buf| buf.slice(..))
    }
}

impl RenderedEntityDebugBuffer for EntityDebugVertexBuffer {
    fn add_vertex(&mut self, position: Vec3) {
        self.hardware_buffer = None;
        let vertex = FrameVertex { position };
        self.vertices.push(vertex);
    }
}

pub struct EntityVertexBuffer {
    pub(super) quads: Vec<NormQuad>,
    hardware_buffer: Option<wgpu::Buffer>,
}

impl EntityVertexBuffer {
    pub(super) fn new() -> Self {
        Self {
            quads: Vec::new(),
            hardware_buffer: None,
        }
    }

    pub(super) fn write_vertex_buffer(&mut self, device: &wgpu::Device) {
        if self.hardware_buffer.is_none() {
            let hardware_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: None,
                contents: bytemuck::cast_slice(&self.quads),
                usage: wgpu::BufferUsages::VERTEX,
            });
            self.hardware_buffer = Some(hardware_buffer);
        }
    }

    pub(super) fn hardware_buffer_slice(&self) -> Option<wgpu::BufferSlice<'_>> {
        self.hardware_buffer.as_ref().map(|buf| buf.slice(..))
    }
}

impl RenderedEntityBuffer for EntityVertexBuffer {
    type QuadType = NormQuad;

    fn add_quad(&mut self, quad: Self::QuadType) {
        self.quads.push(quad);
    }
}

pub struct BlockVertexBuffer {
    pub(super) quads: Vec<Quad>,
    sorted_from: Option<Vec3>,
    /// The origin point of this buffer.  Unit: blocks.
    origin: IVec2,
    hardware_buffer: Option<wgpu::Buffer>,
}

impl BlockVertexBuffer {
    pub(super) fn new(origin: IVec2) -> Self {
        Self {
            quads: Vec::new(),
            sorted_from: None,
            origin,
            hardware_buffer: None,
        }
    }

    pub(super) fn write_vertex_buffer(&mut self, device: &wgpu::Device) {
        if self.hardware_buffer.is_none() {
            let span = info_span!("render_display_bvb_upload");
            let _enter = span.enter();

            let hardware_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: None,
                contents: bytemuck::cast_slice(&self.quads),
                usage: wgpu::BufferUsages::VERTEX,
            });
            self.hardware_buffer = Some(hardware_buffer);
        }
    }

    pub(super) fn hardware_buffer_slice(&self) -> Option<wgpu::BufferSlice<'_>> {
        self.hardware_buffer.as_ref().map(|buf| buf.slice(..))
    }

    pub fn unwrapped_origin(&self, eye: IVec2) -> IVec2 {
        let r = 64 * 16;
        let origin = self.origin;
        let x = [origin.x - 2 * r, origin.x, origin.x + 2 * r]
            .into_iter()
            .min_by_key(|&x| (eye.x - x - r / 2).abs())
            .unwrap();
        let y = [origin.y - 2 * r, origin.y, origin.y + 2 * r]
            .into_iter()
            .min_by_key(|&y| (eye.y - y - r / 2).abs())
            .unwrap();
        ivec2(x, y)
    }

    #[allow(dead_code)]
    pub fn origin(&self) -> IVec2 {
        self.origin
    }

    pub(super) fn should_re_sort_from(&self, eye: Vec3) -> bool {
        self.sorted_from.is_none()
            || self
                .sorted_from
                .is_some_and(|last| last.distance_squared(eye) >= 1.0)
    }

    pub(super) fn re_sort_from(&mut self, eye: Vec3) {
        // Sort the quads by the distance between the eye and the _unwrapped_ center of the quad.
        // Further quads get sorted to the beginning of `self.quads`.
        //
        // Unit for all calculations: blocks.
        let origin = self
            .unwrapped_origin(eye.xz().as_ivec2())
            .xxy()
            .with_y(0)
            .as_vec3();
        self.quads.sort_unstable_by_key(|quad| {
            Reverse(OrderedFloat((origin + quad.center()).distance_squared(eye)))
        });
        self.hardware_buffer = None;
        self.sorted_from = Some(eye);
    }

    pub(super) fn is_empty(&self) -> bool {
        self.quads.is_empty()
    }
}

impl RenderedBlockBuffer for BlockVertexBuffer {
    type QuadType = Quad;

    fn add_quad(&mut self, quad: Self::QuadType) {
        self.quads.push(quad);
    }
}

pub struct UiVertexBuffer {
    pub(super) quads: Vec<UiQuad>,
    pub(super) hardware_buffer: Option<wgpu::Buffer>,
}

impl UiVertexBuffer {
    pub(super) fn new() -> Self {
        Self {
            quads: Vec::new(),
            hardware_buffer: None,
        }
    }

    pub(super) fn write_vertex_buffer(&mut self, device: &wgpu::Device) {
        if self.hardware_buffer.is_none() {
            let span = info_span!("render_display_uvb_upload");
            let _enter = span.enter();

            let hardware_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: None,
                contents: bytemuck::cast_slice(&self.quads),
                usage: wgpu::BufferUsages::VERTEX,
            });
            self.hardware_buffer = Some(hardware_buffer);
        }
    }

    pub(super) fn hardware_buffer_slice(&self) -> Option<wgpu::BufferSlice<'_>> {
        self.hardware_buffer.as_ref().map(|buf| buf.slice(..))
    }
}

impl RenderedUiBuffer for UiVertexBuffer {
    type QuadType = UiQuad;

    fn add_quad(&mut self, quad: Self::QuadType) {
        self.quads.push(quad)
    }
}
