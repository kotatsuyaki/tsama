use bytemuck::{Pod, Zeroable};
use glam::{dvec3, ivec2, ivec3, vec3, DVec3, IVec2, IVec3, Mat4, Vec3, Vec3Swizzles};
use tsama_core::util::Vec3Ext;

use super::{CameraConfig, UniformBuffer};

pub(super) struct Camera {
    // values supplied from the game logic
    config: CameraConfig,

    // supplied from the window
    aspect_ratio: f32,

    // gpu buffer
    uniform_buffer: UniformBuffer,
    uniform_buffer_with_pivot: UniformBuffer,
}

#[repr(C)]
#[derive(Debug, Clone, Copy, Pod, Zeroable)]
struct CameraUniform {
    view_proj: Mat4,
}

impl Camera {
    const INITIAL_ASPECT_RATIO: f32 = 800.0 / 600.0;

    pub(super) fn new(device: &wgpu::Device, config: CameraConfig) -> Self {
        let aspect_ratio = Self::INITIAL_ASPECT_RATIO;

        let uniform = config.uniform(aspect_ratio);
        let uniform_with_pivot = config.uniform_with_pivot(aspect_ratio);
        let uniform_buffer = UniformBuffer::new(device, uniform, wgpu::ShaderStages::VERTEX);
        let uniform_buffer_with_pivot =
            UniformBuffer::new(device, uniform_with_pivot, wgpu::ShaderStages::VERTEX);

        Self {
            config,
            aspect_ratio,
            uniform_buffer,
            uniform_buffer_with_pivot,
        }
    }

    pub(super) fn set_config(&mut self, config: CameraConfig) {
        self.config = config;
    }

    pub(super) fn set_aspect_ratio(&mut self, aspect_ratio: f32) {
        self.aspect_ratio = aspect_ratio;
    }

    pub(super) fn write_uniform_buffers(&self, queue: &wgpu::Queue) {
        let uniform = self.config.uniform(self.aspect_ratio);
        let uniform_with_pivot = self.config.uniform_with_pivot(self.aspect_ratio);
        self.uniform_buffer.write(queue, 0, uniform);
        self.uniform_buffer_with_pivot
            .write(queue, 0, uniform_with_pivot);
    }

    pub(super) fn bind_group(&self) -> &wgpu::BindGroup {
        self.uniform_buffer.bind_group()
    }

    pub(super) fn bind_group_with_pivot(&self) -> &wgpu::BindGroup {
        self.uniform_buffer_with_pivot.bind_group()
    }

    pub(super) fn bind_group_layout(&self) -> &wgpu::BindGroupLayout {
        self.uniform_buffer.bind_group_layout()
    }

    pub(super) fn config(&self) -> &CameraConfig {
        &self.config
    }

    pub(super) fn view_proj_with_pivot_point(&self) -> Mat4 {
        self.config.view_proj_with_pivot_point(self.aspect_ratio)
    }
}

impl Default for CameraConfig {
    fn default() -> Self {
        Self::new(dvec3(0.0, 0.0, 0.0), 0.0, 0.0)
    }
}

impl CameraUniform {
    fn new(view_proj: Mat4) -> CameraUniform {
        Self { view_proj }
    }
}

impl CameraConfig {
    const UP: Vec3 = vec3(0.0, 1.0, 0.0);

    /// Creates a new [`CameraConfig`] with the specified eye position and rotations.
    ///
    /// The `pitch` value should be between `0.0` (looking upwards) and `std::f32::consts::PI`
    /// (looking downwards) inclusively.
    /// The `yaw` value should be between `0.0` (no rotation, inclusive) and `2 * std::f32::consts::PI`
    /// (no rotation, exclusive).
    pub fn new(eye: DVec3, pitch: f32, yaw: f32) -> Self {
        Self { eye, pitch, yaw }
    }

    fn uniform_with_pivot(&self, aspect_ratio: f32) -> CameraUniform {
        CameraUniform::new(self.view_proj_with_pivot_point(aspect_ratio))
    }

    fn uniform(&self, aspect_ratio: f32) -> CameraUniform {
        CameraUniform::new(self.view_proj(aspect_ratio))
    }

    /// Returns the view-then-project matrix computed from the stored eye, pitch, and yaw values,
    /// with the eye position relative to the pivot point.
    ///
    /// The `aspect_ratio` is defined as the width divided by the height of the viewport.
    /// For a typical 800x600 viewport, the value should be `800.0 / 600.0`.
    ///
    /// This is intended to be used in cases where the vertices (plus its buffer origin if exists)
    /// are relative to the pivot point.
    fn view_proj_with_pivot_point(&self, aspect_ratio: f32) -> Mat4 {
        let eye_local = self.eye - self.pivot_point_ivec3().as_dvec3();
        self.view_proj_with_eye_at(eye_local, aspect_ratio)
    }

    /// Returns the view-then-project matrix computed from the stored pitch and yaw values, while
    /// leaving the eye position at the origin.
    ///
    /// This is intended to be used in cases where the vertices are at positions relative to the
    /// eye already.
    fn view_proj(&self, aspect_ratio: f32) -> Mat4 {
        self.view_proj_with_eye_at(dvec3(0.0, 0.0, 0.0), aspect_ratio)
    }

    fn view_proj_with_eye_at(&self, eye: DVec3, aspect_ratio: f32) -> Mat4 {
        let up = if approx::ulps_eq!(self.pitch, std::f32::consts::PI) {
            // Fix the gimbal lock issue when looking downwards by using another up vector
            Self::UP.apply_rotation(std::f32::consts::FRAC_PI_2, self.yaw)
        } else {
            Self::UP
        };

        let front = Self::UP.apply_rotation(self.pitch, self.yaw);
        let view = Mat4::look_to_lh(eye.as_vec3(), front, up);
        let proj = Mat4::perspective_lh(70.0_f32.to_radians(), aspect_ratio, 0.1, 1000.0);

        proj * view
    }

    fn pivot_point_ivec3(&self) -> IVec3 {
        let IVec2 { x, y: z } = self.pivot_point();
        ivec3(x, 0, z)
    }

    pub(super) fn pivot_point(&self) -> IVec2 {
        self.eye
            .xz()
            .floor()
            .as_ivec2()
            .div_euclid(ivec2(1024, 1024))
            * ivec2(1024, 1024)
    }
}
