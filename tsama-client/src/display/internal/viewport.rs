use glam::IVec2;

use super::UniformBuffer;

pub(super) struct Viewport {
    uniform_buffer: UniformBuffer,
}

impl Viewport {
    pub(super) fn new(device: &wgpu::Device, size: IVec2) -> Self {
        let uniform_buffer = UniformBuffer::new(device, size, wgpu::ShaderStages::VERTEX);

        Self { uniform_buffer }
    }

    pub(super) fn bind_group(&self) -> &wgpu::BindGroup {
        self.uniform_buffer.bind_group()
    }

    pub(super) fn bind_group_layout(&self) -> &wgpu::BindGroupLayout {
        self.uniform_buffer.bind_group_layout()
    }
}
