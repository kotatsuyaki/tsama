use bytemuck::Pod;
use wgpu::util::DeviceExt;

pub(super) struct UniformBuffer {
    buffer: wgpu::Buffer,
    bind_group: wgpu::BindGroup,
    bind_group_layout: wgpu::BindGroupLayout,
}

impl UniformBuffer {
    pub(super) fn new<ValueType: Pod>(
        device: &wgpu::Device,
        value: ValueType,
        visibility: wgpu::ShaderStages,
    ) -> Self {
        const HAS_DYNAMIC_OFFSET: bool = false;

        let buffer = Self::create_buffer_init(device, value);
        let bind_group_layout =
            Self::create_bind_group_layout(device, visibility, HAS_DYNAMIC_OFFSET);
        let bind_group =
            Self::create_bind_group(device, buffer.as_entire_binding(), &bind_group_layout);

        Self {
            buffer,
            bind_group,
            bind_group_layout,
        }
    }

    pub(super) fn new_with_dynamic_offset<ValueType: Pod>(
        device: &wgpu::Device,
        count: u64,
        visibility: wgpu::ShaderStages,
    ) -> Self {
        const HAS_DYNAMIC_OFFSET: bool = true;

        let buffer = Self::create_dynamic_offset_buffer::<ValueType>(device, count);
        let bind_group_layout =
            Self::create_bind_group_layout(device, visibility, HAS_DYNAMIC_OFFSET);
        let bind_group = Self::create_bind_group(
            device,
            wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                buffer: &buffer,
                offset: 0,
                size: wgpu::BufferSize::new(std::mem::size_of::<ValueType>() as u64),
            }),
            &bind_group_layout,
        );

        Self {
            buffer,
            bind_group,
            bind_group_layout,
        }
    }

    pub(super) fn write<ValueType: Pod>(&self, queue: &wgpu::Queue, offset: u64, value: ValueType) {
        queue.write_buffer(&self.buffer, offset, bytemuck::cast_slice(&[value]));
    }

    pub(super) fn bind_group(&self) -> &wgpu::BindGroup {
        &self.bind_group
    }

    pub(super) fn bind_group_layout(&self) -> &wgpu::BindGroupLayout {
        &self.bind_group_layout
    }

    fn create_buffer_init<ValueType: Pod>(device: &wgpu::Device, value: ValueType) -> wgpu::Buffer {
        device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: None,
            contents: bytemuck::cast_slice(&[value]),
            usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
        })
    }

    fn create_dynamic_offset_buffer<ValueType: Pod>(
        device: &wgpu::Device,
        count: u64,
    ) -> wgpu::Buffer {
        // Round the size up to the uniform offset alignment.
        let size = (std::mem::size_of::<ValueType>().div_euclid(256) + 1) * 256;
        device.create_buffer(&wgpu::BufferDescriptor {
            label: None,
            size: size as u64 * count,
            usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
            mapped_at_creation: false,
        })
    }

    fn create_bind_group_layout(
        device: &wgpu::Device,
        visibility: wgpu::ShaderStages,
        has_dynamic_offset: bool,
    ) -> wgpu::BindGroupLayout {
        device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            label: None,
            entries: &[wgpu::BindGroupLayoutEntry {
                binding: 0,
                visibility,
                ty: wgpu::BindingType::Buffer {
                    ty: wgpu::BufferBindingType::Uniform,
                    has_dynamic_offset,
                    min_binding_size: None,
                },
                count: None,
            }],
        })
    }

    fn create_bind_group(
        device: &wgpu::Device,
        resource: wgpu::BindingResource,
        layout: &wgpu::BindGroupLayout,
    ) -> wgpu::BindGroup {
        device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: None,
            layout,
            entries: &[wgpu::BindGroupEntry {
                binding: 0,
                resource,
            }],
        })
    }
}
