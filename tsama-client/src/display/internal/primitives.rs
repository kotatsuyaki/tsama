use bytemuck::{Pod, Zeroable};
use glam::{IVec3, Vec2, Vec3};

use super::{
    RenderedBlockQuad, RenderedBlockVertex, RenderedEntityQuad, RenderedEntityVertex,
    RenderedUiQuad, RenderedUiVertex,
};

#[repr(C)]
#[derive(Copy, Clone, Debug, Pod, Zeroable)]
pub struct Vertex {
    position: Vec3,
    tint: Vec3,
    tex_coords: Vec2,
}

#[repr(C)]
#[derive(Copy, Clone, Debug, Pod, Zeroable)]
pub struct NormVertex {
    position: Vec3,
    norm: Vec3,
    tint: Vec3,
    tex_coords: Vec2,
}

#[repr(C)]
#[derive(Copy, Clone, Debug, Pod, Zeroable)]
pub struct UiVertex {
    position: IVec3,
    tint: Vec3,
    tex_coords: Vec2,
}

impl Vertex {
    const ATTRIBUTES: [wgpu::VertexAttribute; 3] =
        wgpu::vertex_attr_array![0 => Float32x3, 1 => Float32x3, 2 => Float32x2];

    pub(super) const fn buffer_layout() -> wgpu::VertexBufferLayout<'static> {
        wgpu::VertexBufferLayout {
            array_stride: std::mem::size_of::<Self>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &Self::ATTRIBUTES,
        }
    }
}

impl NormVertex {
    const ATTRIBUTES: [wgpu::VertexAttribute; 4] =
        wgpu::vertex_attr_array![0 => Float32x3, 1 => Float32x3, 2 => Float32x3, 3 => Float32x2];

    pub(super) const fn buffer_layout() -> wgpu::VertexBufferLayout<'static> {
        wgpu::VertexBufferLayout {
            array_stride: std::mem::size_of::<Self>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &Self::ATTRIBUTES,
        }
    }
}

impl UiVertex {
    const ATTRIBUTES: [wgpu::VertexAttribute; 3] =
        wgpu::vertex_attr_array![0 => Sint32x3, 1 => Float32x3, 2 => Float32x2];

    pub(super) const fn buffer_layout() -> wgpu::VertexBufferLayout<'static> {
        wgpu::VertexBufferLayout {
            array_stride: std::mem::size_of::<Self>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &Self::ATTRIBUTES,
        }
    }
}

#[repr(C)]
#[derive(Copy, Clone, Debug, Pod, Zeroable)]
pub struct Quad {
    vertices: [Vertex; 6],
}

#[repr(C)]
#[derive(Copy, Clone, Debug, Pod, Zeroable)]
pub struct NormQuad {
    vertices: [NormVertex; 6],
}

#[repr(C)]
#[derive(Copy, Clone, Debug, Pod, Zeroable)]
pub struct UiQuad {
    vertices: [UiVertex; 6],
}

impl RenderedBlockVertex for Vertex {
    fn new(position: Vec3, tint: Vec3, tex_coords: Vec2) -> Self {
        Self {
            position,
            tint,
            tex_coords,
        }
    }
}

impl RenderedBlockQuad for Quad {
    type VertexType = Vertex;

    fn new(vertices: [Self::VertexType; 4]) -> Self {
        Self {
            vertices: [
                vertices[0],
                vertices[1],
                vertices[2],
                vertices[2],
                vertices[3],
                vertices[0],
            ],
        }
    }
}

impl RenderedEntityVertex for NormVertex {
    fn new(position: Vec3, norm: Vec3, tint: Vec3, tex_coords: Vec2) -> Self {
        Self {
            position,
            norm,
            tint,
            tex_coords,
        }
    }
}

impl RenderedEntityQuad for NormQuad {
    type VertexType = NormVertex;

    fn new(vertices: &[Self::VertexType; 4]) -> Self {
        Self {
            vertices: [
                vertices[0],
                vertices[1],
                vertices[2],
                vertices[2],
                vertices[3],
                vertices[0],
            ],
        }
    }
}

impl RenderedUiQuad for UiQuad {
    type VertexType = UiVertex;

    fn new(vertices: &[Self::VertexType; 4]) -> Self {
        Self {
            vertices: [
                vertices[0],
                vertices[1],
                vertices[2],
                vertices[2],
                vertices[3],
                vertices[0],
            ],
        }
    }
}

impl RenderedUiVertex for UiVertex {
    fn new(position: IVec3, tint: Vec3, tex_coords: Vec2) -> Self {
        Self {
            position,
            tint,
            tex_coords,
        }
    }
}

impl Quad {
    pub(super) fn center(&self) -> Vec3 {
        let a = self.vertices[0].position;
        let b = self.vertices[1].position;
        let c = self.vertices[2].position;
        let d = self.vertices[4].position;
        (a + b + c + d) * 0.25
    }
}

#[repr(C)]
#[derive(Copy, Clone, Debug, Pod, Zeroable)]
pub(super) struct FrameVertex {
    pub(super) position: Vec3,
}

impl FrameVertex {
    const ATTRIBUTES: [wgpu::VertexAttribute; 1] = wgpu::vertex_attr_array![0 => Float32x3];

    pub(super) const fn buffer_layout() -> wgpu::VertexBufferLayout<'static> {
        wgpu::VertexBufferLayout {
            array_stride: std::mem::size_of::<Self>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &Self::ATTRIBUTES,
        }
    }
}
