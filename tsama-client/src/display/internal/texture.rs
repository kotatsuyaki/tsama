use anyhow::Result;
use glam::ivec2;
use glam::IVec2;
use tracing::info;

use super::TextureRect;

/// A texture without an associated bind group.
pub(super) struct BasicTexture {
    pub(super) texture: wgpu::Texture,
    pub(super) view: wgpu::TextureView,
    #[allow(dead_code)]
    pub(super) sampler: wgpu::Sampler,
}

pub(super) struct BindableTexture {
    pub(super) texture: BasicTexture,
    pub(super) bind_group: wgpu::BindGroup,
    pub(super) bind_group_layout: wgpu::BindGroupLayout,
}

pub(super) struct TextureAtlas {
    pub(super) canvas: image::RgbaImage,
    pub(super) size: IVec2,
    pub(super) next_origin: IVec2,
    pub(super) row_height: i32,
}

impl TextureAtlas {
    pub(super) fn new(size: i32) -> Self {
        let canvas = image::ImageBuffer::new(size as u32, size as u32);
        Self {
            canvas,
            size: ivec2(size, size),
            next_origin: ivec2(0, 0),
            row_height: 0,
        }
    }

    /// # Panics
    ///
    /// The function panics if `data` isn't a valid image.
    pub(super) fn add_texture(&mut self, data: &[u8], expected_size: IVec2) -> TextureRect {
        let (texture_rect, new_texture_size) = self.add_texture_with_unknown_size(data);
        assert_eq!(expected_size, new_texture_size);
        texture_rect
    }

    pub(super) fn add_texture_with_unknown_size(&mut self, data: &[u8]) -> (TextureRect, IVec2) {
        let new_texture_image = image::load_from_memory(data)
            .expect("Failed to load texture from memory")
            .into_rgba8();
        let new_texture_size = {
            let (w, h) = new_texture_image.dimensions();
            ivec2(w as i32, h as i32)
        };

        if self.next_origin.x + new_texture_size.x <= self.size.x {
            // The texture fits horizontally, so there's no need to move to the next line.
        } else {
            // The texture does not fit horizontally.  Move to the next line.
            self.next_origin = ivec2(0, self.next_origin.y + self.row_height);
            self.row_height = 0;
            info!("next_origin = {:?}", self.next_origin);
        }

        image::imageops::overlay(
            &mut self.canvas,
            &new_texture_image,
            self.next_origin.x as i64,
            self.next_origin.y as i64,
        );
        let texture_rect = TextureRect {
            min: self.next_origin.as_vec2() / self.size.as_vec2(),
            size: new_texture_size.as_vec2() / self.size.as_vec2(),
        };

        self.next_origin.x += new_texture_size.x;
        self.row_height = self.row_height.max(new_texture_size.y);

        (texture_rect, new_texture_size)
    }
}

impl BindableTexture {
    pub(super) fn from_size(
        device: &wgpu::Device,
        size: wgpu::Extent3d,
        label: Option<&str>,
    ) -> Result<Self> {
        let texture = BasicTexture::from_size(device, size, label)?;

        let bind_group_layout =
            Self::create_bind_group_layout(device, wgpu::TextureViewDimension::D2);

        let texture_view_resource = wgpu::BindingResource::TextureView(&texture.view);
        let sampler_resource = wgpu::BindingResource::Sampler(&texture.sampler);
        let bind_group = Self::create_bind_group(
            device,
            texture_view_resource,
            sampler_resource,
            &bind_group_layout,
        );

        Ok(Self {
            texture,
            bind_group,
            bind_group_layout,
        })
    }

    pub(super) fn write_texture(&self, queue: &wgpu::Queue, img: &image::RgbaImage) {
        self.texture.write_texture(queue, img);
    }

    pub(super) fn create_bind_group(
        device: &wgpu::Device,
        texture_view_resource: wgpu::BindingResource,
        sampler_resource: wgpu::BindingResource,
        layout: &wgpu::BindGroupLayout,
    ) -> wgpu::BindGroup {
        device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: None,
            layout,
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: texture_view_resource,
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: sampler_resource,
                },
            ],
        })
    }

    fn create_bind_group_layout(
        device: &wgpu::Device,
        view_dimension: wgpu::TextureViewDimension,
    ) -> wgpu::BindGroupLayout {
        device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            label: None,
            entries: &[
                wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Texture {
                        sample_type: wgpu::TextureSampleType::Float { filterable: true },
                        view_dimension,
                        multisampled: false,
                    },
                    count: None,
                },
                wgpu::BindGroupLayoutEntry {
                    binding: 1,
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Sampler(wgpu::SamplerBindingType::Filtering),
                    count: None,
                },
            ],
        })
    }

    pub(super) fn bind_group(&self) -> &wgpu::BindGroup {
        &self.bind_group
    }

    pub(super) fn bind_group_layout(&self) -> &wgpu::BindGroupLayout {
        &self.bind_group_layout
    }
}

impl BasicTexture {
    pub(super) const DEPTH_FORMAT: wgpu::TextureFormat = wgpu::TextureFormat::Depth32Float;

    pub(super) fn format(&self) -> wgpu::TextureFormat {
        self.texture.format()
    }

    pub(super) fn view(&self) -> &wgpu::TextureView {
        &self.view
    }

    pub(super) fn from_size(
        device: &wgpu::Device,
        size: wgpu::Extent3d,
        label: Option<&str>,
    ) -> Result<Self> {
        let texture = device.create_texture(&wgpu::TextureDescriptor {
            label,
            size,
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: wgpu::TextureFormat::Rgba8UnormSrgb,
            usage: wgpu::TextureUsages::TEXTURE_BINDING | wgpu::TextureUsages::COPY_DST,
            view_formats: &[],
        });

        let view = texture.create_view(&wgpu::TextureViewDescriptor::default());
        let sampler = device.create_sampler(&wgpu::SamplerDescriptor {
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Nearest,
            min_filter: wgpu::FilterMode::Nearest,
            mipmap_filter: wgpu::FilterMode::Nearest,
            ..Default::default()
        });

        Ok(Self {
            texture,
            view,
            sampler,
        })
    }

    pub(super) fn write_texture(&self, queue: &wgpu::Queue, img: &image::RgbaImage) {
        let dimensions = img.dimensions();

        let size = wgpu::Extent3d {
            width: dimensions.0,
            height: dimensions.1,
            depth_or_array_layers: 1,
        };

        queue.write_texture(
            wgpu::ImageCopyTexture {
                aspect: wgpu::TextureAspect::All,
                texture: &self.texture,
                mip_level: 0,
                origin: wgpu::Origin3d::ZERO,
            },
            img,
            wgpu::ImageDataLayout {
                offset: 0,
                bytes_per_row: Some(4 * dimensions.0),
                rows_per_image: Some(dimensions.1),
            },
            size,
        );
    }

    #[allow(dead_code)]
    pub(super) fn from_image(
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        img: &image::RgbaImage,
        label: Option<&str>,
    ) -> Result<Self> {
        let dimensions = img.dimensions();

        let size = wgpu::Extent3d {
            width: dimensions.0,
            height: dimensions.1,
            depth_or_array_layers: 1,
        };
        let texture = device.create_texture(&wgpu::TextureDescriptor {
            label,
            size,
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: wgpu::TextureFormat::Rgba8UnormSrgb,
            usage: wgpu::TextureUsages::TEXTURE_BINDING | wgpu::TextureUsages::COPY_DST,
            view_formats: &[],
        });

        queue.write_texture(
            wgpu::ImageCopyTexture {
                aspect: wgpu::TextureAspect::All,
                texture: &texture,
                mip_level: 0,
                origin: wgpu::Origin3d::ZERO,
            },
            img,
            wgpu::ImageDataLayout {
                offset: 0,
                bytes_per_row: Some(4 * dimensions.0),
                rows_per_image: Some(dimensions.1),
            },
            size,
        );

        let view = texture.create_view(&wgpu::TextureViewDescriptor::default());
        let sampler = device.create_sampler(&wgpu::SamplerDescriptor {
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Nearest,
            min_filter: wgpu::FilterMode::Nearest,
            mipmap_filter: wgpu::FilterMode::Nearest,
            ..Default::default()
        });

        Ok(Self {
            texture,
            view,
            sampler,
        })
    }

    pub(super) fn new_depth_texture(
        device: &wgpu::Device,
        config: &wgpu::SurfaceConfiguration,
    ) -> Self {
        let size = wgpu::Extent3d {
            width: config.width,
            height: config.height,
            depth_or_array_layers: 1,
        };
        let desc = wgpu::TextureDescriptor {
            label: None,
            size,
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: Self::DEPTH_FORMAT,
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT | wgpu::TextureUsages::TEXTURE_BINDING,
            view_formats: &[],
        };
        let texture = device.create_texture(&desc);

        let view = texture.create_view(&wgpu::TextureViewDescriptor::default());
        let sampler = device.create_sampler(&wgpu::SamplerDescriptor {
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Linear,
            min_filter: wgpu::FilterMode::Linear,
            mipmap_filter: wgpu::FilterMode::Nearest,
            compare: Some(wgpu::CompareFunction::LessEqual),
            lod_min_clamp: 0.0,
            lod_max_clamp: 100.0,
            ..Default::default()
        });

        Self {
            texture,
            view,
            sampler,
        }
    }
}
