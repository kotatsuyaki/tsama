use std::collections::HashMap;

use bytemuck::{Pod, Zeroable};
use glam::{ivec2, IVec2};
use tsama_core::types::SegmentLocation;

use crate::consts::BLOCK_BUFFER_SIZE;

use super::UniformBuffer;

pub(super) struct BufferOrigins {
    uniform_buffer: UniformBuffer,
    /// Next buffer binding offset to write to.
    next_offset: u64,
    /// Mapping from points to offsets.
    points: HashMap<IVec2, u64>,
}

#[repr(C)]
#[derive(Debug, Clone, Copy, Pod, Zeroable)]
struct BufferOriginUniform {
    point: IVec2,
}

impl BufferOrigins {
    pub(super) fn new(device: &wgpu::Device) -> Self {
        let uniform_buffer = UniformBuffer::new_with_dynamic_offset::<BufferOriginUniform>(
            device,
            1024,
            wgpu::ShaderStages::VERTEX,
        );
        let next_offset = 0;
        let points = HashMap::new();

        Self {
            uniform_buffer,
            next_offset,
            points,
        }
    }

    pub(super) fn clear(&mut self) {
        self.next_offset = 0;
        self.points.clear();
    }

    /// Inserts a point to the buffer if it's not yet in it.
    pub(super) fn insert_point(&mut self, queue: &wgpu::Queue, point: IVec2) {
        use std::collections::hash_map::Entry;

        if let Entry::Vacant(entry) = self.points.entry(point) {
            entry.insert(self.next_offset);
            self.write_uniform_buffer(queue, point);
            self.advance_offset();
        }
    }

    pub(super) fn offset(&self, point: IVec2) -> u32 {
        *self.points.get(&point).unwrap() as u32
    }

    fn write_uniform_buffer(&self, queue: &wgpu::Queue, point: IVec2) {
        self.uniform_buffer
            .write(queue, self.next_offset, BufferOriginUniform::new(point));
    }

    fn advance_offset(&mut self) {
        assert!(std::mem::size_of::<BufferOriginUniform>() <= 256);
        self.next_offset += 256_u64;
    }

    pub(super) fn bind_group(&self) -> &wgpu::BindGroup {
        self.uniform_buffer.bind_group()
    }

    pub(super) fn bind_group_layout(&self) -> &wgpu::BindGroupLayout {
        self.uniform_buffer.bind_group_layout()
    }
}

impl BufferOriginUniform {
    fn new(point: IVec2) -> Self {
        Self { point }
    }
}

pub(super) struct Pivot {
    point: IVec2,
    uniform_buffer: UniformBuffer,
}

impl Pivot {
    pub(super) fn new(device: &wgpu::Device, point: IVec2) -> Self {
        let uniform = PivotUniform::new(point);
        let uniform_buffer = UniformBuffer::new(device, uniform, wgpu::ShaderStages::VERTEX);

        Self {
            point,
            uniform_buffer,
        }
    }

    pub(super) fn set_point(&mut self, point: IVec2) {
        self.point = point;
    }

    pub(super) fn write_uniform_buffer(&self, queue: &wgpu::Queue) {
        let uniform = PivotUniform::new(self.point);
        self.uniform_buffer.write(queue, 0, uniform);
    }

    pub(super) fn bind_group(&self) -> &wgpu::BindGroup {
        self.uniform_buffer.bind_group()
    }

    pub(super) fn bind_group_layout(&self) -> &wgpu::BindGroupLayout {
        self.uniform_buffer.bind_group_layout()
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, Pod, Zeroable)]
struct PivotUniform {
    point: IVec2,
}

impl PivotUniform {
    fn new(point: IVec2) -> Self {
        Self { point }
    }
}

pub(super) trait SegmentLocationExt {
    fn buffer_origin(&self) -> IVec2;
}

impl SegmentLocationExt for SegmentLocation {
    fn buffer_origin(&self) -> IVec2 {
        let block_location = ivec2(self.x(), self.z()) * 16;
        block_location.div_euclid(ivec2(BLOCK_BUFFER_SIZE, BLOCK_BUFFER_SIZE)) * BLOCK_BUFFER_SIZE
    }
}
