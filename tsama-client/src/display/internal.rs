use anyhow::Result;
use glam::{ivec2, ivec3, vec2, IVec2, Vec2, Vec3, Vec3Swizzles, Vec4};
use itertools::Itertools;
use std::cmp::Reverse;
use std::collections::HashMap;
use std::sync::Arc;
use tracing::{info, info_span, warn};
use tsama_core::types::{Position, SegmentLocation, WorldSize};
use winit::window::Window;

use super::api::*;
use crate::consts::MERGED_TEXTURE_SIZE;

mod primitives;
pub use primitives::*;

mod texture;
use texture::*;

mod buffer;
use buffer::*;

mod buffer_origin;
use buffer_origin::*;

mod camera;
use camera::*;

mod uniform_buffer;
use uniform_buffer::*;

mod viewport;
use viewport::*;

mod inputs;

pub struct GameDisplay {
    surface: wgpu::Surface<'static>,
    device: wgpu::Device,
    queue: wgpu::Queue,
    config: wgpu::SurfaceConfiguration,
    size: winit::dpi::PhysicalSize<u32>,

    block_render_pipeline: wgpu::RenderPipeline,
    transparent_block_render_pipeline: wgpu::RenderPipeline,
    entity_render_pipeline: wgpu::RenderPipeline,
    entity_debug_render_pipeline: wgpu::RenderPipeline,
    ui_render_pipeline: wgpu::RenderPipeline,

    block_buffers: HashMap<(SegmentLocation, BlockBufferKind), BlockVertexBuffer>,
    entity_buffer: Option<EntityVertexBuffer>,
    entity_debug_buffer: Option<EntityDebugVertexBuffer>,
    ui_buffer: Option<UiVertexBuffer>,
    camera: Camera,
    depth_texture: BasicTexture,
    merged_texture: BindableTexture,
    pivot: Pivot,
    buffer_origins: BufferOrigins,

    inputs: Inputs,
    window: Arc<Window>,

    texture_atlas: TextureAtlas,
    viewport: Viewport,

    world_size: WorldSize,
}

impl TextureRect {
    pub fn to_face_texture_coords(self) -> [Vec2; 4] {
        let min_x = self.min.x;
        let min_y = self.min.y;
        let max_x = self.min.x + self.size.x;
        let max_y = self.min.y + self.size.y;

        [
            vec2(min_x, max_y),
            vec2(max_x, max_y),
            vec2(max_x, min_y),
            vec2(min_x, min_y),
        ]
    }
}

impl GameDisplay {
    fn update_pivot_point(&mut self, camera_config: &CameraConfig) {
        let point = camera_config.pivot_point();
        self.pivot.set_point(point);
    }

    pub async fn new(window: Arc<Window>) -> Self {
        let size = window.inner_size();

        // The instance is a handle to our GPU
        let instance = wgpu::Instance::new(wgpu::InstanceDescriptor {
            backends: wgpu::Backends::all(),
            dx12_shader_compiler: Default::default(),
            flags: wgpu::InstanceFlags::default(),
            gles_minor_version: wgpu::Gles3MinorVersion::default(),
        });

        let surface = instance.create_surface(window.clone()).unwrap();

        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::default(),
                compatible_surface: Some(&surface),
                force_fallback_adapter: false,
            })
            .await
            .unwrap();
        info!("Adapter limits: {:#?}", adapter.limits());

        let (device, queue) = adapter
            .request_device(
                &wgpu::DeviceDescriptor {
                    required_features: wgpu::Features::empty(),
                    required_limits: wgpu::Limits::default(),
                    memory_hints: wgpu::MemoryHints::default(),
                    label: None,
                },
                None, // Trace path
            )
            .await
            .unwrap();

        let surface_caps = surface.get_capabilities(&adapter);
        let surface_format = surface_caps
            .formats
            .iter()
            .copied()
            .find(|f| f.is_srgb())
            .unwrap_or(surface_caps.formats[0]);
        info!("Surface format: {surface_format:#?}");
        info!("Surface present modes: {:#?}", surface_caps.present_modes);
        assert!(surface_caps
            .present_modes
            .contains(&wgpu::PresentMode::Fifo));
        let config = wgpu::SurfaceConfiguration {
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            format: surface_format,
            width: size.width,
            height: size.height,
            present_mode: wgpu::PresentMode::Fifo,
            alpha_mode: surface_caps.alpha_modes[0],
            view_formats: vec![],
            // 2 is the minimum recommended value.
            desired_maximum_frame_latency: 2,
        };
        surface.configure(&device, &config);

        let block_shader =
            device.create_shader_module(wgpu::include_wgsl!("../../assets/shader.wgsl"));
        let entity_shader =
            device.create_shader_module(wgpu::include_wgsl!("../../assets/entity_shader.wgsl"));
        let entity_debug_shader = device
            .create_shader_module(wgpu::include_wgsl!("../../assets/entity_debug_shader.wgsl"));
        let ui_shader =
            device.create_shader_module(wgpu::include_wgsl!("../../assets/ui_shader.wgsl"));
        let camera = Camera::new(&device, CameraConfig::default());
        let pivot = Pivot::new(&device, ivec2(0, 0));
        let viewport = Viewport::new(&device, ivec2(size.width as i32, size.height as i32));
        let buffer_origins = BufferOrigins::new(&device);
        let depth_texture = BasicTexture::new_depth_texture(&device, &config);
        let merged_texture = BindableTexture::from_size(
            &device,
            wgpu::Extent3d {
                width: MERGED_TEXTURE_SIZE,
                height: MERGED_TEXTURE_SIZE,
                depth_or_array_layers: 1,
            },
            None,
        )
        .expect("Expected block texture creation to succeed");

        info!("Creating block render pipeline");
        let block_render_pipeline = create_block_render_pipeline(
            &device,
            &camera,
            &pivot,
            &buffer_origins,
            &merged_texture,
            &block_shader,
            &config,
            &depth_texture,
            wgpu::BlendState::REPLACE,
            wgpu::CompareFunction::Less,
            true,
        );
        let transparent_block_render_pipeline = create_block_render_pipeline(
            &device,
            &camera,
            &pivot,
            &buffer_origins,
            &merged_texture,
            &block_shader,
            &config,
            &depth_texture,
            wgpu::BlendState::ALPHA_BLENDING,
            wgpu::CompareFunction::Less,
            false,
        );
        info!("Creating entity render pipeline");
        let entity_render_pipeline = create_entity_render_pipeline(
            &device,
            &camera,
            &merged_texture,
            entity_shader,
            &config,
            &depth_texture,
        );
        info!("Creating entity debug render pipeline");
        let entity_debug_render_pipeline =
            create_entity_debug_render_pipeline(&device, &camera, entity_debug_shader, &config);
        info!("Creating ui render pipeline");
        let ui_render_pipeline = create_ui_render_pipeline(
            &device,
            &viewport,
            &merged_texture,
            ui_shader,
            &config,
            &depth_texture,
        );

        const TEXTURE_ATLAS_SIZE: i32 = MERGED_TEXTURE_SIZE as i32;
        let texture_atlas = TextureAtlas::new(TEXTURE_ATLAS_SIZE);

        Self {
            window,
            surface,
            device,
            queue,
            config,
            size,
            block_render_pipeline,
            transparent_block_render_pipeline,
            entity_render_pipeline,
            entity_debug_render_pipeline,
            ui_render_pipeline,
            block_buffers: HashMap::new(),
            entity_buffer: None,
            entity_debug_buffer: None,
            ui_buffer: None,
            camera,
            depth_texture,
            merged_texture,
            pivot,
            buffer_origins,
            inputs: Inputs::new(),
            texture_atlas,
            viewport,
            world_size: WorldSize::from_radius_in_chunks(128),
        }
    }

    pub fn handle_mouse_event(
        &mut self,
        state: &winit::event::ElementState,
        button: &winit::event::MouseButton,
    ) {
        self.inputs.process_mouse_event(*button, *state);
    }

    pub fn handle_device_event(&mut self, event: winit::event::DeviceEvent) {
        if let winit::event::DeviceEvent::MouseMotion { delta } = event {
            self.inputs.add_cursor_delta(delta)
        }
    }

    /// Handles an incoming [`winit::event::Event`].
    pub fn resize(&mut self, new_size: winit::dpi::PhysicalSize<u32>) {
        if new_size.width > 0 && new_size.height > 0 {
            self.size = new_size;
            self.config.width = new_size.width;
            self.config.height = new_size.height;
            self.surface.configure(&self.device, &self.config);
            self.depth_texture = BasicTexture::new_depth_texture(&self.device, &self.config);
            self.camera
                .set_aspect_ratio(new_size.width as f32 / new_size.height as f32);
            self.viewport = Viewport::new(
                &self.device,
                ivec2(new_size.width as i32, new_size.height as i32),
            );
            dbg!(&new_size);
        }
    }

    fn sort_transparent_quads(&mut self) {
        let keys = self
            .block_buffers
            .keys()
            .cloned()
            .filter(|(_, kind)| *kind == BlockBufferKind::Transparent)
            .collect_vec();

        let eye = self.camera.config().eye.as_vec3();
        for &key in keys.iter() {
            let block_buffer: &mut BlockVertexBuffer = self.block_buffers.get_mut(&key).unwrap();
            if block_buffer.should_re_sort_from(eye) {
                block_buffer.re_sort_from(eye);
            }
        }
    }

    pub fn render(&mut self) -> std::ops::ControlFlow<()> {
        match self.render_impl() {
            Ok(_) => std::ops::ControlFlow::Continue(()),
            Err(wgpu::SurfaceError::Lost) => {
                self.resize(self.size);
                std::ops::ControlFlow::Continue(())
            }
            Err(wgpu::SurfaceError::OutOfMemory) => std::ops::ControlFlow::Break(()),
            Err(e) => {
                warn!("Error occured during redraw: {:?}", e);
                std::ops::ControlFlow::Continue(())
            }
        }
    }

    fn render_impl(&mut self) -> Result<(), wgpu::SurfaceError> {
        self.sort_transparent_quads();

        self.camera.write_uniform_buffers(&self.queue);
        self.pivot.write_uniform_buffer(&self.queue);

        let output_surface_texture = self.surface.get_current_texture()?;
        let output_texture_view = output_surface_texture
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());
        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor { label: None });

        // Render blocks.
        info_span!("render_display_opaque_blocks").in_scope(|| {
            self.buffer_origins.clear();
            self.render_blocks(
                &mut encoder,
                &output_texture_view,
                BlockBufferKind::Opaque,
                wgpu::LoadOp::Clear(wgpu::Color {
                    r: 0.1,
                    g: 0.333,
                    b: 0.66,
                    a: 1.0,
                }),
                wgpu::LoadOp::Clear(1.0),
            );
        });

        // Render entities.
        info_span!("render_display_entities").in_scope(|| {
            self.render_entities(&mut encoder, &output_texture_view);
            self.render_entity_debug_frames(&mut encoder, &output_texture_view);
        });

        // Render transparent blocks.
        info_span!("render_display_transparent_blocks").in_scope(|| {
            self.render_blocks(
                &mut encoder,
                &output_texture_view,
                BlockBufferKind::Transparent,
                wgpu::LoadOp::Load,
                wgpu::LoadOp::Load,
            );
        });

        // Render UI.
        info_span!("render_ui").in_scope(|| {
            self.render_ui(&mut encoder, &output_texture_view);
        });

        // Submit the frame.
        info_span!("render_display_queue_submit").in_scope(|| {
            self.queue.submit(std::iter::once(encoder.finish()));
        });
        info_span!("render_display_notify").in_scope(|| {
            self.window.pre_present_notify();
        });
        info_span!("render_display_present").in_scope(|| {
            output_surface_texture.present();
        });

        self.center_cursor();

        Ok(())
    }

    fn center_cursor(&mut self) {
        if self.window.has_focus() {
            let center_position =
                winit::dpi::PhysicalPosition::new(self.size.width / 2, self.size.height / 2);
            let _ = self.window.set_cursor_position(center_position);
        }
    }

    fn render_ui(
        &mut self,
        encoder: &mut wgpu::CommandEncoder,
        output_texture_view: &wgpu::TextureView,
    ) {
        let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &[
                // @location(0) color target
                Some(wgpu::RenderPassColorAttachment {
                    view: output_texture_view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Load,
                        store: wgpu::StoreOp::Store,
                    },
                }),
            ],
            depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachment {
                view: self.depth_texture.view(),
                depth_ops: Some(wgpu::Operations {
                    load: wgpu::LoadOp::Load,
                    store: wgpu::StoreOp::Store,
                }),
                stencil_ops: None,
            }),
            timestamp_writes: None,
            occlusion_query_set: None,
        });
        render_pass.set_pipeline(&self.ui_render_pipeline);
        render_pass.set_bind_group(0, self.viewport.bind_group(), &[]);
        render_pass.set_bind_group(1, self.merged_texture.bind_group(), &[]);

        let ui_buffer = self.ui_buffer.as_mut().unwrap();
        ui_buffer.write_vertex_buffer(&self.device);
        render_pass.set_vertex_buffer(0, ui_buffer.hardware_buffer_slice().unwrap());
        render_pass.draw(0..(ui_buffer.quads.len() as u32 * 6), 0..1);
    }

    fn render_blocks(
        &mut self,
        encoder: &mut wgpu::CommandEncoder,
        output_texture_view: &wgpu::TextureView,
        kind: BlockBufferKind,
        color_load_op: wgpu::LoadOp<wgpu::Color>,
        depth_load_op: wgpu::LoadOp<f32>,
    ) {
        let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &[
                // @location(0) color target
                Some(wgpu::RenderPassColorAttachment {
                    view: output_texture_view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: color_load_op,
                        store: wgpu::StoreOp::Store,
                    },
                }),
            ],
            depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachment {
                view: self.depth_texture.view(),
                depth_ops: Some(wgpu::Operations {
                    load: depth_load_op,
                    store: wgpu::StoreOp::Store,
                }),
                stencil_ops: None,
            }),
            timestamp_writes: None,
            occlusion_query_set: None,
        });
        render_pass.set_pipeline(match kind {
            BlockBufferKind::Opaque => &self.block_render_pipeline,
            BlockBufferKind::Transparent => &self.transparent_block_render_pipeline,
        });
        render_pass.set_bind_group(0, self.camera.bind_group_with_pivot(), &[]);
        render_pass.set_bind_group(1, self.pivot.bind_group(), &[]);
        render_pass.set_bind_group(3, self.merged_texture.bind_group(), &[]);

        // Collect buffer keys matching the requested block buffer kind.
        let mut block_buffer_keys = self
            .block_buffers
            .keys()
            .cloned()
            .filter(|(_, candidate_kind)| *candidate_kind == kind)
            .collect_vec();

        // Transparent block buffers require sorting.
        if kind == BlockBufferKind::Transparent {
            let player_location = Position::from(self.camera.config().eye).segment_location();
            block_buffer_keys.sort_unstable_by_key(|(location, _)| {
                Reverse(location.distance_squared_shortest(player_location, self.world_size))
            });
        }

        self.frustum_cull(&mut block_buffer_keys);

        // For each block buffer, write the buffer origin and the vertices to the hardware buffer.
        for &key in block_buffer_keys.iter() {
            let block_buffer = self.block_buffers.get_mut(&key).unwrap();
            self.buffer_origins.insert_point(
                &self.queue,
                block_buffer.unwrapped_origin(self.camera.config().eye.xz().floor().as_ivec2()),
            );
            block_buffer.write_vertex_buffer(&self.device);
        }
        self.queue.submit([]);

        // Draw each buffer.
        for &key in block_buffer_keys.iter() {
            let block_buffer = self.block_buffers.get(&key).unwrap();
            if block_buffer.is_empty() {
                continue;
            }

            let buffer_origin_offset = self.buffer_origins.offset(
                block_buffer.unwrapped_origin(self.camera.config().eye.xz().floor().as_ivec2()),
            );
            render_pass.set_bind_group(
                2,
                self.buffer_origins.bind_group(),
                &[buffer_origin_offset],
            );

            render_pass.set_vertex_buffer(0, block_buffer.hardware_buffer_slice().unwrap());
            info_span!("render_display_draw").in_scope(|| {
                render_pass.draw(0..(block_buffer.quads.len() as u32 * 6), 0..1);
            });
        }
    }

    fn frustum_cull(&self, block_buffer_keys: &mut Vec<(SegmentLocation, BlockBufferKind)>) {
        let pivot_point = self.camera.config().pivot_point();
        let view_proj = self.camera.view_proj_with_pivot_point();
        let view_proj_rows = [0, 1, 2, 3].map(|i| view_proj.row(i));
        block_buffer_keys.retain(|&(location, kind)| {
            let block_buffer = self.block_buffers.get(&(location, kind)).unwrap();
            let buffer_origin =
                block_buffer.unwrapped_origin(self.camera.config().eye.xz().floor().as_ivec2());
            let frustum_planes: [Vec4; 6] = [
                // Left, right, bottom, top planes.
                (view_proj_rows[3] + view_proj_rows[0]),
                (view_proj_rows[3] - view_proj_rows[0]),
                (view_proj_rows[3] + view_proj_rows[1]),
                (view_proj_rows[3] - view_proj_rows[1]),
                // Near and far planes.  The near plane has a form different from the rest because
                // of the z axis being [0.0, 1.0] in wgsl.
                (view_proj_rows[3]),
                (view_proj_rows[3] - view_proj_rows[2]),
            ];
            let min_buffer_location = {
                let min_block_location = location.min_block_location();
                // Map `BlockLocation` to the rendered position in the vertex buffer.
                // Same as in `tsama_client::game::render_solid_block()`.
                ivec3(
                    min_block_location.x().rem_euclid(1024),
                    min_block_location.y(),
                    min_block_location.z().rem_euclid(1024),
                )
            };
            // Same position offset as in the vertex shader.
            let offset = Vec3::from(((buffer_origin - pivot_point).as_vec2(), 0.0)).xzy();
            let vmin = min_buffer_location.as_vec3() + offset;
            let vmax = vmin + 16.0;

            for plane in frustum_planes {
                if (plane.dot(Vec4::new(vmin.x, vmin.y, vmin.z, 1.0)) < 0.0)
                    && (plane.dot(Vec4::new(vmax.x, vmin.y, vmin.z, 1.0)) < 0.0)
                    && (plane.dot(Vec4::new(vmin.x, vmax.y, vmin.z, 1.0)) < 0.0)
                    && (plane.dot(Vec4::new(vmax.x, vmax.y, vmin.z, 1.0)) < 0.0)
                    && (plane.dot(Vec4::new(vmin.x, vmin.y, vmax.z, 1.0)) < 0.0)
                    && (plane.dot(Vec4::new(vmax.x, vmin.y, vmax.z, 1.0)) < 0.0)
                    && (plane.dot(Vec4::new(vmin.x, vmax.y, vmax.z, 1.0)) < 0.0)
                    && (plane.dot(Vec4::new(vmax.x, vmax.y, vmax.z, 1.0)) < 0.0)
                {
                    // The bounding box of the plane is completely outside a particular plane.
                    return false;
                }
            }

            true
        });
    }

    fn render_entities(
        &mut self,
        encoder: &mut wgpu::CommandEncoder,
        output_texture_view: &wgpu::TextureView,
    ) {
        let Some(entity_buffer) = self.entity_buffer.as_mut() else {
            return;
        };

        let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &[
                // @location(0) color target
                Some(wgpu::RenderPassColorAttachment {
                    view: output_texture_view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Load,
                        store: wgpu::StoreOp::Store,
                    },
                }),
            ],
            depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachment {
                view: self.depth_texture.view(),
                depth_ops: Some(wgpu::Operations {
                    load: wgpu::LoadOp::Load,
                    store: wgpu::StoreOp::Store,
                }),
                stencil_ops: None,
            }),
            timestamp_writes: None,
            occlusion_query_set: None,
        });
        render_pass.set_pipeline(&self.entity_render_pipeline);
        render_pass.set_bind_group(0, self.camera.bind_group(), &[]);
        render_pass.set_bind_group(1, self.merged_texture.bind_group(), &[]);

        entity_buffer.write_vertex_buffer(&self.device);
        render_pass.set_vertex_buffer(0, entity_buffer.hardware_buffer_slice().unwrap());
        render_pass.draw(0..(entity_buffer.quads.len() as u32 * 6), 0..1);
    }

    fn render_entity_debug_frames(
        &mut self,
        encoder: &mut wgpu::CommandEncoder,
        output_texture_view: &wgpu::TextureView,
    ) {
        // Skip rendering if the entity debug buffer is None.
        let Some(entity_debug_buffer) = self.entity_debug_buffer.as_mut() else {
            return;
        };

        let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &[
                // @location(0) color target
                Some(wgpu::RenderPassColorAttachment {
                    view: output_texture_view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Load,
                        store: wgpu::StoreOp::Store,
                    },
                }),
            ],
            depth_stencil_attachment: None,
            timestamp_writes: None,
            occlusion_query_set: None,
        });
        render_pass.set_pipeline(&self.entity_debug_render_pipeline);
        render_pass.set_bind_group(0, self.camera.bind_group(), &[]);

        entity_debug_buffer.write_vertex_buffer(&self.device);
        render_pass.set_vertex_buffer(0, entity_debug_buffer.hardware_buffer_slice().unwrap());
        render_pass.draw(0..(entity_debug_buffer.vertices.len() as u32), 0..1);
    }

    pub fn handle_keyboard_event(&mut self, event: &winit::event::KeyEvent) {
        self.inputs.process_key_event(event);
    }
}

fn create_entity_debug_render_pipeline(
    device: &wgpu::Device,
    camera: &Camera,
    shader: wgpu::ShaderModule,
    config: &wgpu::SurfaceConfiguration,
) -> wgpu::RenderPipeline {
    let entity_debug_render_pipeline_layout =
        device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: None,
            bind_group_layouts: &[camera.bind_group_layout()],
            push_constant_ranges: &[],
        });
    device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
        label: None,
        layout: Some(&entity_debug_render_pipeline_layout),
        vertex: wgpu::VertexState {
            module: &shader,
            entry_point: Some("main_vertex"),
            buffers: &[FrameVertex::buffer_layout()],
            compilation_options: wgpu::PipelineCompilationOptions::default(),
        },
        fragment: Some(wgpu::FragmentState {
            module: &shader,
            entry_point: Some("main_fragment"),
            targets: &[Some(wgpu::ColorTargetState {
                format: config.format,
                blend: Some(wgpu::BlendState::REPLACE),
                write_mask: wgpu::ColorWrites::ALL,
            })],
            compilation_options: wgpu::PipelineCompilationOptions::default(),
        }),
        primitive: wgpu::PrimitiveState {
            topology: wgpu::PrimitiveTopology::LineList,
            strip_index_format: None,
            front_face: wgpu::FrontFace::Ccw,
            cull_mode: None,
            polygon_mode: wgpu::PolygonMode::Fill,
            unclipped_depth: false,
            conservative: false,
        },
        depth_stencil: None,
        multisample: wgpu::MultisampleState {
            count: 1,
            mask: !0,
            alpha_to_coverage_enabled: false,
        },
        multiview: None,
        cache: None,
    })
}

fn create_entity_render_pipeline(
    device: &wgpu::Device,
    camera: &Camera,
    merged_texture: &BindableTexture,
    shader: wgpu::ShaderModule,
    config: &wgpu::SurfaceConfiguration,
    depth_texture: &BasicTexture,
) -> wgpu::RenderPipeline {
    let entity_render_pipeline_layout =
        device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: None,
            bind_group_layouts: &[
                camera.bind_group_layout(),
                merged_texture.bind_group_layout(),
            ],
            push_constant_ranges: &[],
        });
    device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
        label: None,
        layout: Some(&entity_render_pipeline_layout),
        vertex: wgpu::VertexState {
            module: &shader,
            entry_point: Some("main_vertex"),
            buffers: &[NormVertex::buffer_layout()],
            compilation_options: wgpu::PipelineCompilationOptions::default(),
        },
        fragment: Some(wgpu::FragmentState {
            module: &shader,
            entry_point: Some("main_fragment"),
            targets: &[Some(wgpu::ColorTargetState {
                format: config.format,
                blend: Some(wgpu::BlendState::REPLACE),
                write_mask: wgpu::ColorWrites::ALL,
            })],
            compilation_options: wgpu::PipelineCompilationOptions::default(),
        }),
        primitive: wgpu::PrimitiveState {
            topology: wgpu::PrimitiveTopology::TriangleList,
            strip_index_format: None,
            front_face: wgpu::FrontFace::Ccw,
            cull_mode: Some(wgpu::Face::Back),
            polygon_mode: wgpu::PolygonMode::Fill,
            unclipped_depth: false,
            conservative: false,
        },
        depth_stencil: Some(wgpu::DepthStencilState {
            format: depth_texture.format(),
            depth_write_enabled: true,
            depth_compare: wgpu::CompareFunction::Less,
            stencil: wgpu::StencilState::default(),
            bias: wgpu::DepthBiasState::default(),
        }),
        multisample: wgpu::MultisampleState {
            count: 1,
            mask: !0,
            alpha_to_coverage_enabled: false,
        },
        multiview: None,
        cache: None,
    })
}

#[allow(clippy::too_many_arguments)]
fn create_block_render_pipeline(
    device: &wgpu::Device,
    camera: &Camera,
    pivot: &Pivot,
    buffer_origins: &BufferOrigins,
    merged_texture: &BindableTexture,
    shader: &wgpu::ShaderModule,
    config: &wgpu::SurfaceConfiguration,
    depth_texture: &BasicTexture,
    // The following args are for the distinction between opaque & transparent block rendering.
    blend: wgpu::BlendState,
    depth_compare: wgpu::CompareFunction,
    depth_write_enabled: bool,
) -> wgpu::RenderPipeline {
    let block_render_pipeline_layout =
        device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: None,
            bind_group_layouts: &[
                camera.bind_group_layout(),
                pivot.bind_group_layout(),
                buffer_origins.bind_group_layout(),
                merged_texture.bind_group_layout(),
            ],
            push_constant_ranges: &[],
        });
    device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
        label: None,
        layout: Some(&block_render_pipeline_layout),
        vertex: wgpu::VertexState {
            module: shader,
            entry_point: Some("main_vertex"),
            buffers: &[Vertex::buffer_layout()],
            compilation_options: wgpu::PipelineCompilationOptions::default(),
        },
        fragment: Some(wgpu::FragmentState {
            module: shader,
            entry_point: Some("main_fragment"),
            targets: &[Some(wgpu::ColorTargetState {
                format: config.format,
                blend: Some(blend),
                write_mask: wgpu::ColorWrites::ALL,
            })],
            compilation_options: wgpu::PipelineCompilationOptions::default(),
        }),
        primitive: wgpu::PrimitiveState {
            topology: wgpu::PrimitiveTopology::TriangleList,
            strip_index_format: None,
            front_face: wgpu::FrontFace::Ccw,
            cull_mode: Some(wgpu::Face::Back),
            polygon_mode: wgpu::PolygonMode::Fill,
            unclipped_depth: false,
            conservative: false,
        },
        depth_stencil: Some(wgpu::DepthStencilState {
            format: depth_texture.format(),
            depth_write_enabled,
            depth_compare,
            stencil: wgpu::StencilState::default(),
            bias: wgpu::DepthBiasState::default(),
        }),
        multisample: wgpu::MultisampleState {
            count: 1,
            mask: !0,
            alpha_to_coverage_enabled: false,
        },
        multiview: None,
        cache: None,
    })
}

fn create_ui_render_pipeline(
    device: &wgpu::Device,
    viewport: &Viewport,
    merged_texture: &BindableTexture,
    shader: wgpu::ShaderModule,
    config: &wgpu::SurfaceConfiguration,
    depth_texture: &BasicTexture,
) -> wgpu::RenderPipeline {
    let ui_render_pipeline_layout =
        device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: None,
            bind_group_layouts: &[
                viewport.bind_group_layout(),
                merged_texture.bind_group_layout(),
            ],
            push_constant_ranges: &[],
        });
    device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
        label: None,
        layout: Some(&ui_render_pipeline_layout),
        vertex: wgpu::VertexState {
            module: &shader,
            entry_point: Some("main_vertex"),
            compilation_options: wgpu::PipelineCompilationOptions::default(),
            buffers: &[UiVertex::buffer_layout()],
        },
        fragment: Some(wgpu::FragmentState {
            module: &shader,
            entry_point: Some("main_fragment"),
            compilation_options: wgpu::PipelineCompilationOptions::default(),
            targets: &[Some(wgpu::ColorTargetState {
                format: config.format,
                blend: Some(wgpu::BlendState::ALPHA_BLENDING),
                write_mask: wgpu::ColorWrites::ALL,
            })],
        }),
        primitive: wgpu::PrimitiveState {
            topology: wgpu::PrimitiveTopology::TriangleList,
            strip_index_format: None,
            front_face: wgpu::FrontFace::Ccw,
            cull_mode: Some(wgpu::Face::Back),
            polygon_mode: wgpu::PolygonMode::Fill,
            unclipped_depth: false,
            conservative: false,
        },
        depth_stencil: Some(wgpu::DepthStencilState {
            format: depth_texture.format(),
            depth_write_enabled: true,
            depth_compare: wgpu::CompareFunction::Less,
            stencil: wgpu::StencilState::default(),
            bias: wgpu::DepthBiasState::default(),
        }),
        multisample: wgpu::MultisampleState {
            count: 1,
            mask: !0,
            alpha_to_coverage_enabled: false,
        },
        multiview: None,
        cache: None,
    })
}

impl RenderTarget for GameDisplay {
    type BlockBuffer = BlockVertexBuffer;
    type EntityBuffer = EntityVertexBuffer;
    type EntityDebugBuffer = EntityDebugVertexBuffer;
    type UiBuffer = UiVertexBuffer;
    type InputSourceType = Inputs;

    fn get_or_create_block_buffer(
        &mut self,
        location: SegmentLocation,
        kind: BlockBufferKind,
    ) -> &mut Self::BlockBuffer {
        let buffer_origin = location.buffer_origin();
        self.block_buffers
            .entry((location, kind))
            .or_insert_with(|| BlockVertexBuffer::new(buffer_origin))
    }

    fn remove_block_buffer(&mut self, location: SegmentLocation, kind: BlockBufferKind) {
        self.block_buffers.remove(&(location, kind));
    }

    fn get_or_create_entity_buffer(&mut self) -> &mut Self::EntityBuffer {
        self.entity_buffer
            .get_or_insert_with(|| EntityVertexBuffer::new())
    }

    fn get_or_create_entity_debug_buffer(&mut self) -> &mut Self::EntityDebugBuffer {
        self.entity_debug_buffer
            .get_or_insert_with(|| EntityDebugVertexBuffer::new())
    }

    fn remove_entity_buffers(&mut self) {
        self.entity_buffer = None;
        self.entity_debug_buffer = None;
    }

    fn get_or_create_ui_buffer(&mut self) -> &mut Self::UiBuffer {
        self.ui_buffer.get_or_insert_with(|| UiVertexBuffer::new())
    }

    fn remove_ui_buffer(&mut self) {
        self.ui_buffer = None;
    }

    fn update_camera_config(&mut self, config: CameraConfig) {
        self.update_pivot_point(&config);
        self.camera.set_config(config);
    }

    fn register_texture(&mut self, data: &[u8], expected_size: IVec2) -> TextureRect {
        self.texture_atlas.add_texture(data, expected_size)
    }

    fn register_texture_with_unknown_size(&mut self, data: &[u8]) -> (TextureRect, IVec2) {
        self.texture_atlas.add_texture_with_unknown_size(data)
    }

    fn upload_textures(&self) {
        self.texture_atlas
            .canvas
            .write_to(
                &mut std::io::BufWriter::new(std::fs::File::create("/tmp/debug.png").unwrap()),
                image::ImageFormat::Png,
            )
            .unwrap();
        self.merged_texture
            .write_texture(&self.queue, &self.texture_atlas.canvas);
    }

    fn input_source(&mut self) -> &mut Self::InputSourceType {
        &mut self.inputs
    }

    fn update_world_size(&mut self, size: WorldSize) {
        self.world_size = size;
    }

    fn physical_size(&self) -> (u32, u32) {
        (self.size.width, self.size.height)
    }
}
