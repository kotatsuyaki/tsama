use std::collections::HashSet;

use glam::{vec2, DVec3, IVec2, IVec3, Vec2, Vec3};
use tsama_core::types::{SegmentLocation, WorldSize};
use winit::{event::MouseButton, keyboard::KeyCode};

pub trait RenderTarget {
    type BlockBuffer: RenderedBlockBuffer;
    type EntityBuffer: RenderedEntityBuffer;
    type EntityDebugBuffer: RenderedEntityDebugBuffer;
    type UiBuffer: RenderedUiBuffer;

    type InputSourceType: InputSource;

    fn input_source(&mut self) -> &mut Self::InputSourceType;

    fn update_camera_config(&mut self, config: CameraConfig);

    // Creating and removing block buffers.
    fn get_or_create_block_buffer(
        &mut self,
        location: SegmentLocation,
        kind: BlockBufferKind,
    ) -> &mut Self::BlockBuffer;
    fn remove_block_buffer(&mut self, location: SegmentLocation, kind: BlockBufferKind);

    // Creating and removing entity buffers.
    fn get_or_create_entity_buffer(&mut self) -> &mut Self::EntityBuffer;
    fn get_or_create_entity_debug_buffer(&mut self) -> &mut Self::EntityDebugBuffer;
    fn remove_entity_buffers(&mut self);

    fn get_or_create_ui_buffer(&mut self) -> &mut Self::UiBuffer;
    fn remove_ui_buffer(&mut self);

    fn register_texture(&mut self, data: &[u8], expected_size: IVec2) -> TextureRect;
    fn register_texture_with_unknown_size(&mut self, data: &[u8]) -> (TextureRect, IVec2);
    fn upload_textures(&self);

    /// Returns `(width, height)` of the render target in physical pixels.
    #[allow(dead_code)]
    fn physical_size(&self) -> (u32, u32);

    #[allow(dead_code)]
    fn update_world_size(&mut self, size: WorldSize);
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum BlockBufferKind {
    Opaque,
    Transparent,
}

#[derive(Debug, Clone)]
pub struct Inputs {
    pub(super) presesd_keys: HashSet<KeyCode>,
    pub(super) pressed_mouse_buttons: HashSet<MouseButton>,
    pub(super) last_pressed_keys: HashSet<KeyCode>,
    pub(super) last_pressed_mouse_buttons: HashSet<MouseButton>,
    pub(super) cursor_delta: (f64, f64),
}

pub struct CameraConfig {
    pub(super) eye: DVec3,
    pub(super) pitch: f32,
    pub(super) yaw: f32,
}

#[derive(Clone, Copy, Debug)]
pub struct TextureRect {
    pub(super) min: Vec2,
    pub(super) size: Vec2,
}

impl TextureRect {
    pub fn view(&self, offset: Vec2, size: Vec2) -> Self {
        Self {
            min: self.min + offset * self.size,
            size: size * self.size,
        }
    }

    /// Creates an `[TextureRect; 6]` array viewing into the `self` for a box.
    ///
    /// * `offset` - The offset of the top-left corner within `self`.
    /// * `box_size` - The `(x, y, z)` sizes of the box.
    /// * `texture_size` - The size of `self`.
    pub fn view_box(
        &self,
        offset: impl Into<IVec2>,
        box_size: impl Into<IVec3>,
        texture_size: impl Into<IVec2>,
    ) -> [TextureRect; 6] {
        let offset = offset.into().as_vec2();
        let texture_size = texture_size.into().as_vec2();
        let [x, y, z] = box_size.into().as_vec3().to_array();

        // The faces are rendered in the order of +X, -X, +Y, -Y, +Z, -Z.
        // Each face that is not ±Y views the texture with the top-left corner as its offset and
        // with the (horizontal, vertical) lengthes as its size.
        //
        // Because we render entities facing the opposite direction (our +Z versus their -Z), The ±Y
        // faces have their bottom-left corners as their offsets and have the vertical length
        // negated.
        //
        //         z         x       x
        //   + - - - - - +-------+-------+
        //               |       |       |
        // z |         z |     z |     z |
        //               |  +Y   |  -Y   |
        //   |     z     |   x   |   x   |
        //   +-----------+-------+-------+---+-------+
        // y |           |       |           |       |
        //   |    +X     |  +Z   |    -X     |  -Z   |
        //   +-----------+-------+-----------+-------+

        [
            self.view(
                (offset + vec2(0.0, z)) / texture_size,
                vec2(z, y) / texture_size,
            ),
            self.view(
                (offset + vec2(z + x, z)) / texture_size,
                vec2(z, y) / texture_size,
            ),
            self.view(
                (offset + vec2(z, /* Flip Z, 0 -> z */ z)) / texture_size,
                vec2(x, /* Flip Z, z -> -z */ -z) / texture_size,
            ),
            self.view(
                (offset + vec2(z + x, 0.0)) / texture_size,
                vec2(x, z) / texture_size,
            ),
            self.view(
                (offset + vec2(z, z)) / texture_size,
                vec2(x, y) / texture_size,
            ),
            self.view(
                (offset + vec2(2.0 * z + x, z)) / texture_size,
                vec2(x, y) / texture_size,
            ),
        ]
    }

    pub fn view_mirrored_box(
        &self,
        offset: impl Into<IVec2>,
        box_size: impl Into<IVec3>,
        texture_size: impl Into<IVec2>,
    ) -> [TextureRect; 6] {
        let offset = offset.into().as_vec2();
        let texture_size = texture_size.into().as_vec2();
        let [x, y, z] = box_size.into().as_vec3().to_array();

        //         z         x       x
        //   + - - - - - +-------+-------+
        //               |       |       |
        // z |         z |   r z |   r z |
        //               |  +Y   |  -Y   |
        //   |     z     |   x   |   x   |
        //   +-----------+-------+-------+---+-------+
        // y |     r     |   r   |     r     |   r   |
        //   |    -X     |  +Z   |    +X     |  -Z   |
        //   +-----------+-------+-----------+-------+

        [
            self.view(
                (offset + vec2(2.0 * z + x, z)) / texture_size,
                vec2(-z, y) / texture_size,
            ),
            self.view(
                (offset + vec2(z, z)) / texture_size,
                vec2(-z, y) / texture_size,
            ),
            self.view(
                (offset + vec2(z + x, /* Flip Z, 0 -> z */ z)) / texture_size,
                vec2(-x, /* Flip Z, z -> -z */ -z) / texture_size,
            ),
            self.view(
                (offset + vec2(z + 2.0 * x, 0.0)) / texture_size,
                vec2(-x, z) / texture_size,
            ),
            self.view(
                (offset + vec2(z + x, z)) / texture_size,
                vec2(-x, y) / texture_size,
            ),
            self.view(
                (offset + vec2(2.0 * z + 2.0 * x, z)) / texture_size,
                vec2(-x, y) / texture_size,
            ),
        ]
    }
}

// Supplementary traits

pub trait InputSource {
    /// Returns whether the key has just been pressed down.
    fn is_key_down(&self, key: KeyCode) -> bool;

    /// Returns whether the key is pressed.
    fn is_key_held(&self, key: KeyCode) -> bool;

    fn is_mouse_down(&self, button: MouseButton) -> bool;

    fn is_mouse_held(&self, button: MouseButton) -> bool;

    /// Returns the cumulative cursor delta since the last call to [`clear_cursor_delta`].
    ///
    /// The first element is the horizontal delta, and the second element is the vertical delta.
    fn cursor_delta(&self) -> (f64, f64);

    /// Clears the key downs.
    ///
    /// After calling this function, [`is_key_down`] returns false for keys that are continuously
    /// held, until the keys are released and pressed again.
    fn clear_key_downs(&mut self);

    /// Clears the cursor delta.
    fn clear_cursor_delta(&mut self);
}

pub trait RenderedBlockBuffer {
    type QuadType: RenderedBlockQuad;

    fn add_quad(&mut self, quad: Self::QuadType);
}

pub trait RenderedBlockQuad {
    type VertexType: RenderedBlockVertex;

    fn new(vertices: [Self::VertexType; 4]) -> Self;
}

pub trait RenderedBlockVertex {
    /// Creates a new vertex.
    ///
    /// The `position` is the position relative to the buffer origin.
    fn new(position: Vec3, tint: Vec3, tex_coords: Vec2) -> Self;
}

pub trait RenderedEntityBuffer {
    type QuadType: RenderedEntityQuad;

    fn add_quad(&mut self, quad: Self::QuadType);
}

pub trait RenderedEntityQuad {
    type VertexType: RenderedEntityVertex;

    fn new(vertices: &[Self::VertexType; 4]) -> Self;
}

pub trait RenderedEntityVertex {
    fn new(position: Vec3, norm: Vec3, tint: Vec3, tex_coords: Vec2) -> Self;
}

pub trait RenderedEntityDebugBuffer {
    fn add_vertex(&mut self, position: Vec3);
}

pub trait RenderedUiBuffer {
    type QuadType: RenderedUiQuad;

    fn add_quad(&mut self, quad: Self::QuadType);
}

pub trait RenderedUiQuad {
    type VertexType: RenderedUiVertex;

    fn new(vertices: &[Self::VertexType; 4]) -> Self;
}

pub trait RenderedUiVertex {
    fn new(position: IVec3, tint: Vec3, tex_coords: Vec2) -> Self;
}
