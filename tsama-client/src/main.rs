#![allow(clippy::bool_comparison)]

use std::{str::FromStr, sync::Arc};

use anyhow::{Context, Result};
use tracing::{info, info_span, warn};
use tracing_subscriber::{layer::SubscriberExt, Layer};
use winit::event_loop::EventLoop;

mod consts;
mod display;
mod game;
mod network_client;
mod render;

use display::{GameDisplay, RenderTarget};
use game::Game;
use tsama_core::timer::Timer;

fn main() -> Result<()> {
    let guard = init_logger();

    let runtime = build_async_runtime();
    run(runtime.handle(), guard)?;

    info!("Client normal exit");
    Ok(())
}

#[must_use]
fn init_logger() -> Option<tracing_chrome::FlushGuard> {
    let filter = tracing_subscriber::filter::Builder::default()
        .with_default_directive("tsama_client=info".parse().unwrap())
        .from_env()
        .unwrap();
    // TODO: Switch to tracing-perfetto once it's possible.
    if std::env::var("TSAMA_TRACING_CHROME").is_ok() {
        let (chrome_layer, chrome_guard) = tracing_chrome::ChromeLayerBuilder::new().build();
        tracing::subscriber::set_global_default(
            tracing_subscriber::registry()
                .with(tracing_subscriber::fmt::layer().with_filter(filter))
                .with(chrome_layer.with_filter(
                    tracing_subscriber::EnvFilter::from_str("tsama_client=info").unwrap(),
                )),
        )
        .unwrap();
        Some(chrome_guard)
    } else {
        tracing::subscriber::set_global_default(
            tracing_subscriber::registry()
                .with(tracing_subscriber::fmt::layer().with_filter(filter)),
        )
        .unwrap();
        None
    }
}

fn build_async_runtime() -> tokio::runtime::Runtime {
    tokio::runtime::Builder::new_multi_thread()
        .thread_name("tsama-client-thread")
        .enable_all()
        .build()
        .expect("Failed to build tokio runtime")
}

fn run(handle: &tokio::runtime::Handle, guard: Option<tracing_chrome::FlushGuard>) -> Result<()> {
    let mut app = GameApp {
        handle: handle.clone(),
        window: None,
        game_display: None,
        game: None,
        timer: None,
        network_client_near: None,
        network_client_far: None,
        guard,
    };

    let event_loop = EventLoop::new()?;
    event_loop.run_app(&mut app)?;

    Ok(())
}

fn configure_hidden_cursor(window: &winit::window::Window) {
    if let Err(err) = window
        // This fails for macOS.
        .set_cursor_grab(winit::window::CursorGrabMode::Confined)
        // This fails for X11 and Windows.
        .or_else(|_| window.set_cursor_grab(winit::window::CursorGrabMode::Locked))
    {
        warn!("Failed to set the cursor grab mode: {:?}", err);
    }
    window.set_cursor_visible(false);
}

struct GameApp {
    handle: tokio::runtime::Handle,
    window: Option<Arc<winit::window::Window>>,
    game_display: Option<GameDisplay>,
    game: Option<Game>,
    timer: Option<Timer>,
    network_client_near: Option<network_client::NetworkClientNear>,
    network_client_far: Option<network_client::NetworkClientFar>,
    guard: Option<tracing_chrome::FlushGuard>,
}

impl GameApp {
    fn ensure_game_display(&mut self) {
        if self.game_display.is_none() {
            self.game_display = Some(
                self.handle
                    .block_on(GameDisplay::new(self.window.as_ref().unwrap().clone())),
            );
        }
    }

    fn ensure_game(&mut self) {
        if self.game.is_none() {
            let game_display = self.game_display.as_mut().unwrap();
            let (network_client_near, network_client_far) = network_client::new_network_client();
            let mut game = Game::new();
            let timer = Timer::new_from_target_framerate(20);

            game.register_fonts_and_textures(game_display);

            self.game = Some(game);
            self.timer = Some(timer);
            self.network_client_near = Some(network_client_near);
            self.network_client_far = Some(network_client_far);
        }
    }
}

impl winit::application::ApplicationHandler for GameApp {
    fn resumed(&mut self, event_loop: &winit::event_loop::ActiveEventLoop) {
        let window_attributes = winit::window::Window::default_attributes()
            .with_title("Tsama")
            .with_inner_size(winit::dpi::LogicalSize::new(800, 600));
        let window = event_loop
            .create_window(window_attributes)
            .context("Unable to create window")
            .unwrap();
        configure_hidden_cursor(&window);

        self.window = Some(Arc::new(window));

        {
            let _enter_guard = self.handle.enter();
            self.ensure_game_display();
            self.ensure_game();
        }
    }

    fn device_event(
        &mut self,
        _event_loop: &winit::event_loop::ActiveEventLoop,
        _device_id: winit::event::DeviceId,
        event: winit::event::DeviceEvent,
    ) {
        let game_display = self.game_display.as_mut().unwrap();
        game_display.handle_device_event(event);
    }

    fn window_event(
        &mut self,
        event_loop: &winit::event_loop::ActiveEventLoop,
        _window_id: winit::window::WindowId,
        event: winit::event::WindowEvent,
    ) {
        match event {
            winit::event::WindowEvent::RedrawRequested => {
                let window = self.window.as_mut().unwrap();
                let timer = self.timer.as_mut().unwrap();
                let game = self.game.as_mut().unwrap();
                let game_display = self.game_display.as_mut().unwrap();
                let network_client_near = self.network_client_near.as_mut().unwrap();

                timer.record_frame_start();
                let accumulated_frame = timer.accumulated_frame();
                let whole_frame_count = accumulated_frame.whole_frame_count();
                let remainder_frame_fraction = accumulated_frame.remainder_frame_fraction();
                let fps = timer.fps();

                // Queue a new RedrawRequested event for the next frame.
                info_span!("request_redraw").in_scope(|| {
                    window.request_redraw();
                });

                // Run the game logic.
                let span = info_span!("game_step");
                for _ in 0..whole_frame_count {
                    let _enter = span.enter();
                    game.run_step(network_client_near, game_display.input_source());
                }
                let span = info_span!("game_partial_step");
                span.in_scope(|| {
                    game.run_partial_step(game_display.input_source(), remainder_frame_fraction);
                });

                // Render vertex buffers to the display.
                let span = info_span!("render");
                span.in_scope(|| {
                    game.render_world(game_display, remainder_frame_fraction, fps);
                });

                if let std::ops::ControlFlow::Break(_) =
                    info_span!("render_display").in_scope(|| game_display.render())
                {
                    event_loop.exit();
                };
            }
            winit::event::WindowEvent::MouseInput {
                device_id: _device_id,
                state,
                button,
            } => {
                let game_display = self.game_display.as_mut().unwrap();
                game_display.handle_mouse_event(&state, &button);
            }
            winit::event::WindowEvent::KeyboardInput {
                device_id: _device_id,
                event,
                is_synthetic: _is_synthetic,
            } => {
                let game_display = self.game_display.as_mut().unwrap();
                game_display.handle_keyboard_event(&event);
            }
            winit::event::WindowEvent::Resized(physical_size) => {
                let game_display = self.game_display.as_mut().unwrap();
                game_display.resize(physical_size);
            }
            winit::event::WindowEvent::CloseRequested => {
                event_loop.exit();
            }
            _ => (),
        }
    }

    fn exiting(&mut self, _event_loop: &winit::event_loop::ActiveEventLoop) {
        self.guard.take();
    }
}
