use ambassador::{delegatable_trait, delegate_remote};
use enum_map::EnumMap;
use glam::{ivec3, vec3, EulerRot, IVec3, Mat4, Vec3, Vec3Swizzles};
use tsama_core::{
    entity_types::EntityMotion,
    iterate_blocks,
    types::{BlockLocation, Face, LocalBlockLocation, Position},
    Aurochs, BlockAir, BlockBehavior, BlockGlass, BlockGrass, BlockKind, BlockStone, BlockWater,
    Deer, DummyEntity, EntityKind, Player, World,
};

use crate::display::{
    RenderTarget, RenderedBlockBuffer, RenderedBlockQuad, RenderedBlockVertex,
    RenderedEntityBuffer, RenderedEntityQuad, RenderedEntityVertex, TextureRect,
};

mod render_deer;
mod render_simple_blocks;
mod render_simple_entities;
mod texts;
pub use texts::Font;

#[delegate_remote]
#[delegate(RenderEntity)]
pub enum EntityKind {
    Player(Player),
    DummyEntity(DummyEntity),
    Aurochs(Aurochs),
    Deer(Deer),
}

#[delegatable_trait]
pub trait RenderEntity {
    fn render<BufferType: RenderedEntityBuffer>(
        &self,
        motion: EntityMotion,
        eye_position: Position,
        world: &World,
        buffer: &mut BufferType,
        entity_textures: &[TextureRect],
    );

    fn register_textures<TargetType: RenderTarget>(
        &self,
        _target: &mut TargetType,
    ) -> Vec<TextureRect> {
        Vec::new()
    }
}

#[delegate_remote]
#[delegate(RenderBlock)]
#[repr(u8)]
enum BlockKind {
    BlockAir(BlockAir),
    BlockGrass(BlockGrass),
    BlockStone(BlockStone),
    BlockWater(BlockWater),
    BlockGlass(BlockGlass),
}

#[allow(unused_variables)]
#[delegatable_trait]
pub trait RenderBlock {
    /// Render the block at `location` (global) and `local_location` (relative to chunk origin) in
    /// `world` to the vertex buffer `buffer`.
    fn render<BufferType: RenderedBlockBuffer>(
        &self,
        location: BlockLocation,
        local_location: LocalBlockLocation,
        world: &World,
        buffer: &mut BufferType,
        block_textures: &[TextureRect],
    ) {
    }

    fn render_transparent<BufferType: RenderedBlockBuffer>(
        &self,
        location: BlockLocation,
        world: &World,
        buffer: &mut BufferType,
        block_textures: &[TextureRect],
    ) {
    }

    fn register_textures<TargetType: RenderTarget>(
        &self,
        target: &mut TargetType,
    ) -> Vec<TextureRect> {
        Vec::new()
    }
}

fn render_solid_block<BufferType: RenderedBlockBuffer>(
    location: BlockLocation,
    world: &World,
    buffer: &mut BufferType,
    face_textures: EnumMap<Face, TextureRect>,
    tint: Vec3,
) {
    type Vertex<BufferType> =
        <<BufferType as RenderedBlockBuffer>::QuadType as RenderedBlockQuad>::VertexType;

    for face in Face::iter() {
        let neighbor_kind = world.block_kind(location + face.to_offset());
        let neighbor_is_solid = neighbor_kind.is_some_and(|kind| kind.is_opaque());
        let should_render = !neighbor_is_solid || neighbor_kind.is_none();

        let buffer_location = ivec3(
            location.x().rem_euclid(1024),
            location.y(),
            location.z().rem_euclid(1024),
        );

        if should_render {
            let quad = face.to_unit_quad();
            let min_ao_offsets = face.to_ao_offset_quad();
            let face_texture = face_textures[face];
            let face_texture_coords = face_texture.to_face_texture_coords();

            let vertices = [0, 1, 2, 3].map(|i| {
                let point = quad[i];
                let texture_coords = face_texture_coords[i];

                let min_ao_offset = min_ao_offsets[i];
                let ao_locations = iterate_blocks!(
                    location + min_ao_offset,
                    location + min_ao_offset + ivec3(1, 1, 1)
                );
                let mut solid_count = 0;
                for loc in ao_locations {
                    let is_solid = world
                        .block_kind(loc)
                        .is_some_and(|block_kind| block_kind.is_opaque());
                    if is_solid {
                        solid_count += 1;
                    }
                }
                let tint = match solid_count {
                    0..=4 => tint,
                    5 => 0.75 * tint,
                    6 => 0.5 * tint,
                    7 => 0.3 * tint,
                    8 => 0.6 * tint,
                    _ => unreachable!("Solid count cannot be greater than 8"),
                };

                Vertex::<BufferType>::new(point + buffer_location.as_vec3(), tint, texture_coords)
            });
            let quad = BufferType::QuadType::new(vertices);
            buffer.add_quad(quad);
        }
    }
}

/// A box for rendering, defined by a minimum point and a maximum point.
///
/// The name `RenderPart` is chosen over the similar `RenderBox`, because it would be intriguing to
/// name a variable of the type `RenderBox` as `box`, which is a keyword in Rust.
#[derive(Clone, Copy)]
struct RenderPart {
    min: Vec3,
    max: Vec3,
}

enum PointIndex {
    Min,
    Center,
    Max,
}

#[allow(unused)]
impl RenderPart {
    /// # Panics
    /// Panics if any element in `min` is greather than its corresponding element in `max`.  To
    /// create a `RenderPart` with an arbitrary pair of opposite vertices, use
    /// [`RenderPart::from_unordered`].
    fn new(min: impl Into<Vec3>, max: impl Into<Vec3>) -> Self {
        let min = min.into();
        let max = max.into();
        assert!(min.cmple(max).all());
        Self { min, max }
    }

    fn from_unordered(point_a: Vec3, point_b: Vec3) -> Self {
        Self {
            min: point_a.min(point_b),
            max: point_a.max(point_b),
        }
    }

    fn neighbor(&self, face: Face) -> Self {
        let offset = face.to_offset().as_vec3() * (self.max - self.min);
        Self {
            min: self.min + offset,
            max: self.max + offset,
        }
    }

    fn offset_max(&self, offset: Vec3) -> Self {
        Self::new(self.min, self.max + offset)
    }

    fn offset_min(&self, offset: Vec3) -> Self {
        Self::new(self.min + offset, self.max)
    }

    fn with_max_x_from_min_x(&self, offset: f32) -> Self {
        Self::new(self.min, self.max.with_x(self.min.x + offset))
    }

    fn with_min_x_from_max_x(&self, offset: f32) -> Self {
        Self::new(self.min.with_x(self.max.x + offset), self.max)
    }

    fn with_max_z_from_min_z(&self, offset: f32) -> Self {
        Self::new(self.min, self.max.with_z(self.min.z + offset))
    }

    fn with_min_z_from_max_z(&self, offset: f32) -> Self {
        Self::new(self.min.with_z(self.max.z + offset), self.max)
    }

    fn with_min_y_from_max_y(&self, offset: f32) -> Self {
        Self::new(self.min.with_y(self.max.y + offset), self.max)
    }

    const fn point(&self, x: PointIndex, y: PointIndex, z: PointIndex) -> Vec3 {
        vec3(
            match x {
                PointIndex::Min => self.min.x,
                PointIndex::Center => self.min.x * 0.5 + self.max.x * 0.5,
                PointIndex::Max => self.max.x,
            },
            match y {
                PointIndex::Min => self.min.y,
                PointIndex::Center => self.min.y * 0.5 + self.max.y * 0.5,
                PointIndex::Max => self.max.y,
            },
            match z {
                PointIndex::Min => self.min.z,
                PointIndex::Center => self.min.z * 0.5 + self.max.z * 0.5,
                PointIndex::Max => self.max.z,
            },
        )
    }

    fn min(&self) -> Vec3 {
        self.min
    }

    fn max(&self) -> Vec3 {
        self.max
    }

    const fn left_bottom_back(&self) -> Vec3 {
        self.min
    }

    const fn left_bottom_front(&self) -> Vec3 {
        self.point(PointIndex::Min, PointIndex::Min, PointIndex::Max)
    }

    const fn center_bottom_front(&self) -> Vec3 {
        self.point(PointIndex::Center, PointIndex::Min, PointIndex::Max)
    }

    const fn center_bottom_back(&self) -> Vec3 {
        self.point(PointIndex::Center, PointIndex::Min, PointIndex::Min)
    }

    const fn right_bottom_back(&self) -> Vec3 {
        self.point(PointIndex::Max, PointIndex::Min, PointIndex::Min)
    }

    const fn right_bottom_front(&self) -> Vec3 {
        self.point(PointIndex::Max, PointIndex::Min, PointIndex::Max)
    }

    const fn right_top_back(&self) -> Vec3 {
        self.point(PointIndex::Max, PointIndex::Max, PointIndex::Min)
    }

    const fn right_top_front(&self) -> Vec3 {
        self.point(PointIndex::Max, PointIndex::Max, PointIndex::Max)
    }
}

/// A render transform for rendering [`RenderPart`]s of entities.
///
/// # Typical usages
/// ```
/// let base_transform = RenderTransform::from_offset(center_bottom_position);
/// let part_transform = base_transform.with_transform(
///     /* scale*/ Vec3::ONE,
///     /* center */ (0.0, 1.0, 0.0),
///     /* angles */ (1.2, 0.0, 0.0),
/// );
/// part_transform.render_part(part, &texture_views, buffer);
/// ```
#[derive(Clone, Copy)]
struct RenderTransform {
    transform: Mat4,
    norm_transform: Mat4,
}

impl RenderTransform {
    /// * `offset` - The offset for the _bottom-center_ point of the box.
    fn from_offset(offset: Vec3) -> Self {
        Self {
            transform: Mat4::from_translation(offset),
            norm_transform: Mat4::IDENTITY,
        }
    }

    /// * `angles` - Roll, pitch, and yaw in radians.
    fn with_transform(
        &self,
        scale: impl Into<Vec3>,
        center: impl Into<Vec3>,
        angles: impl Into<Vec3>,
    ) -> Self {
        let scale = scale.into();
        let center = center.into();
        let angles = angles.into().yxz();

        let mat_scale = Mat4::from_scale(scale);
        let mat_translate_inverse = Mat4::from_translation(-center);
        let mat_rotate = Mat4::from_euler(EulerRot::YXZEx, angles[0], angles[1], angles[2]);
        let mat_translate = Mat4::from_translation(center);
        let transform =
            self.transform * (mat_translate * mat_rotate * mat_translate_inverse * mat_scale);
        let norm_transform = self.norm_transform * mat_rotate;

        Self {
            transform,
            norm_transform,
        }
    }

    fn render_part<BufferType: RenderedEntityBuffer>(
        &self,
        part: RenderPart,
        textures: &[TextureRect; 6],
        buffer: &mut BufferType,
    ) {
        self.render_box(part.min, part.max, textures, buffer);
    }

    fn render_box<BufferType: RenderedEntityBuffer>(
        &self,
        min: impl Into<Vec3>,
        max: impl Into<Vec3>,
        textures: &[TextureRect; 6],
        buffer: &mut BufferType,
    ) {
        type Vertex<BufferType> =
            <<BufferType as RenderedEntityBuffer>::QuadType as RenderedEntityQuad>::VertexType;
        let min = min.into();
        let max = max.into();
        for (i, face) in Face::iter().enumerate() {
            let quad = face
                .to_quad(min, max)
                .map(|v| self.transform.transform_point3(v));
            let norm = self.norm_transform.transform_vector3(face.to_norm());
            let texture_coords = textures[i].to_face_texture_coords();
            let vertices = [0, 1, 2, 3].map(|i| {
                Vertex::<BufferType>::new(quad[i], norm, vec3(1.0, 1.0, 1.0), texture_coords[i])
            });
            buffer.add_quad(BufferType::QuadType::new(&vertices));
        }
    }

    fn render_centered_box<BufferType: RenderedEntityBuffer>(
        &self,
        textures: &[TextureRect; 6],
        buffer: &mut BufferType,
    ) {
        type Vertex<BufferType> =
            <<BufferType as RenderedEntityBuffer>::QuadType as RenderedEntityQuad>::VertexType;
        for (i, face) in Face::iter().enumerate() {
            let quad = face
                .to_centered_unit_quad()
                .map(|v| self.transform.transform_point3(v));
            let norm = self.norm_transform.transform_vector3(face.to_norm());
            let texture_coords = textures[i].to_face_texture_coords();
            let vertices = [0, 1, 2, 3].map(|i| {
                Vertex::<BufferType>::new(quad[i], norm, vec3(1.0, 1.0, 1.0), texture_coords[i])
            });
            buffer.add_quad(BufferType::QuadType::new(&vertices));
        }
    }
}

pub trait FaceExt {
    #[allow(dead_code)]
    fn to_offset(&self) -> IVec3;
    fn to_quad(&self, min: Vec3, max: Vec3) -> [Vec3; 4];
    fn to_unit_quad(&self) -> [Vec3; 4];
    fn to_centered_unit_quad(&self) -> [Vec3; 4];
    fn to_ao_offset_quad(&self) -> [IVec3; 4];
    fn to_int_unit_quad(&self) -> [IVec3; 4];
    fn to_norm(&self) -> Vec3;
}

impl FaceExt for Face {
    fn to_offset(&self) -> IVec3 {
        match *self {
            Face::PosX => ivec3(1, 0, 0),
            Face::NegX => ivec3(-1, 0, 0),
            Face::PosY => ivec3(0, 1, 0),
            Face::NegY => ivec3(0, -1, 0),
            Face::PosZ => ivec3(0, 0, 1),
            Face::NegZ => ivec3(0, 0, -1),
        }
    }

    fn to_quad(&self, min: Vec3, max: Vec3) -> [Vec3; 4] {
        match *self {
            Face::PosX => [
                vec3(max.x, min.y, min.z),
                vec3(max.x, min.y, max.z),
                vec3(max.x, max.y, max.z),
                vec3(max.x, max.y, min.z),
            ],
            Face::NegX => [
                vec3(min.x, min.y, max.z),
                vec3(min.x, min.y, min.z),
                vec3(min.x, max.y, min.z),
                vec3(min.x, max.y, max.z),
            ],
            Face::PosY => [
                vec3(min.x, max.y, min.z),
                vec3(max.x, max.y, min.z),
                vec3(max.x, max.y, max.z),
                vec3(min.x, max.y, max.z),
            ],
            Face::NegY => [
                vec3(min.x, min.y, max.z),
                vec3(max.x, min.y, max.z),
                vec3(max.x, min.y, min.z),
                vec3(min.x, min.y, min.z),
            ],
            Face::PosZ => [
                vec3(max.x, min.y, max.z),
                vec3(min.x, min.y, max.z),
                vec3(min.x, max.y, max.z),
                vec3(max.x, max.y, max.z),
            ],
            Face::NegZ => [
                vec3(min.x, min.y, min.z),
                vec3(max.x, min.y, min.z),
                vec3(max.x, max.y, min.z),
                vec3(min.x, max.y, min.z),
            ],
        }
    }

    fn to_unit_quad(&self) -> [Vec3; 4] {
        match *self {
            Face::PosX => [
                vec3(1.0, 0.0, 0.0),
                vec3(1.0, 0.0, 1.0),
                vec3(1.0, 1.0, 1.0),
                vec3(1.0, 1.0, 0.0),
            ],
            Face::NegX => [
                vec3(0.0, 0.0, 1.0),
                vec3(0.0, 0.0, 0.0),
                vec3(0.0, 1.0, 0.0),
                vec3(0.0, 1.0, 1.0),
            ],
            Face::PosY => [
                vec3(0.0, 1.0, 0.0),
                vec3(1.0, 1.0, 0.0),
                vec3(1.0, 1.0, 1.0),
                vec3(0.0, 1.0, 1.0),
            ],
            Face::NegY => [
                vec3(0.0, 0.0, 1.0),
                vec3(1.0, 0.0, 1.0),
                vec3(1.0, 0.0, 0.0),
                vec3(0.0, 0.0, 0.0),
            ],
            Face::PosZ => [
                vec3(1.0, 0.0, 1.0),
                vec3(0.0, 0.0, 1.0),
                vec3(0.0, 1.0, 1.0),
                vec3(1.0, 1.0, 1.0),
            ],
            Face::NegZ => [
                vec3(0.0, 0.0, 0.0),
                vec3(1.0, 0.0, 0.0),
                vec3(1.0, 1.0, 0.0),
                vec3(0.0, 1.0, 0.0),
            ],
        }
    }

    fn to_centered_unit_quad(&self) -> [Vec3; 4] {
        self.to_unit_quad().map(|v| v - 0.5)
    }

    fn to_ao_offset_quad(&self) -> [IVec3; 4] {
        match *self {
            Face::PosX => [
                ivec3(0, -1, -1),
                ivec3(0, -1, 0),
                ivec3(0, 0, 0),
                ivec3(0, 0, -1),
            ],
            Face::NegX => [
                ivec3(-1, -1, 0),
                ivec3(-1, -1, -1),
                ivec3(-1, 0, -1),
                ivec3(-1, 0, 0),
            ],
            Face::PosY => [
                ivec3(-1, 0, -1),
                ivec3(0, 0, -1),
                ivec3(0, 0, 0),
                ivec3(-1, 0, 0),
            ],
            Face::NegY => [
                ivec3(-1, -1, 0),
                ivec3(0, -1, 0),
                ivec3(0, -1, -1),
                ivec3(-1, -1, -1),
            ],
            Face::PosZ => [
                ivec3(0, -1, 0),
                ivec3(-1, -1, 0),
                ivec3(-1, 0, 0),
                ivec3(0, 0, 0),
            ],
            Face::NegZ => [
                ivec3(-1, -1, -1),
                ivec3(0, -1, -1),
                ivec3(0, 0, -1),
                ivec3(-1, 0, -1),
            ],
        }
    }

    fn to_int_unit_quad(&self) -> [IVec3; 4] {
        match *self {
            Face::PosX => [
                ivec3(1, 0, 0),
                ivec3(1, 0, 1),
                ivec3(1, 1, 1),
                ivec3(1, 1, 0),
            ],
            Face::NegX => [
                ivec3(0, 0, 1),
                ivec3(0, 0, 0),
                ivec3(0, 1, 0),
                ivec3(0, 1, 1),
            ],
            Face::PosY => [
                ivec3(0, 1, 0),
                ivec3(1, 1, 0),
                ivec3(1, 1, 1),
                ivec3(0, 1, 1),
            ],
            Face::NegY => [
                ivec3(0, 0, 1),
                ivec3(1, 0, 1),
                ivec3(1, 0, 0),
                ivec3(0, 0, 0),
            ],
            Face::PosZ => [
                ivec3(1, 0, 1),
                ivec3(0, 0, 1),
                ivec3(0, 1, 1),
                ivec3(1, 1, 1),
            ],
            Face::NegZ => [
                ivec3(0, 0, 0),
                ivec3(1, 0, 0),
                ivec3(1, 1, 0),
                ivec3(0, 1, 0),
            ],
        }
    }

    fn to_norm(&self) -> Vec3 {
        match self {
            Face::PosX => vec3(1.0, 0.0, 0.0),
            Face::NegX => vec3(-1.0, 0.0, 0.0),
            Face::PosY => vec3(0.0, 1.0, 0.0),
            Face::NegY => vec3(0.0, -1.0, 0.0),
            Face::PosZ => vec3(0.0, 0.0, 1.0),
            Face::NegZ => vec3(0.0, 0.0, -1.0),
        }
    }
}
