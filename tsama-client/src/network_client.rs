use std::env;

use anyhow::{Context, Result};
use futures::{SinkExt, StreamExt, TryStreamExt};
use tokio::{
    net::{
        tcp::{OwnedReadHalf, OwnedWriteHalf},
        TcpStream,
    },
    sync::mpsc::{unbounded_channel, UnboundedReceiver, UnboundedSender},
};
use tokio_util::codec::{FramedRead, FramedWrite, LengthDelimitedCodec};
use tracing::{debug, trace, warn};
use tsama_core::{ClientMessage, SendPlayerMessage, ServerMessage};

pub trait Remote: SendPlayerMessage {
    fn send_message(&self, message: ClientMessage) -> Result<()>;
    fn receive_message(&mut self) -> Result<ServerMessage>;
}

impl SendPlayerMessage for NetworkClientNear {
    fn send(&self, message: ClientMessage) -> Result<()> {
        self.send_message(message)
    }
}

impl Remote for NetworkClientNear {
    fn send_message(&self, message: ClientMessage) -> Result<()> {
        debug!("-> {message:.03?}");
        self.outgoing_sender
            .send(message)
            .context("Failed to send client message")
    }

    fn receive_message(&mut self) -> Result<ServerMessage> {
        self.incoming_receiver
            .try_recv()
            .context("Failed to receive server message")
            .inspect(|message| {
                debug!("<- {message:.03?}");
            })
    }
}

#[derive(Clone)]
pub struct NetworkClientFar {
    incoming_sender: UnboundedSender<ServerMessage>,
    #[allow(dead_code)]
    outgoing_sender: UnboundedSender<ClientMessage>,
}

pub struct NetworkClientNear {
    incoming_receiver: UnboundedReceiver<ServerMessage>,
    outgoing_sender: UnboundedSender<ClientMessage>,
}

pub fn new_network_client() -> (NetworkClientNear, NetworkClientFar) {
    let (incoming_sender, incoming_receiver) = unbounded_channel();
    let (outgoing_sender, outgoing_receiver) = unbounded_channel();

    let near = NetworkClientNear {
        incoming_receiver,
        outgoing_sender: outgoing_sender.clone(),
    };
    let far = NetworkClientFar {
        incoming_sender,
        outgoing_sender,
    };

    tokio::spawn(far.clone().connect(outgoing_receiver));

    (near, far)
}

impl NetworkClientFar {
    async fn connect(self, outgoing_receiver: UnboundedReceiver<ClientMessage>) {
        let address = env::var("TSAMA_CLIENT_ADDRESS").unwrap_or("127.0.0.0:1111".to_string());
        let server_stream = match TcpStream::connect(address).await {
            Ok(stream) => stream,
            Err(err) => {
                warn!("Failed to connect to the port: {:?}", err);
                std::process::exit(1)
            }
        };
        let (read_half, write_half) = server_stream.into_split();
        tokio::spawn(self.clone().forward_outgoing(write_half, outgoing_receiver));
        tokio::spawn(self.forward_incoming(read_half));
    }

    async fn forward_outgoing(
        self,
        write_half: OwnedWriteHalf,
        mut outgoing_receiver: UnboundedReceiver<ClientMessage>,
    ) {
        let encoding_sink = FramedWrite::new(write_half, LengthDelimitedCodec::new()).with(
            |message: ClientMessage| async move {
                let mut encoded_buffer = vec![];
                ciborium::into_writer(&message, &mut encoded_buffer)?;
                let encoded_message: bytes::Bytes = encoded_buffer.into();
                Result::<bytes::Bytes>::Ok(encoded_message)
            },
        );
        let mut encoding_sink = Box::pin(encoding_sink);

        while let Some(message) = outgoing_receiver.recv().await {
            trace!("Forwarding outgoing message to the server");

            let Err(err) = encoding_sink.send(message).await else {
                continue;
            };
            warn!("Failed to forward a message to the socket: {:?}", err);
        }
    }

    async fn forward_incoming(self, read_half: OwnedReadHalf) {
        let decoded_stream = FramedRead::new(read_half, LengthDelimitedCodec::new())
            .map_err(|e| e.into())
            .and_then(|encoded_message| async move {
                let message: ServerMessage = ciborium::from_reader(encoded_message.as_ref())?;
                Result::<ServerMessage>::Ok(message)
            });
        let mut decoded_stream = Box::pin(decoded_stream);

        while let Some(message) = decoded_stream.next().await {
            trace!("Received incoming message from server");

            let message = match message {
                Ok(message) => message,
                Err(err) => {
                    warn!("Failed to receive a message from the server: {:?}", err);
                    continue;
                }
            };

            if let Err(err) = self.incoming_sender.send(message) {
                warn!(
                    "Failed to forward an incoming message from the server: {:?}",
                    err
                );
                continue;
            }
        }
    }
}
