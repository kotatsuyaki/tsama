use glam::{ivec2, vec3, IVec2, Vec3};
use tsama_core::{
    entity_types::EntityMotion,
    types::{Face, Position},
    Deer, World,
};

use crate::display::{RenderTarget, RenderedEntityBuffer, TextureRect};

use super::{RenderEntity, RenderPart, RenderTransform};

const TEXTURE_SIZE: IVec2 = ivec2(128, 64);

impl RenderEntity for Deer {
    fn render<BufferType: RenderedEntityBuffer>(
        &self,
        motion: EntityMotion,
        eye_position: Position,
        world: &World,
        buffer: &mut BufferType,
        entity_textures: &[TextureRect],
    ) {
        let render_position = motion
            .position()
            .sub_shortest(eye_position, world.size())
            .as_vec3()
            - vec3(0.0, Self::BOX_HEIGHT as f32 * 0.5, 0.0);
        let transform = RenderTransform::from_offset(render_position);

        // Render body.
        transform
            .with_transform(Vec3::ONE, (0.0, 1.4375, -0.125), (1.43, 0.0, 0.0))
            .render_part(
                RenderPart::new((-0.25, 1.625, -0.25), (0.25, 2.125, 0.375)),
                &entity_textures[0].view_box((18, 4), (8, 8, 10), TEXTURE_SIZE),
                buffer,
            );

        // Render torso.
        transform
            .with_transform(Vec3::ONE, (0.0, 1.4375, -0.125), (0.12, 0.0, 0.0))
            .render_part(
                RenderPart::new((-0.21875, 1.0, -0.3125), (0.21875, 1.625, 0.1875)),
                &entity_textures[0].view_box((0, 29), (7, 10, 8), TEXTURE_SIZE),
                buffer,
            );

        // Render rump.
        transform
            .with_transform(Vec3::ONE, (0.0, 1.40625, 0.0625), (-0.09, 0.0, 0.0))
            .render_part(
                RenderPart::new((-0.1875, 1.03125, -0.625), (0.1875, 1.65625, -0.25)),
                &entity_textures[0].view_box((0, 47), (6, 10, 6), TEXTURE_SIZE),
                buffer,
            );

        // Render tail.
        transform
            .with_transform(Vec3::ONE, (0.0, 1.59375, -0.625), (-1.3, 0.0, 0.0))
            .render_part(
                RenderPart::new((-0.09375, 1.5, -1.1875), (0.09375, 1.625, -0.625)),
                &entity_textures[0].view_box((24, 52), (3, 2, 9), TEXTURE_SIZE),
                buffer,
            );

        // Render neck.
        transform
            .with_transform(Vec3::ONE, (0.0, 1.5625, 0.5), (1.82, 0.0, 0.0))
            .render_part(
                RenderPart::new((-0.125, 1.5, 0.125), (0.125, 1.8125, 0.625)),
                &entity_textures[0].view_box((57, 22), (4, 5, 8), TEXTURE_SIZE),
                buffer,
            );

        // Render collar.
        transform
            .with_transform(Vec3::ONE, (0.125, 1.5625, 0.5), (1.15, 0.0, 0.0))
            .render_part(
                RenderPart::new((-0.125, 1.3125, 0.3125), (0.125, 1.6875, 0.75)),
                &entity_textures[0].view_box((30, 38), (4, 6, 7), TEXTURE_SIZE),
                buffer,
            );

        // Render head.
        let head_transform =
            transform.with_transform(Vec3::ONE, (0.0, 1.5625, 0.5), (0.16, 0.0, 0.0));
        let head_part = RenderPart::new((-0.15625, 1.875, 0.4375), (0.15625, 2.25, 0.8125));
        let snout_part = head_part
            .neighbor(Face::PosZ)
            .offset_min(vec3(0.0626, 0.025, 0.0))
            .offset_max(vec3(-0.0626, -0.15, 0.0))
            .with_max_z_from_min_z(0.25);

        head_transform.render_part(
            head_part,
            &entity_textures[0].view_box((54, 35), (5, 6, 6), TEXTURE_SIZE),
            buffer,
        );
        head_transform
            .with_transform(
                Vec3::ONE,
                head_part.left_bottom_back().with_y(2.25),
                (-0.1, -0.35, -0.35),
            )
            .render_box(
                (-0.15625 - 0.3125 + 0.15, 2.25 - 0.1875 + 0.05, 0.4375 + 0.1),
                (-0.15625 + 0.15, 2.25 + 0.05, 0.4375 + 0.1),
                &entity_textures[0].view_box((54, 16), (5, 3, 0), TEXTURE_SIZE),
                buffer,
            );
        head_transform
            .with_transform(
                Vec3::ONE,
                head_part.right_bottom_back().with_y(2.25),
                (-0.1, 0.35, 0.35),
            )
            .render_box(
                (0.15625 + 0.3125 - 0.15, 2.25 - 0.1875 + 0.05, 0.4375 + 0.1),
                (0.15625 - 0.15, 2.25 + 0.05, 0.4375 + 0.1),
                &entity_textures[0].view_box((54, 16), (5, 3, 0), TEXTURE_SIZE),
                buffer,
            );
        head_transform
            .with_transform(Vec3::ONE, (0.0, 0.5, 0.0), (0.0, 0.0, 0.0))
            .render_part(
                snout_part,
                &entity_textures[0].view_box((54, 0), (3, 3, 4), TEXTURE_SIZE),
                buffer,
            );

        // Render thigh1.
        let thigh_1_transform =
            transform.with_transform(Vec3::ONE, (0.1875, 1.375, -0.4375), (-0.17, 0.0, 0.17));
        let thigh_1_part = RenderPart::new((0.125, 0.95625, -0.625), (0.25, 1.51875, -0.3125));
        thigh_1_transform.render_part(
            thigh_1_part,
            &entity_textures[0].view_box((40, 22), (2, 9, 5), TEXTURE_SIZE),
            buffer,
        );

        // Render calf1.
        let calf_1_transform = thigh_1_transform.with_transform(
            Vec3::ONE,
            thigh_1_part.center_bottom_front(),
            (0.56, 0.0, -0.17),
        );
        let calf_1_part = thigh_1_part
            .neighbor(Face::NegY)
            .with_min_y_from_max_y(-0.375)
            .with_max_z_from_min_z(0.1875);
        calf_1_transform.render_part(
            calf_1_part,
            &entity_textures[0].view_box((54, 7), (2, 6, 3), TEXTURE_SIZE),
            buffer,
        );

        // Render leg1.
        let leg_1_transform = calf_1_transform.with_transform(
            Vec3::ONE,
            calf_1_part.center_bottom_back(),
            (-0.38, 0.0, 0.0),
        );
        let leg_1_part = calf_1_part
            .neighbor(Face::NegY)
            .with_min_y_from_max_y(-0.5625)
            .with_min_z_from_max_z(-0.125);
        leg_1_transform.render_part(
            leg_1_part,
            &entity_textures[0].view_box((0, 16), (2, 9, 2), TEXTURE_SIZE),
            buffer,
        );

        // Render toes1.
        let toes_1_transform = leg_1_transform.with_transform(
            Vec3::ONE,
            leg_1_part.center_bottom_front(),
            (1.13, 0.0, 0.0),
        );
        let toes_1_part = leg_1_part
            .neighbor(Face::NegY)
            .with_min_y_from_max_y(-0.0625)
            .with_max_z_from_min_z(0.25);
        toes_1_transform.render_part(
            toes_1_part,
            &entity_textures[0].view_box((18, 22), (2, 1, 4), TEXTURE_SIZE),
            buffer,
        );

        // Render hoof1.
        let hoof_1_part = toes_1_part
            .neighbor(Face::PosZ)
            .with_max_z_from_min_z(0.125);
        toes_1_transform
            .with_transform(
                Vec3::ONE,
                toes_1_part.center_bottom_front(),
                (-1.13, 0.0, 0.0),
            )
            .render_part(
                hoof_1_part,
                &entity_textures[0].view_box((30, 0), (2, 1, 2), TEXTURE_SIZE),
                buffer,
            );

        // Render thigh2.
        let thigh_2_transform =
            transform.with_transform(Vec3::ONE, (-0.1875, 1.375, -0.4375), (-0.17, 0.0, -0.17));
        let thigh_2_part = RenderPart::new((-0.25, 0.95625, -0.625), (-0.125, 1.51875, -0.3125));
        thigh_2_transform.render_part(
            thigh_2_part,
            &entity_textures[0].view_mirrored_box((40, 22), (2, 9, 5), TEXTURE_SIZE),
            buffer,
        );

        // Render calf2.
        let calf_2_transform = thigh_2_transform.with_transform(
            Vec3::ONE,
            thigh_2_part.center_bottom_front(),
            (0.56, 0.0, 0.17),
        );
        let calf_2_part = thigh_2_part
            .neighbor(Face::NegY)
            .with_min_y_from_max_y(-0.375)
            .with_max_z_from_min_z(0.1875);
        calf_2_transform.render_part(
            calf_2_part,
            &entity_textures[0].view_mirrored_box((54, 7), (2, 6, 3), TEXTURE_SIZE),
            buffer,
        );

        // Render leg2.
        let leg_2_transform = calf_2_transform.with_transform(
            Vec3::ONE,
            calf_2_part.center_bottom_back(),
            (-0.38, 0.0, 0.0),
        );
        let leg_2_part = calf_2_part
            .neighbor(Face::NegY)
            .with_min_y_from_max_y(-0.5625)
            .with_min_z_from_max_z(-0.125);
        leg_2_transform.render_part(
            leg_2_part,
            &entity_textures[0].view_mirrored_box((0, 16), (2, 9, 2), TEXTURE_SIZE),
            buffer,
        );

        // Render toes2.
        let toes_2_transform = leg_2_transform.with_transform(
            Vec3::ONE,
            leg_2_part.center_bottom_front(),
            (1.13, 0.0, 0.0),
        );
        let toes_2_part = leg_2_part
            .neighbor(Face::NegY)
            .with_min_y_from_max_y(-0.0625)
            .with_max_z_from_min_z(0.25);
        toes_2_transform.render_part(
            toes_2_part,
            &entity_textures[0].view_mirrored_box((18, 22), (2, 1, 4), TEXTURE_SIZE),
            buffer,
        );

        // Render hoof2.
        let hoof_2_part = toes_2_part
            .neighbor(Face::PosZ)
            .with_max_z_from_min_z(0.125);
        toes_2_transform
            .with_transform(
                Vec3::ONE,
                toes_2_part.center_bottom_front(),
                (-1.13, 0.0, 0.0),
            )
            .render_part(
                hoof_2_part,
                &entity_textures[0].view_mirrored_box((30, 0), (2, 1, 2), TEXTURE_SIZE),
                buffer,
            );

        // Render upperLeg3.
        let upper_leg_3_transform =
            transform.with_transform(Vec3::ONE, (0.25, 1.1875, 0.4375), (0.35, 0.0, 0.0));
        let upper_leg_3_part =
            RenderPart::new((0.16875, 0.9375, 0.34375), (0.29375, 1.25, 0.53125));
        upper_leg_3_transform.render_part(
            upper_leg_3_part,
            &entity_textures[0].view_box((30, 22), (2, 5, 3), TEXTURE_SIZE),
            buffer,
        );

        // Render leg3.
        let leg_3_transform = upper_leg_3_transform.with_transform(
            Vec3::ONE,
            upper_leg_3_part.left_bottom_back(),
            (-0.35, 0.0, 0.0),
        );
        let leg_3_part = upper_leg_3_part
            .neighbor(Face::NegY)
            .with_min_x_from_max_x(-0.125)
            .with_min_y_from_max_y(-0.4375)
            .with_min_z_from_max_z(-0.125);
        leg_3_transform.render_part(
            leg_3_part,
            &entity_textures[0].view_box((8, 16), (2, 7, 2), TEXTURE_SIZE),
            buffer,
        );

        // Render lowerleg3.
        let lower_leg_3_transform = leg_3_transform.with_transform(
            Vec3::ONE,
            leg_3_part.left_bottom_back(),
            (0.0, 0.0, 0.0),
        );
        let lower_leg_3_part = leg_3_part
            .neighbor(Face::NegY)
            .with_min_y_from_max_y(-0.375);
        lower_leg_3_transform.render_part(
            lower_leg_3_part,
            &entity_textures[0].view_box((30, 30), (2, 6, 2), TEXTURE_SIZE),
            buffer,
        );

        // Render toes3.
        let toes_3_transform = lower_leg_3_transform.with_transform(
            Vec3::ONE,
            lower_leg_3_part.center_bottom_front(),
            (1.13, 0.0, 0.0),
        );
        let toes_3_part = lower_leg_3_part
            .neighbor(Face::NegY)
            .with_min_y_from_max_y(-0.0625)
            .with_max_z_from_min_z(0.25);
        toes_3_transform.render_part(
            toes_3_part,
            &entity_textures[0].view_box((18, 22), (2, 1, 4), TEXTURE_SIZE),
            buffer,
        );

        // Render hoof3.
        let hoof_3_part = toes_3_part
            .neighbor(Face::PosZ)
            .with_max_z_from_min_z(0.125);
        toes_3_transform
            .with_transform(
                Vec3::ONE,
                toes_3_part.center_bottom_front(),
                (-1.13, 0.0, 0.0),
            )
            .render_part(
                hoof_3_part,
                &entity_textures[0].view_box((30, 0), (2, 1, 2), TEXTURE_SIZE),
                buffer,
            );

        // Render upperLeg4.
        let upper_leg_4_transform =
            transform.with_transform(Vec3::ONE, (-0.25, 1.1875, 0.4375), (0.35, 0.0, 0.0));
        let upper_leg_4_part =
            RenderPart::new((-0.296875, 0.9375, 0.34375), (-0.171875, 1.25, 0.53125));
        upper_leg_4_transform.render_part(
            upper_leg_4_part,
            &entity_textures[0].view_mirrored_box((30, 22), (2, 5, 3), TEXTURE_SIZE),
            buffer,
        );

        // Render leg4.
        let leg_4_transform = upper_leg_4_transform.with_transform(
            Vec3::ONE,
            upper_leg_4_part.right_bottom_back(),
            (-0.35, 0.0, 0.0),
        );
        let leg_4_part = upper_leg_4_part
            .neighbor(Face::NegY)
            .with_min_y_from_max_y(-0.4375)
            .with_min_z_from_max_z(-0.125)
            .with_max_x_from_min_x(0.125);
        leg_4_transform.render_part(
            leg_4_part,
            &entity_textures[0].view_mirrored_box((8, 16), (2, 7, 2), TEXTURE_SIZE),
            buffer,
        );

        // Render lowerleg4.
        let lower_leg_4_transform = leg_4_transform.with_transform(
            Vec3::ONE,
            leg_4_part.right_bottom_back(),
            (0.0, 0.0, 0.0),
        );
        let lower_leg_4_part = leg_4_part
            .neighbor(Face::NegY)
            .with_min_y_from_max_y(-0.375);
        lower_leg_4_transform.render_part(
            lower_leg_4_part,
            &entity_textures[0].view_mirrored_box((30, 30), (2, 6, 2), TEXTURE_SIZE),
            buffer,
        );

        // Render toes4.
        let toes_4_transform = lower_leg_4_transform.with_transform(
            Vec3::ONE,
            lower_leg_4_part.center_bottom_front(),
            (1.13, 0.0, 0.0),
        );
        let toes_4_part = lower_leg_4_part
            .neighbor(Face::NegY)
            .with_min_y_from_max_y(-0.0625)
            .with_max_z_from_min_z(0.25);
        toes_4_transform.render_part(
            toes_4_part,
            &entity_textures[0].view_mirrored_box((18, 22), (2, 1, 4), TEXTURE_SIZE),
            buffer,
        );

        // Render hoof4.
        let hoof_4_part = toes_4_part
            .neighbor(Face::PosZ)
            .with_max_z_from_min_z(0.125);
        toes_4_transform
            .with_transform(
                Vec3::ONE,
                toes_4_part.center_bottom_front(),
                (-1.13, 0.0, 0.0),
            )
            .render_part(
                hoof_4_part,
                &entity_textures[0].view_mirrored_box((30, 0), (2, 1, 2), TEXTURE_SIZE),
                buffer,
            );

        // render antler11.
        transform
            .with_transform(Vec3::ONE, (0.0, 1.5625, 0.5), (0.0, 0.0, 0.0))
            .render_box(
                (-0.125, 2.1875, 0.59375),
                (-0.0625, 2.375, 0.65625),
                &entity_textures[0].view_box((44, 0), (1, 3, 1), TEXTURE_SIZE),
                buffer,
            );

        // Render antler21.
        transform
            .with_transform(Vec3::ONE, (0.0, 1.5625, 0.5), (0.0, 0.0, 0.0))
            .render_box(
                (0.0625, 2.1875, 0.59375),
                (0.125, 2.375, 0.65625),
                &entity_textures[0].view_box((44, 0), (1, 3, 1), TEXTURE_SIZE),
                buffer,
            );
    }

    fn register_textures<TargetType: RenderTarget>(
        &self,
        target: &mut TargetType,
    ) -> Vec<TextureRect> {
        [target.register_texture(include_bytes!("../../assets/Deer.png"), ivec2(128, 64))].into()
    }
}
