use glam::{ivec2, Vec3};
use tsama_core::{
    entity_types::EntityMotion, types::Position, Aurochs, DummyEntity, Player, World,
};

use crate::display::{RenderTarget, RenderedEntityBuffer, TextureRect};

use super::{RenderEntity, RenderTransform};

impl RenderEntity for Player {
    fn render<BufferType: RenderedEntityBuffer>(
        &self,
        _motion: EntityMotion,
        _eye_position: Position,
        _world: &World,
        _buffer: &mut BufferType,
        _entity_textures: &[TextureRect],
    ) {
    }
}

impl RenderEntity for DummyEntity {
    fn render<BufferType: RenderedEntityBuffer>(
        &self,
        motion: EntityMotion,
        eye_position: Position,
        world: &World,
        buffer: &mut BufferType,
        entity_textures: &[TextureRect],
    ) {
        let render_position = motion
            .position()
            .sub_shortest(eye_position, world.size())
            .as_vec3();
        RenderTransform::from_offset(render_position)
            .with_transform(
                (1.2, 2.5, 1.2),
                Vec3::ZERO,
                (motion.facing().yaw(), 0.0, 0.0),
            )
            .render_centered_box(&[entity_textures[0]; 6], buffer);
    }

    fn register_textures<TargetType: RenderTarget>(
        &self,
        target: &mut TargetType,
    ) -> Vec<TextureRect> {
        [target.register_texture(include_bytes!("../../assets/Dummy.png"), ivec2(16, 16))].into()
    }
}

impl RenderEntity for Aurochs {
    fn render<BufferType: RenderedEntityBuffer>(
        &self,
        motion: EntityMotion,
        eye_position: Position,
        world: &World,
        _buffer: &mut BufferType,
        _entity_textures: &[TextureRect],
    ) {
        let _render_position = motion
            .position()
            .sub_shortest(eye_position, world.size())
            .as_vec3();
    }
}
