use std::{collections::HashMap, io::Read};

use anyhow::{Context, Result};
use glam::{ivec3, vec2, IVec2, IVec3, Vec3};

use crate::display::{RenderedUiBuffer, RenderedUiQuad, RenderedUiVertex, TextureRect};
use crate::render::Face;

use super::FaceExt;

pub struct Font {
    texture: TextureRect,
    texture_size: IVec2,
    all_glyph_metrics: HashMap<char, GlyphMetrics>,
}

#[derive(Debug)]
pub struct GlyphMetrics {
    texture_offsets: (u16, u16),
    size: (u8, u8),
    offsets: (i8, i8),
}

static HAS_PRINTED: std::sync::atomic::AtomicBool = std::sync::atomic::AtomicBool::new(false);

impl Font {
    pub fn load(texture: TextureRect, texture_size: IVec2, metrics_data: &[u8]) -> Result<Self> {
        let mut cursor = std::io::Cursor::new(metrics_data);
        let mut buf = [0u8; GlyphMetrics::BINARY_ENTRY_SIZE];

        let mut all_glyph_metrics = HashMap::new();
        while cursor.read_exact(&mut buf).is_ok() {
            let (ch, glyph_metrics) = GlyphMetrics::from_bytes(&buf)?;
            all_glyph_metrics.insert(ch, glyph_metrics);
        }
        Ok(Self {
            texture,
            texture_size,
            all_glyph_metrics,
        })
    }

    pub fn render_left_aligned_text<BufferType: RenderedUiBuffer>(
        &self,
        text: impl AsRef<str>,
        position: IVec3,
        scale: i32,
        buffer: &mut BufferType,
    ) {
        type QuadType<BufferType> = <BufferType as RenderedUiBuffer>::QuadType;
        type VertexType<BufferType> =
            <<BufferType as RenderedUiBuffer>::QuadType as RenderedUiQuad>::VertexType;

        let mut current_position = position + ivec3(0, -16 * scale, 0);
        let has_printed = HAS_PRINTED.swap(true, std::sync::atomic::Ordering::Relaxed);
        for ch in text.as_ref().chars() {
            if ch == ' ' {
                current_position.x += 10;
                continue;
            }

            let Some(metrics) = self.all_glyph_metrics.get(&ch) else {
                continue;
            };

            let glyph_position = current_position
                + ivec3(metrics.offsets.0 as i32, metrics.offsets.1 as i32, 0) * scale;
            current_position.x += (metrics.size.0 as i32 + metrics.offsets.0 as i32) * scale;
            let scale = ivec3(metrics.size.0 as i32, metrics.size.1 as i32, 0) * scale;

            let glyph_texture = self.texture.view(
                vec2(
                    metrics.texture_offsets.0 as f32,
                    metrics.texture_offsets.1 as f32,
                ) / self.texture_size.as_vec2(),
                vec2(metrics.size.0 as f32, metrics.size.1 as f32) / self.texture_size.as_vec2(),
            );
            let glyph_texture_coords = glyph_texture.to_face_texture_coords();

            if has_printed == false {
                dbg!(ch, &glyph_position, &scale);
            }

            let quad = Face::NegZ.to_int_unit_quad();
            let vertices = [0, 1, 2, 3].map(|i| {
                let v = quad[i] * scale + glyph_position;
                let uv = glyph_texture_coords[i];
                <VertexType<BufferType> as RenderedUiVertex>::new(v, Vec3::ONE, uv)
            });
            let quad = <QuadType<BufferType> as RenderedUiQuad>::new(&vertices);

            buffer.add_quad(quad);
        }
    }
}

impl GlyphMetrics {
    const BINARY_ENTRY_SIZE: usize = 4 + 2 + 2 + 1 + 1 + 1 + 1;

    pub fn from_bytes(buf: &[u8]) -> Result<(char, Self)> {
        assert_eq!(buf.len(), Self::BINARY_ENTRY_SIZE);
        let ch = u32::from_be_bytes(buf[0..4].try_into()?);
        let ch =
            char::from_u32(ch).context("font metrics should not contain invalid codepoints")?;
        let texture_offsets = (
            u16::from_be_bytes(buf[4..6].try_into()?),
            u16::from_be_bytes(buf[6..8].try_into()?),
        );
        let size = (buf[8], buf[9]);
        let offsets = (
            i8::from_be_bytes(buf[10..11].try_into()?),
            i8::from_be_bytes(buf[11..12].try_into()?),
        );
        Ok((
            ch,
            Self {
                texture_offsets,
                size,
                offsets,
            },
        ))
    }
}
