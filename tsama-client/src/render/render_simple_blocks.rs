use enum_map::enum_map;
use glam::{ivec2, vec3};
use tsama_core::{
    types::{BlockLocation, Face, LocalBlockLocation},
    BlockAir, BlockGlass, BlockGrass, BlockStone, BlockWater, World,
};

use crate::display::{RenderTarget, RenderedBlockBuffer, TextureRect};

use super::{render_solid_block, RenderBlock};

impl RenderBlock for BlockAir {}

impl RenderBlock for BlockGrass {
    fn render<BufferType: RenderedBlockBuffer>(
        &self,
        location: BlockLocation,
        _local_location: LocalBlockLocation,
        world: &World,
        buffer: &mut BufferType,
        block_textures: &[TextureRect],
    ) {
        let top_texture = block_textures[0];
        // 94c048;
        render_solid_block(
            location,
            world,
            buffer,
            enum_map! {
                Face::PosX => top_texture,
                Face::NegX => top_texture,
                Face::PosY => top_texture,
                Face::NegY => top_texture,
                Face::PosZ => top_texture,
                Face::NegZ => top_texture,
            },
            vec3(0.58, 0.75, 0.28),
        );
    }

    fn register_textures<TargetType: RenderTarget>(
        &self,
        target: &mut TargetType,
    ) -> Vec<TextureRect> {
        [target.register_texture(include_bytes!("../../assets/GrassTop.png"), ivec2(16, 16))].into()
    }
}

impl RenderBlock for BlockStone {
    fn render<BufferType: RenderedBlockBuffer>(
        &self,
        location: BlockLocation,
        _local_location: LocalBlockLocation,
        world: &World,
        buffer: &mut BufferType,
        block_textures: &[TextureRect],
    ) {
        let top_texture = block_textures[0];
        render_solid_block(
            location,
            world,
            buffer,
            enum_map! {
                Face::PosX => top_texture,
                Face::NegX => top_texture,
                Face::PosY => top_texture,
                Face::NegY => top_texture,
                Face::PosZ => top_texture,
                Face::NegZ => top_texture,
            },
            vec3(1.0, 1.0, 1.0),
        );
    }

    fn register_textures<TargetType: RenderTarget>(
        &self,
        target: &mut TargetType,
    ) -> Vec<TextureRect> {
        [target.register_texture(include_bytes!("../../assets/Basalt Raw.png"), ivec2(16, 16))]
            .into()
    }
}

impl RenderBlock for BlockWater {
    fn render_transparent<BufferType: RenderedBlockBuffer>(
        &self,
        location: BlockLocation,
        world: &World,
        buffer: &mut BufferType,
        block_textures: &[TextureRect],
    ) {
        let texture = block_textures[0];
        render_solid_block(
            location,
            world,
            buffer,
            enum_map! {
                    Face::PosX => texture,
                    Face::NegX => texture,
                    Face::PosY => texture,
                    Face::NegY => texture,
                    Face::PosZ => texture,
                    Face::NegZ => texture,
            },
            vec3(1.0, 1.0, 1.0),
        );
    }

    fn register_textures<TargetType: RenderTarget>(
        &self,
        target: &mut TargetType,
    ) -> Vec<TextureRect> {
        [target.register_texture(include_bytes!("../../assets/Water.png"), ivec2(16, 16))].into()
    }
}

impl RenderBlock for BlockGlass {
    fn render_transparent<BufferType: RenderedBlockBuffer>(
        &self,
        location: BlockLocation,
        world: &World,
        buffer: &mut BufferType,
        block_textures: &[TextureRect],
    ) {
        let texture = block_textures[0];
        render_solid_block(
            location,
            world,
            buffer,
            enum_map! {
                    Face::PosX => texture,
                    Face::NegX => texture,
                    Face::PosY => texture,
                    Face::NegY => texture,
                    Face::PosZ => texture,
                    Face::NegZ => texture,
            },
            vec3(1.0, 1.0, 1.0),
        );
    }

    fn register_textures<TargetType: RenderTarget>(
        &self,
        target: &mut TargetType,
    ) -> Vec<TextureRect> {
        [target.register_texture(include_bytes!("../../assets/Glass.png"), ivec2(16, 16))].into()
    }
}
