#[macro_use]
mod api;
pub use api::*;

mod internal;
pub use internal::*;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_quad_type_layout() {
        assert_eq!(
            6 * std::mem::size_of::<Vertex>(),
            std::mem::size_of::<Quad>(),
        );
        assert_eq!(std::mem::align_of::<Vertex>(), std::mem::align_of::<Quad>(),);
    }
}
