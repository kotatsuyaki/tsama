use glam::{ivec3, vec3};
use itertools::{iproduct, Itertools};
use std::{
    collections::{HashMap, HashSet},
    ops::ControlFlow,
};
use tracing::{error, info, info_span, warn};
use tsama_core::{
    consts::VISIBLE_DISTANCE,
    entity_types::{EntityId, EntityMotion},
    types::{BoundingBox, ChunkLocation, Position, SegmentLocation, WorldSize},
    BlockKind, Chunk, ClientMessage, EntityKind, MovementSign, Player, PlayerPartialStepConfig,
    PlayerStepConfig, SendPlayerMessage, ServerMessage, World, WorldPartialStepConfig,
    WorldStepConfig,
};
use winit::{event::MouseButton, keyboard::KeyCode};

use crate::{
    consts::SEGMENTS_RENDERED_PER_FRAME,
    display::{
        BlockBufferKind, CameraConfig, InputSource, RenderTarget, RenderedEntityDebugBuffer,
        TextureRect,
    },
    network_client::Remote,
    render::{Font, RenderBlock, RenderEntity},
};

pub struct Game {
    world: World,
    login_status: LoginStatus,
    dirty_segment_locations: HashSet<SegmentLocation>,
    player_entity_id: Option<EntityId>,
    player_interpolated_motion: Option<EntityMotion>,
    player_motion: Option<EntityMotion>,
    removed_chunks: HashSet<ChunkLocation>,
    font: Option<Font>,
    block_textures_map: HashMap<BlockKind, Vec<TextureRect>>,
    entity_textures_map: HashMap<EntityKind, Vec<TextureRect>>,
}

#[derive(Debug, PartialEq, Eq)]
enum LoginStatus {
    Initial,
    LoginSent,
    LoggedIn,
}

impl Game {
    pub fn new() -> Self {
        Self {
            world: World::new(WorldSize::from_radius_in_chunks(64)),
            login_status: LoginStatus::Initial,
            dirty_segment_locations: HashSet::new(),
            player_entity_id: None,
            player_interpolated_motion: None,
            player_motion: None,
            removed_chunks: HashSet::new(),
            font: None,
            block_textures_map: HashMap::new(),
            entity_textures_map: HashMap::new(),
        }
    }

    pub fn run_step<RemoteType: Remote, InputSourceType: InputSource>(
        &mut self,
        remote: &mut RemoteType,
        input_source: &mut InputSourceType,
    ) {
        if let ControlFlow::Break(()) = self.update_login_status(remote) {
            return;
        }

        self.handle_all_server_messages(remote);
        self.update_world_step(remote, input_source);
        self.send_player_motion(remote);
    }

    pub fn run_partial_step<InputSourceType: InputSource>(
        &mut self,
        input_source: &mut InputSourceType,
        _remainder_frame_fraction: f64,
    ) {
        if self.login_status != LoginStatus::LoggedIn {
            return;
        }

        self.update_world_partial_step(input_source);
    }

    fn update_world_step<RemoteType: Remote, InputSourceType: InputSource>(
        &mut self,
        remote: &mut RemoteType,
        input_source: &mut InputSourceType,
    ) {
        if input_source.is_key_down(KeyCode::KeyI) {
            self.log_debug_info();
        }

        let world_step_config = WorldStepConfig {
            player_config: Some(self.player_step_config(remote, input_source)),
            should_update_entities: false,
        };
        self.world.step(&world_step_config);

        input_source.clear_key_downs();
        input_source.clear_cursor_delta();
    }

    fn log_debug_info(&mut self) {
        let motion = self.world.entity_motion(self.player_entity_id());
        let chunk_location = self
            .world
            .entity_position(self.player_entity_id())
            .unwrap()
            .chunk_location();
        let segment_location = self
            .world
            .entity_position(self.player_entity_id())
            .unwrap()
            .segment_location();

        info!("Player entity motion = {:.03?}", motion);
        info!("Player entity chunk = {:?}", chunk_location);
        info!("Player entity segment = {:?}", segment_location);
    }

    fn update_world_partial_step<InputSourceType: InputSource>(
        &mut self,
        input_source: &mut InputSourceType,
    ) {
        let world_step_config = WorldPartialStepConfig {
            player_config: Some(self.player_partial_step_config(input_source)),
        };
        self.world.step_partial(&world_step_config);
        input_source.clear_cursor_delta();
    }

    pub fn send_player_motion<RemoteType: Remote>(&self, remote: &mut RemoteType) {
        let player_motion = self
            .world
            .entity_motion(self.player_entity_id())
            .unwrap()
            .clone();
        let message = ClientMessage::PlayerMotion {
            id: self.player_entity_id(),
            motion: player_motion,
        };
        if let Err(err) = remote.send_message(message) {
            warn!("Failed to send player motion to server: {:?}", err);
        }
    }

    pub fn player_step_config<'s, Sender: SendPlayerMessage, InputSourceType: InputSource>(
        &self,
        sender: &'s Sender,
        input_source: &InputSourceType,
    ) -> PlayerStepConfig<'s, Sender> {
        let is_pressed = |key: KeyCode| input_source.is_key_held(key);

        let is_front_pressed = is_pressed(KeyCode::KeyW) || is_pressed(KeyCode::ArrowUp);
        let is_back_pressed = is_pressed(KeyCode::KeyS) || is_pressed(KeyCode::ArrowDown);
        let player_movement_front = match (is_front_pressed, is_back_pressed) {
            (true, false) => MovementSign::Pos,
            (false, true) => MovementSign::Neg,
            _ => MovementSign::Zero,
        };

        let is_right_pressed = is_pressed(KeyCode::KeyD) || is_pressed(KeyCode::ArrowRight);
        let is_left_pressed = is_pressed(KeyCode::KeyA) || is_pressed(KeyCode::ArrowLeft);
        let player_movement_right = match (is_right_pressed, is_left_pressed) {
            (true, false) => MovementSign::Pos,
            (false, true) => MovementSign::Neg,
            _ => MovementSign::Zero,
        };

        let is_jump_pressed = is_pressed(KeyCode::Space);
        let is_sneak_pressed = is_pressed(KeyCode::ShiftLeft);

        PlayerStepConfig {
            entity_id: self.player_entity_id(),
            front: player_movement_front,
            right: player_movement_right,
            jump: is_jump_pressed,
            sneak: is_sneak_pressed,
            yaw: input_source.cursor_delta().0 as f32,
            pitch: input_source.cursor_delta().1 as f32,
            attack: input_source.is_mouse_down(MouseButton::Left),
            dig: input_source.is_mouse_held(MouseButton::Left),
            utilize: input_source.is_mouse_down(MouseButton::Right),
            sender,
        }
    }

    pub fn player_partial_step_config<InputSourceType: InputSource>(
        &self,
        input_source: &InputSourceType,
    ) -> PlayerPartialStepConfig {
        PlayerPartialStepConfig {
            entity_id: self.player_entity_id(),
            yaw: input_source.cursor_delta().0 as f32,
            pitch: input_source.cursor_delta().1 as f32,
        }
    }

    pub fn handle_all_server_messages<RemoteType: Remote>(&mut self, remote: &mut RemoteType) {
        while let Ok(message) = remote.receive_message() {
            self.handle_server_message(message, remote);
        }
    }

    pub fn handle_server_message<RemoteType: Remote>(
        &mut self,
        message: ServerMessage,
        _remote: &mut RemoteType,
    ) {
        match message {
            ServerMessage::LoginSuccess { .. } => {
                panic!("handle_server_message got LoginSuccess, which may be a server bug");
            }
            ServerMessage::ChunkLoad { chunk } => {
                let location = chunk.location();
                self.set_chunk_as_dirty(location);
                self.world.add_chunk(chunk);
                self.removed_chunks.remove(&location);
            }
            ServerMessage::ChunkUnload { location } => {
                self.unset_chunk_as_dirty(location);
                self.world.remove_chunk(location);
                self.removed_chunks.insert(location);
            }
            ServerMessage::EntityLoad { id, kind, motion } => {
                self.world.create_entity_with_id(id, kind, motion);
            }
            ServerMessage::EntityUnload { id } => {
                self.world.destroy_entity(id);
            }
            ServerMessage::EntityMotion { id, motion } => {
                if let Err(err) = self.world.set_entity_motion(id, motion) {
                    warn!("Failed to set entity motion: {err}");
                }
            }
            ServerMessage::SetBlock { location, kind } => {
                self.world.set_block_kind(location, kind);
                self.set_segment_as_dirty(location.segment_location());
            }
        }
    }

    fn set_chunk_as_dirty(&mut self, location: ChunkLocation) {
        for segment_location in location.segment_locations() {
            self.dirty_segment_locations.insert(segment_location);
        }
    }

    fn set_segment_as_dirty(&mut self, location: SegmentLocation) {
        self.dirty_segment_locations.insert(location);
    }

    fn unset_chunk_as_dirty(&mut self, location: ChunkLocation) {
        for segment_location in location.segment_locations() {
            self.dirty_segment_locations.remove(&segment_location);
        }
    }

    fn update_login_status<RemoteType: Remote>(
        &mut self,
        remote: &mut RemoteType,
    ) -> ControlFlow<()> {
        match self.login_status {
            LoginStatus::Initial => {
                send_login_message(remote);
                self.login_status = LoginStatus::LoginSent;
                ControlFlow::Break(())
            }
            LoginStatus::LoginSent => {
                // Try to receive the login success confirmation, otherwise return.
                let Ok(message) = remote.receive_message() else {
                    return ControlFlow::Break(());
                };

                // Check that the message is the expected confirmation.
                let ServerMessage::LoginSuccess {
                    player_entity_id,
                    player_motion,
                } = message
                else {
                    error!("Unexpected first message received from the server");
                    std::process::exit(1);
                };

                info!("Creating player entity with id {:?}", player_entity_id);
                self.player_entity_id = Some(player_entity_id);
                self.world.create_entity_with_id(
                    player_entity_id,
                    EntityKind::Player(Player),
                    player_motion,
                );

                info!("Successfully logged in");
                self.login_status = LoginStatus::LoggedIn;
                ControlFlow::Continue(())
            }
            LoginStatus::LoggedIn => ControlFlow::Continue(()),
        }
    }

    pub fn render_world<TargetType: RenderTarget>(
        &mut self,
        target: &mut TargetType,
        remainder_frame_fraction: f64,
        fps: usize,
    ) {
        info_span!("update_misc").in_scope(|| {
            self.update_cached_player_motion(remainder_frame_fraction);
            self.update_camera_config(target);
        });

        info_span!("render_blocks").in_scope(|| {
            self.cleanup_block_buffers(target);
            self.render_dirty_segments(target);
        });

        info_span!("render_entities").in_scope(|| {
            self.cleanup_entity_buffers(target);
            self.render_entities(target, remainder_frame_fraction);
        });

        info_span!("render_ui").in_scope(|| {
            self.cleanup_ui_buffers(target);
            self.render_ui(target, fps);
        });
    }

    fn render_entities<TargetType: RenderTarget>(
        &mut self,
        target: &mut TargetType,
        remainder_frame_fraction: f64,
    ) {
        if self.player_entity_id.is_none() {
            return;
        }
        let player_chunk_location = self
            .world
            .entity_position(self.player_entity_id())
            .unwrap()
            .chunk_location();

        for (entity_id, _entity_kind) in self.world.entity_kinds() {
            let entity_chunk_location = self
                .world
                .entity_position(entity_id)
                .unwrap()
                .chunk_location();
            let unwrapped_entity_chunk_location =
                entity_chunk_location.unwrap_near(player_chunk_location, self.world.size());
            let is_entity_close_enough = unwrapped_entity_chunk_location
                .distance_squared(player_chunk_location)
                <= VISIBLE_DISTANCE.pow(2);
            let is_entity_the_player = self.player_entity_id.unwrap() == entity_id;
            let should_render = is_entity_close_enough && !is_entity_the_player;
            if should_render == false {
                continue;
            }
            self.render_entity(entity_id, target, remainder_frame_fraction);
        }
    }

    fn render_entity<TargetType: RenderTarget>(
        &self,
        entity_id: EntityId,
        target: &mut TargetType,
        remainder_frame_fraction: f64,
    ) {
        // Get the interpolated motion of both the entity and the player.
        let motion = self
            .world
            .interlopated_entity_motion(entity_id, remainder_frame_fraction)
            .unwrap();
        let player_motion = self
            .player_interpolated_motion
            .as_ref()
            .expect("self.player_interpolated_motion is expected to be non-empty in render_entity");
        let eye_position = Player::eye_position_at(player_motion.position());

        let entity_debug_buffer = target.get_or_create_entity_debug_buffer();
        self.render_entity_debug_outline(motion.bounding_box(), eye_position, entity_debug_buffer);

        let entity_buffer = target.get_or_create_entity_buffer();
        let entity_kind = self.world.entity_kind(entity_id).unwrap();
        let entity_textures = self.entity_textures_map.get(&entity_kind).unwrap();
        entity_kind.render(
            motion,
            eye_position,
            &self.world,
            entity_buffer,
            entity_textures,
        );
    }

    fn render_entity_debug_outline<BufferType: RenderedEntityDebugBuffer>(
        &self,
        bounding_box: BoundingBox,
        eye_position: Position,
        entity_debug_buffer: &mut BufferType,
    ) {
        // Get the shortest offset from the eye to the bounding box.
        let min = bounding_box
            .min()
            .sub_shortest(eye_position, self.world.size())
            .as_vec3();
        let max = bounding_box
            .max()
            .sub_shortest(eye_position, self.world.size())
            .as_vec3();

        // Render four lines, each containing two vertices, for each direction along the axes.
        for (x, z) in iproduct!([min.x, max.x], [min.z, max.z]) {
            entity_debug_buffer.add_vertex(vec3(x, min.y, z));
            entity_debug_buffer.add_vertex(vec3(x, max.y, z));
        }

        for (x, y) in iproduct!([min.x, max.x], [min.y, max.y]) {
            entity_debug_buffer.add_vertex(vec3(x, y, min.z));
            entity_debug_buffer.add_vertex(vec3(x, y, max.z));
        }

        for (y, z) in iproduct!([min.y, max.y], [min.z, max.z]) {
            entity_debug_buffer.add_vertex(vec3(min.x, y, z));
            entity_debug_buffer.add_vertex(vec3(max.x, y, z));
        }
    }

    /// Renders dirty (i.e. re-render required) chunks to `target`.
    fn render_dirty_segments<TargetType: RenderTarget>(&mut self, target: &mut TargetType) {
        let Some(player_entity_id) = self.player_entity_id else {
            return;
        };
        let player_segment_location = self
            .world
            .entity_position(player_entity_id)
            .unwrap()
            .segment_location();

        // Get a list of segments that are ready (i.e. its surrounding chunks are all loaded) and
        // dirty (i.e. it's current contents are not yet rendered).
        let mut ready_dirty_segments: Vec<_> = self
            .world
            .chunk_locations()
            .filter(|&chunk_location| self.should_render_chunk(chunk_location))
            .flat_map(|chunk_location| chunk_location.segment_locations().collect_vec())
            .filter(|&segment_location| self.is_segment_dirty(segment_location))
            .collect();

        // Sort by the distances to the player.
        ready_dirty_segments.sort_by_key(|&segment_location| {
            segment_location.distance_squared_shortest(player_segment_location, self.world.size())
        });

        // Limit the number of segments rendered per frame.
        if ready_dirty_segments.len() > SEGMENTS_RENDERED_PER_FRAME {
            ready_dirty_segments.resize(SEGMENTS_RENDERED_PER_FRAME, SegmentLocation::new(0, 0, 0))
        }

        // Render the segments and remove them from the dirty list.
        for segment_location in ready_dirty_segments {
            info_span!("render_segment").in_scope(|| {
                let chunk = self
                    .world
                    .chunk(segment_location.chunk_location())
                    .expect("Missing chunk");
                self.render_segment(chunk, segment_location, target);
            });
            self.dirty_segment_locations.remove(&segment_location);
        }
    }

    /// Removes block buffers corresponding to the removed chunks from `target`.
    ///
    /// This function also clears `self.removed_chunks`.
    fn cleanup_block_buffers<TargetType: RenderTarget>(&mut self, target: &mut TargetType) {
        for chunk_location in self.removed_chunks.iter() {
            for segment_location in chunk_location.segment_locations() {
                target.remove_block_buffer(segment_location, BlockBufferKind::Opaque);
                target.remove_block_buffer(segment_location, BlockBufferKind::Transparent);
            }
        }
        self.removed_chunks.clear();
    }

    fn cleanup_entity_buffers<TargetType: RenderTarget>(&mut self, target: &mut TargetType) {
        target.remove_entity_buffers();
    }

    fn cleanup_ui_buffers<TargetType: RenderTarget>(&mut self, target: &mut TargetType) {
        target.remove_ui_buffer();
    }

    /// Updates the camera config of `target` to the player's eye position and angles.
    ///
    /// This function uses the cached player motion.
    /// Call `Game::update_cached_player_motion` to update the cached player motion.
    fn update_camera_config<TargetType: RenderTarget>(&self, target: &mut TargetType) {
        let (Some(motion), Some(interpolated_motion)) =
            (&self.player_motion, &self.player_interpolated_motion)
        else {
            return;
        };

        let eye_position = Player::eye_position_at(interpolated_motion.position());
        let pitch = motion.facing().pitch();
        let yaw = motion.facing().yaw();

        target.update_camera_config(CameraConfig::new(eye_position.as_inner(), pitch, yaw));
    }

    fn render_segment<TargetType: RenderTarget>(
        &self,
        chunk: &Chunk,
        location: SegmentLocation,
        target: &mut TargetType,
    ) {
        target.remove_block_buffer(location, BlockBufferKind::Opaque);
        target.remove_block_buffer(location, BlockBufferKind::Transparent);

        let solid_block_buffer =
            target.get_or_create_block_buffer(location, BlockBufferKind::Opaque);

        for (global_location, local_location) in location.global_and_local_block_locations() {
            let block_kind = chunk.block_kind(local_location);
            let block_textures = self
                .block_textures_map
                .get(&block_kind)
                .expect("A block kind does not have its texture registered");
            block_kind.render(
                global_location,
                local_location,
                &self.world,
                solid_block_buffer,
                block_textures,
            );
        }

        let transparent_block_buffer =
            target.get_or_create_block_buffer(location, BlockBufferKind::Transparent);
        for (global_location, local_location) in location.global_and_local_block_locations() {
            let block_kind = chunk.block_kind(local_location);
            let block_textures = self
                .block_textures_map
                .get(&block_kind)
                .expect("A block kind does not have its texture registered");
            block_kind.render_transparent(
                global_location,
                &self.world,
                transparent_block_buffer,
                block_textures,
            );
        }
    }

    fn should_render_chunk(&self, location: ChunkLocation) -> bool {
        let neighbor_chunk_locations: Vec<_> = iproduct!(-1..1, -1..1)
            .map(|(x, z)| location.offset(x, z))
            .map(|loc| loc.wrap(self.world.size()))
            .collect();
        let is_neighbor_all_loaded = neighbor_chunk_locations
            .iter()
            .cloned()
            .all(|loc| self.world.chunk(loc).is_some());
        is_neighbor_all_loaded
    }

    fn is_segment_dirty(&self, location: SegmentLocation) -> bool {
        self.dirty_segment_locations.contains(&location)
    }

    fn player_entity_id(&self) -> EntityId {
        self.player_entity_id.unwrap()
    }

    pub fn register_fonts_and_textures<TargetType: RenderTarget>(
        &mut self,
        target: &mut TargetType,
    ) {
        self.register_fonts(target);
        self.register_block_and_entity_textures(target);

        target.upload_textures();
    }

    fn register_fonts<TargetType: RenderTarget>(&mut self, target: &mut TargetType) {
        let (texture, texture_size) =
            target.register_texture_with_unknown_size(include_bytes!("../assets/unifont.png"));
        let font = Font::load(
            texture,
            texture_size,
            include_bytes!("../assets/unifont.metrics.bin"),
        )
        .expect("font load should succeed");
        self.font = Some(font);
    }

    fn register_block_and_entity_textures<TargetType: RenderTarget>(
        &mut self,
        target: &mut TargetType,
    ) {
        for kind in BlockKind::iter() {
            let block_textures = kind.register_textures(target);
            self.block_textures_map.insert(kind, block_textures);
        }

        for kind in EntityKind::iter() {
            let entity_textures = kind.register_textures(target);
            self.entity_textures_map.insert(kind, entity_textures);
        }
    }

    fn update_cached_player_motion(&mut self, remainder_frame_fraction: f64) {
        let Some(player_entity_id) = self.player_entity_id else {
            return;
        };

        let motion = self.world.entity_motion(player_entity_id).unwrap();
        let interpolated_motion = self
            .world
            .interlopated_entity_motion(player_entity_id, remainder_frame_fraction)
            .unwrap();

        self.player_motion = Some(motion.clone());
        self.player_interpolated_motion = Some(interpolated_motion);
    }

    fn render_ui<TargetType: RenderTarget>(&mut self, target: &mut TargetType, fps: usize) {
        let ui_buffer = target.get_or_create_ui_buffer();
        let font = self.font.as_ref().unwrap();

        font.render_left_aligned_text(format!("FPS: {}", fps), ivec3(0, 0, 0), 1, ui_buffer);
    }
}

fn send_login_message<RemoteType: Remote>(remote: &mut RemoteType) {
    info!("Sending a login message");
    remote
        .send_message(ClientMessage::Login {
            player_name: "kotatsuyaki".to_string(),
        })
        .expect("Failed to send the login message");
}
