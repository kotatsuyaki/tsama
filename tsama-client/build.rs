use std::io::Write;

use image::GenericImageView;

fn main() {
    println!("cargo::rerun-if-changed=./assets/unifont.otb");

    eprintln!("Building font texture");
    let start = std::time::Instant::now();

    let font_data = include_bytes!("./assets/unifont.otb");
    let face = ttf_parser::Face::parse(font_data, 0).unwrap();

    let mut buffer = image::RgbaImage::new(8192, 8192);
    let mut metrics_file =
        std::io::BufWriter::new(std::fs::File::create("./assets/unifont.metrics.bin").unwrap());
    let mut current_top_y = 0;
    let mut current_left_x = 0;
    let mut count = 0;

    dbg!(
        face.tables().sbix.is_some(),
        face.tables().bdat.is_some(),
        face.tables().ebdt.is_some(),
        face.tables().cbdt.is_some(),
    );

    for subtable in face
        .tables()
        .cmap
        .expect("font should have cmap table")
        .subtables
    {
        subtable.codepoints(|ch| {
            let ch = char::from_u32(ch).expect("font file should not contain invalid codepoints");
            let glyph_image = face
                .glyph_raster_image(
                    face.glyph_index(ch)
                        .expect("font face should contain the specified codepoints"),
                    std::u16::MAX,
                )
                .unwrap();
            const EXPECTED_HEIGHT: u16 = 16;
            assert_eq!(glyph_image.pixels_per_em, EXPECTED_HEIGHT);

            let glyph_width = if glyph_image.width > 8 { 16 } else { 8 };

            let (width, height) = (glyph_image.width as i16, glyph_image.height as i16);
            let (offset_x, offset_y) = (glyph_image.x, glyph_image.y);
            assert!(
                offset_x + width <= 16,
                "offset_x + width = {} > 16",
                offset_x + width
            );
            assert!(height <= 16, "height = {height} > 16");

            let bit_at = |x: i16, y: i16| {
                let bit_index = y * width + x;
                glyph_image.data[(bit_index / 8) as usize] & (0b10000000 >> (bit_index % 8))
            };

            if current_left_x + glyph_width > 8192 {
                current_top_y += 16;
                current_left_x = 0;
            }
            for y in 0..height {
                for x in 0..width {
                    count += 1;
                    if bit_at(x, y) != 0 {
                        let img_x = (current_left_x + x) as u16;
                        let img_y = (current_top_y + y) as u16;

                        buffer.put_pixel(img_x as u32, img_y as u32, image::Rgba::<u8>([255; 4]));
                    }
                }
            }
            metrics_file.write_all(&(ch as u32).to_be_bytes()).unwrap();
            metrics_file
                .write_all(&(current_left_x as u16).to_be_bytes())
                .unwrap();
            metrics_file
                .write_all(&(current_top_y as u16).to_be_bytes())
                .unwrap();
            metrics_file
                .write_all(&(width as i8).to_be_bytes())
                .unwrap();
            metrics_file
                .write_all(&(height as i8).to_be_bytes())
                .unwrap();
            metrics_file
                .write_all(&(offset_x as i8).to_be_bytes())
                .unwrap();
            metrics_file
                .write_all(&(offset_y as i8).to_be_bytes())
                .unwrap();

            current_left_x += glyph_width;
        });
    }

    let cropped_buffer = buffer
        .view(0, 0, 8192, current_top_y as u32 + 16)
        .to_image();

    let mut font_image_file =
        std::io::BufWriter::new(std::fs::File::create("./assets/unifont.png").unwrap());
    cropped_buffer
        .write_with_encoder(image::codecs::png::PngEncoder::new_with_quality(
            &mut font_image_file,
            image::codecs::png::CompressionType::Best,
            image::codecs::png::FilterType::NoFilter,
        ))
        .unwrap();

    font_image_file.flush().unwrap();
    metrics_file.flush().unwrap();

    let end = std::time::Instant::now();
    eprintln!("Put {count} pixels in {} seconds", (end - start).as_secs());
}
