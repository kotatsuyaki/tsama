#![allow(clippy::bool_comparison)]

pub mod hash;
pub mod noise;
pub mod consts {
    pub const CELL_SIZE: i32 = 1024;
}

pub mod mapgen {
    use std::collections::{HashMap, HashSet, VecDeque};

    use glam::{dvec2, DVec2, IVec2};
    use tracing::info;
    use tsama_core::types::{HorizontalPosition, WorldSize};

    use crate::{
        hex::Center,
        noise::{Noise2d, Perlin2d},
    };

    pub struct IslandMap {
        pub key: IVec2,
        pub centers: Vec<Center>,
        pub centroid: Center,
        pub center_attrs: HashMap<Center, CenterAttributes>,
        pub max_dist_to_border: i32,
        _radius: i32,
    }

    #[derive(Default)]
    pub struct CenterAttributes {
        pub is_border: bool,
        pub kind: HexKind,
        pub dist_to_border: i32,
    }

    impl CenterAttributes {
        fn new() -> Self {
            Self {
                dist_to_border: i32::MAX,
                ..Default::default()
            }
        }
    }

    #[derive(Default)]
    pub enum HexKind {
        #[default]
        Ocean,
        Land,
    }

    impl IslandMap {
        pub fn build(
            from_key: IVec2,
            from_center: Center,
            is_in_island: impl Fn(Center) -> bool,
        ) -> Self {
            let mut visited = HashSet::new();
            let mut centers = Vec::new();

            let mut queue = VecDeque::new();
            queue.push_back(from_center);
            visited.insert(from_center);

            while let Some(center) = queue.pop_front() {
                if is_in_island(center) == false {
                    continue;
                }

                centers.push(center);
                for neighbor in center.neighbors() {
                    if visited.contains(&neighbor) == false {
                        visited.insert(neighbor);
                        queue.push_back(neighbor);
                    }
                }
            }

            let centers: Vec<_> = centers
                .into_iter()
                .collect::<HashSet<_>>()
                .into_iter()
                .collect();
            let center_attrs = centers
                .iter()
                .cloned()
                .map(|center| (center, CenterAttributes::new()))
                .collect();

            let centroid = Self::find_centroid(&centers);

            let mut map = Self {
                key: from_key,
                centers,
                centroid,
                center_attrs,
                max_dist_to_border: 0,
                _radius: 128,
            };
            map.fill_hex_kinds();
            map
        }

        fn find_centroid(centers: &[Center]) -> Center {
            let first_center = centers[0];

            let count = centers.len() as f64;
            let mut centroid_point = DVec2::ZERO;

            for center in centers {
                let point = HorizontalPosition::from(center.to_point())
                    .unwrap_near(
                        HorizontalPosition::from(first_center.to_point()),
                        WorldSize::from_radius_in_chunks(128),
                    )
                    .as_inner();
                centroid_point += point / count;
            }
            let centroid_point = HorizontalPosition::from(centroid_point)
                .wrap(WorldSize::from_radius_in_chunks(128))
                .as_inner();

            info!("Centroid point = {centroid_point}");

            Center::from_point(centroid_point).wrap()
        }

        fn fill_hex_kinds(&mut self) {
            let centers: HashSet<_> = self.centers.iter().cloned().collect();
            let mut queue = VecDeque::new();
            let mut visited = HashSet::new();

            // Mark border hexes as borders and set their distance to border to 0.
            // The queue is also populated with the border centers.
            for &center in self.centers.iter() {
                let are_neighbors_in_island = center
                    .neighbors()
                    .all(|neighbor| centers.contains(&neighbor));
                let is_border = are_neighbors_in_island == false;
                if is_border {
                    let attrs = self.center_attrs.get_mut(&center).unwrap();
                    attrs.is_border = true;
                    attrs.dist_to_border = 0;
                    queue.push_back(center);
                    visited.insert(center);
                }
            }

            // Calculate the minimal distance to border for all centers.
            while let Some(center) = queue.pop_front() {
                for neighbor in center.neighbors() {
                    if centers.contains(&neighbor) == false {
                        continue;
                    }

                    if visited.contains(&neighbor) == false {
                        queue.push_back(neighbor);
                        visited.insert(neighbor);
                    }

                    let neighbor_attrs = self.center_attrs.get(&neighbor).unwrap();
                    let neighbor_dist_to_border = neighbor_attrs.dist_to_border;

                    let attrs = self.center_attrs.get_mut(&center).unwrap();
                    attrs.dist_to_border = attrs
                        .dist_to_border
                        .min(neighbor_dist_to_border.saturating_add(1));
                }
            }

            // Find the max dist to border and save it.
            let mut max_dist_to_border = 0;
            for attrs in self.center_attrs.values() {
                max_dist_to_border = max_dist_to_border.max(attrs.dist_to_border);
            }
            self.max_dist_to_border = max_dist_to_border;

            // Set hex kinds.
            // The noise has to:
            // 1. Be periodic over 2pi.
            // 2. Be reasonably jagged.
            // 3. Be a value between 0 and 1.
            let noise = Perlin2d::new(
                60,
                self.centroid.q() as u64 + self.centroid.r() as u64 * 7981263,
                1.0,
            );
            let mut avg = 0.0;
            for center in self.centers.iter() {
                let offset_from_centroid =
                    center.unwrap_near(self.centroid).to_point() - self.centroid.to_point();
                let angle = offset_from_centroid
                    .y
                    .atan2(offset_from_centroid.x)
                    .to_degrees()
                    / 6.0;
                let value = (noise.get(dvec2(angle, 0.5)) * 0.5 + 1.0) * 0.6;
                avg += value / self.centers.len() as f64;
                let dist = 1.0
                    - self.center_attrs[center].dist_to_border as f64
                        / self.max_dist_to_border as f64;
                if dist > value {
                    self.center_attrs.get_mut(center).unwrap().kind = HexKind::Land;
                }
            }
            info!("avg = {avg}");
        }
    }
}

pub mod hex {
    use glam::{dvec2, ivec2, DMat2, DVec2, IVec2};

    #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
    pub struct Center {
        value: IVec2,
    }

    impl Center {
        pub fn new(q: i32, r: i32) -> Self {
            Self { value: ivec2(q, r) }
        }

        pub fn from_ivec2(value: IVec2) -> Self {
            Self { value }
        }

        pub fn as_ivec2(self) -> IVec2 {
            self.value
        }

        pub fn q(self) -> i32 {
            self.value.x
        }

        pub fn r(self) -> i32 {
            self.value.y
        }

        pub fn add(&self, rhs: Self) -> Self {
            Self {
                value: self.value + rhs.value,
            }
        }

        pub fn sub(&self, rhs: Self) -> Self {
            Self {
                value: self.value - rhs.value,
            }
        }

        pub fn neighbors(self) -> impl Iterator<Item = Self> {
            [
                Center::new(1, 0),
                Center::new(1, -1),
                Center::new(0, -1),
                Center::new(-1, 0),
                Center::new(-1, 1),
                Center::new(0, 1),
            ]
            .map(|offset| self.add(offset).wrap())
            .into_iter()
        }

        pub fn distance(&self, other: Self) -> i32 {
            ((self.q() - other.q()).abs()
                + (self.q() + self.r() - other.q() - other.r()).abs()
                + (self.r() - other.r()).abs())
                / 2
        }

        pub fn unwrap_near(&self, target: Self) -> Self {
            let centers = [
                ivec2(148, 0),
                ivec2(-148, 0),
                ivec2(-85, 85 * 2),
                ivec2(85, -85 * 2),
                ivec2(148, 85 * 2),
                ivec2(148, -85 * 2),
                ivec2(-148, 85 * 2),
                ivec2(-148, -85 * 2),
            ]
            .map(|c| self.add(Self::from_ivec2(c)));
            let mut min_dist = self.distance(target);
            let mut min_center = *self;
            for c in &centers[1..] {
                let new_dist = c.distance(target);
                if new_dist < min_dist {
                    min_dist = new_dist;
                    min_center = *c;
                }
            }
            min_center
        }

        pub fn wrap(&self) -> Self {
            // TODO: Take an extra argument for the world size.
            const QMAX: i32 = 148;
            const RMAX: i32 = 85 * 2;

            let r = self.r().rem_euclid(RMAX);
            let q_pre_offset = self.r().div_euclid(2);
            let q_post_offset = r.div_euclid(2);
            let q = (self.q() + q_pre_offset).rem_euclid(QMAX) - q_post_offset;
            Self::new(q, r)
        }

        pub fn from_point(point: DVec2) -> Self {
            const QX: f64 = 4096.0 / 148.0;
            const QY: f64 = QX * 0.5;
            const RY: f64 = 4096.0 / (85.0 * 2.0);

            const SCALE: f64 = 1.0 / (QX * RY);
            const HEX_TO_PIXEL: DMat2 =
                DMat2::from_cols(dvec2(RY * SCALE, 0.0), dvec2(-QY * SCALE, QX * SCALE));
            let qr = HEX_TO_PIXEL.mul_vec2(point);
            Self::from_ivec2(Self::round(qr).as_ivec2())
        }

        pub fn to_point(self) -> DVec2 {
            const QX: f64 = 4096.0 / 148.0;
            const QY: f64 = QX * 0.5;
            const RY: f64 = 4096.0 / (85.0 * 2.0);

            const PIXEL_TO_HEX: DMat2 = DMat2::from_cols(dvec2(QX, 0.0), dvec2(QY, RY));
            PIXEL_TO_HEX.mul_vec2(self.value.as_dvec2())
        }

        pub fn sum(self) -> i32 {
            self.q() + self.r()
        }

        fn round(qr: DVec2) -> DVec2 {
            let fq = qr.x;
            let fr = qr.y;
            let fs = -fq + -fr;

            let mut q = fq.round();
            let mut r = fr.round();
            let s = fs.round();

            let q_diff = (q - fq).abs();
            let r_diff = (r - fr).abs();
            let s_diff = (s - fs).abs();

            if q_diff > r_diff && q_diff > s_diff {
                q = -r + -s;
            } else if r_diff > s_diff {
                r = -q + -s;
            } else {
                let _s = -q + -r;
            }

            dvec2(q, r)
        }
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn test_center_to_point() {
            // (q, r) range for (x, z) from (0, 0) to (4096, 4096):
            //
            // (-85, 85*2)...(0, 85*2)..(148-85, 85*2)
            // ...         ...      ...            ...
            // (-1, 2) (0, 2)       (146, 2)  (147, 2)
            //     (0, 1)                 (147, 1)
            // (0, 0)  (1, 0)  ...  (147, 0)  (148, 0)
            let pt = Center::new(148, 0).to_point();
            assert_eq!(pt, dvec2(4096.0, 0.0));

            let pt = Center::new(148 - 85, 85 * 2).to_point();
            assert_eq!(pt, dvec2(4096.0, 4096.0));
        }

        #[test]
        fn test_center_point_center_roundtrip() {
            // Each even row has 148 hex centers, and
            // each odd row has 147 hex centers (excluding the to-be-wrapped final row/columns).
            let expected_count = (148 + 147) * 85;
            let mut count = 0;

            // Iterate the even rows of the hex centers.
            for r in (0..85 * 2).step_by(2) {
                for q in (-r / 2)..(148 - r / 2) {
                    count += 1;

                    let original = Center::new(q, r);
                    let result = Center::from_point(original.to_point());
                    assert_eq!(original, result);
                }
            }

            // Iterate the odd rows of the hex centers.
            for r in (1..85 * 2).step_by(2) {
                for q in (-(r - 1) / 2)..(-(r - 1) / 2 + 147) {
                    count += 1;

                    let original = Center::new(q, r);
                    let result = Center::from_point(original.to_point());
                    assert_eq!(original, result);
                }
            }

            assert_eq!(count, expected_count);
        }

        #[test]
        fn test_center_wrap() {
            assert_eq!(Center::new(148, 0).wrap(), Center::new(0, 0));
            assert_eq!(Center::new(-85, 85 * 2).wrap(), Center::new(0, 0));
            assert_eq!(Center::new(149, 1).wrap(), Center::new(1, 1));
        }
    }
}
