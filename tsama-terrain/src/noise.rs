#![allow(dead_code)]

mod scale_point;
use glam::{dvec2, ivec2, DVec2, FloatExt, IVec2};
pub use scale_point::*;

mod repeating_voronoi;
pub use repeating_voronoi::*;

use crate::hash::HashVec2;

pub trait Noise2d {
    type Output;
    fn get(&self, point: DVec2) -> Self::Output;
}

pub trait Noise1d {
    type Output;
    fn get(&self, point: f64) -> Self::Output;
}

pub struct Perlin2d {
    // The wrapping size of the map.
    size: i32,
    seed: u64,
    frequency: f64,
}

impl Perlin2d {
    pub fn new(size: i32, seed: u64, frequency: f64) -> Self {
        Self {
            size,
            seed,
            frequency,
        }
    }
}

impl Noise2d for Perlin2d {
    type Output = f64;

    fn get(&self, point: DVec2) -> Self::Output {
        let point = point * self.frequency;
        let point = point.rem_euclid(DVec2::splat(self.size as f64));

        let p0 = point.floor().as_ivec2();
        let p1 = (p0 + ivec2(1, 0)).rem_euclid(IVec2::splat(self.size));
        let p2 = (p0 + ivec2(0, 1)).rem_euclid(IVec2::splat(self.size));
        let p3 = (p0 + ivec2(1, 1)).rem_euclid(IVec2::splat(self.size));
        let fract = point.fract();

        let h0 = (p0.hash_dvec2(self.seed) * 2.0 - 1.0).normalize();
        let h1 = (p1.hash_dvec2(self.seed) * 2.0 - 1.0).normalize();
        let h2 = (p2.hash_dvec2(self.seed) * 2.0 - 1.0).normalize();
        let h3 = (p3.hash_dvec2(self.seed) * 2.0 - 1.0).normalize();

        let n0 = (fract - dvec2(0.0, 0.0)).dot(h0);
        let n1 = (fract - dvec2(1.0, 0.0)).dot(h1);
        let n2 = (fract - dvec2(0.0, 1.0)).dot(h2);
        let n3 = (fract - dvec2(1.0, 1.0)).dot(h3);

        let u = fract * fract * (3.0 - 2.0 * fract);
        let i0 = n0.lerp(n1, u.x);
        let i1 = n2.lerp(n3, u.x);
        i0.lerp(i1, u.y).clamp(-1.0, 1.0)
    }
}

trait SmoothstepLerp {
    fn smoothstep_lerp(&self, other: Self, s: f64) -> Self;
}
