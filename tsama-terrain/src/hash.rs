use glam::{dvec2, ivec2, DVec2, IVec2};
use rustc_hash::FxHasher;
use std::hash::Hasher;

pub trait HashFloat {
    fn hash(self, seed: u64) -> u64;
}

impl HashFloat for f64 {
    fn hash(self, seed: u64) -> u64 {
        let mut hasher = FxHasher::new_with_seed(seed);
        hasher.write_u64(bytemuck::cast(self));
        hasher.finish()
    }
}

pub trait HashInt {
    fn hash(self, seed: u64) -> u64;
    fn hash_f64(self, seed: u64) -> f64;
}

impl HashInt for i32 {
    fn hash(self, seed: u64) -> u64 {
        let mut hasher = FxHasher::new_with_seed(seed);
        hasher.write_i32(self);
        hasher.finish()
    }

    fn hash_f64(self, seed: u64) -> f64 {
        let v = self.hash(seed);
        u64_to_f64_between_0_1(v)
    }
}

pub trait HashVec2 {
    fn hash(self, seed: u64) -> u64;
    fn hash_f64(self, seed: u64) -> f64;
    fn hash_dvec2(self, seed: u64) -> DVec2;
}

impl HashVec2 for IVec2 {
    fn hash(self, seed: u64) -> u64 {
        let mut hasher = FxHasher::new_with_seed(seed);
        hasher.write_i32(self.x);
        hasher.write_i32(self.y);
        hasher.finish()
    }

    /// Hashes `self` into a `f64` value between 0 and 1.
    fn hash_f64(self, seed: u64) -> f64 {
        let v = self.hash(seed);
        u64_to_f64_between_0_1(v)
    }

    /// Hashes `self` into a `DVec2` value between `(0, 0)` and `(1, 1)`.
    fn hash_dvec2(self, seed: u64) -> DVec2 {
        // TODO: Derive shift from the seed?
        const SHIFT: IVec2 = ivec2(28657, 514229);

        let x = self.hash_f64(seed);
        let y = (self + SHIFT).hash_f64(seed);
        dvec2(x, y)
    }
}

fn u64_to_f64_between_0_1(v: u64) -> f64 {
    // Chop off 11 most significant bits and divide by 2^53.
    let dividend: f64 = (v >> 11) as f64;
    let divisor: f64 = (1_u64 << 53) as f64;
    dividend / divisor
}

pub trait MapVec {
    type ElemType;
    fn map(self, f: impl Fn(Self::ElemType) -> Self::ElemType) -> Self;
}

impl MapVec for DVec2 {
    type ElemType = f64;
    fn map(self, f: impl Fn(f64) -> f64) -> Self {
        dvec2(f(self.x), f(self.y))
    }
}

trait FxHasherExt {
    fn new_with_seed(seed: u64) -> Self;
}

impl FxHasherExt for FxHasher {
    fn new_with_seed(seed: u64) -> Self {
        let mut hasher = Self::default();
        hasher.write_u64(seed);
        hasher
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_hash_f64() {
        let mut numbers = Vec::new();
        for i in -1000..1000 {
            let x = ((i as f64).sin() * 191877.17238) as i32;
            let y = ((i as f64).cos() * 987123.41267) as i32;
            let result = ivec2(x, y).hash_f64(123);
            assert!(result >= 0.0 && result <= 1.0);
            assert!(numbers.contains(&result) == false);
            numbers.push(result);
        }
    }
}
