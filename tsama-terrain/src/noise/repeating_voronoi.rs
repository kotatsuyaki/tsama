use crate::hash::HashVec2;
use crate::noise::Noise2d;
use glam::{ivec2, DVec2, IVec2};

pub struct RepeatingVoronoi {
    size: i32,
    seed: u64,
}

pub struct VoronoiOutput {
    pub nearest_cell: IVec2,
    pub nearest_cell_center: DVec2,
    pub nearest_value: f64,
    pub distance: f64,
}

impl RepeatingVoronoi {
    /// Creates a new [`RepeatingVoronoi`].
    ///
    /// The `size` attribute specifies the side length of the square map.  At coordinates above
    /// `size` (or below `0`), the noise values returned by [`RepeatingVoronoi::get`] repeats.
    pub fn new(size: i32, seed: u64) -> Self {
        Self { size, seed }
    }

    fn wrap(&self, cell_origin: IVec2) -> IVec2 {
        cell_origin.rem_euclid(IVec2::splat(self.size))
    }
}

impl Noise2d for RepeatingVoronoi {
    type Output = VoronoiOutput;

    fn get(&self, point: DVec2) -> Self::Output {
        let center_cell_origin = point.floor();
        let _fract = point - center_cell_origin;

        let mut min_dist = 10.0;
        let mut min_cell_origin = IVec2::ZERO;
        let mut min_cell_hashed_point = DVec2::ZERO;

        for x in -1..=1 {
            for y in -1..=1 {
                let cell_origin = center_cell_origin.as_ivec2() + ivec2(x, y);
                let hashed_point =
                    cell_origin.as_dvec2() + self.wrap(cell_origin).hash_dvec2(self.seed);
                let dist = (hashed_point - point).length();
                if dist < min_dist {
                    min_dist = dist;
                    min_cell_origin = self.wrap(cell_origin);
                    min_cell_hashed_point = hashed_point;
                }
            }
        }

        let value = min_cell_origin.hash_f64(42);

        Self::Output {
            nearest_cell: min_cell_origin,
            nearest_cell_center: min_cell_hashed_point,
            nearest_value: value,
            distance: min_dist,
        }
    }
}
