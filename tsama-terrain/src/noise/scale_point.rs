use crate::noise::Noise2d;
use glam::DVec2;

pub struct ScalePoint<Source: Noise2d> {
    scale: f64,
    source: Source,
}

impl<Source: Noise2d> ScalePoint<Source> {
    pub fn new(scale: f64, source: Source) -> Self {
        Self { scale, source }
    }
}

impl<Source: Noise2d> Noise2d for ScalePoint<Source> {
    type Output = Source::Output;

    fn get(&self, point: DVec2) -> Self::Output {
        self.source.get(point * self.scale)
    }
}
