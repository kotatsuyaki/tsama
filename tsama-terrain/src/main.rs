#![allow(dead_code)]

use std::collections::HashMap;

use glam::{ivec2, IVec2};
use image::{Rgb, RgbImage};
use itertools::iproduct;
use tracing::info;
use tracing_subscriber::util::SubscriberInitExt;
use tsama_terrain::{
    consts::CELL_SIZE,
    hash::{HashFloat, HashVec2},
    hex::Center,
    mapgen::IslandMap,
    noise::{Noise2d, RepeatingVoronoi, ScalePoint, VoronoiOutput},
};

fn main() {
    init_logger();

    const RADIUS: i32 = 2048;
    const SEED: u64 = 135;
    const PIXEL_BLOCK_RATIO: i32 = 1;
    // 8192 * 2 / 4 = 4096
    const IMG_SIZE: i32 = RADIUS * 2 / PIXEL_BLOCK_RATIO;

    info!("Creating image of size {IMG_SIZE}");
    let mut img = RgbImage::new(IMG_SIZE as u32, IMG_SIZE as u32);
    let mut gen = SurfaceGenerator::new(RADIUS, CELL_SIZE, SEED);

    let mut minx = 0;
    let mut maxx = 0;
    let mut miny = 0;
    let mut maxy = 0;
    // for px, py in 0, 0 to 4096, 4096
    for (px, py) in iproduct!(0..IMG_SIZE, 0..IMG_SIZE) {
        let sx = px * PIXEL_BLOCK_RATIO;
        let sy = (IMG_SIZE - py - 1) * PIXEL_BLOCK_RATIO;

        minx = minx.min(sx);
        maxx = maxx.max(sx);
        miny = miny.min(sy);
        maxy = maxy.max(sy);

        let mut colors = Vec::new();
        for x in sx..(sx + PIXEL_BLOCK_RATIO) {
            for y in sy..(sy + PIXEL_BLOCK_RATIO) {
                let color = gen.color(ivec2(x, y));
                colors.push(color);
            }
        }

        let mut color_sum: [f64; 3] = [0.0, 0.0, 0.0];
        for &color in colors.iter() {
            #[allow(clippy::needless_range_loop)]
            for i in 0..3 {
                color_sum[i] += color.0[i] as f64;
            }
        }
        let avg_color =
            color_sum.map(|c| (c / colors.len() as f64).clamp(0.0, 255.0).round() as u8);

        // img.put_pixel(px as u32, py as u32, colors[8]);
        img.put_pixel(px as u32, py as u32, Rgb(avg_color));
    }

    info!("{minx} - {maxx}, {miny} - {maxy}");
    img.save("result.png").unwrap();
}

fn hash_center_to_rgb(center: Center) -> Rgb<u8> {
    let h = center.as_ivec2().hash(929596583401330350);

    Rgb([
        ((h & 0xff0000) >> 16) as u8,
        ((h & 0xff00) >> 8) as u8,
        (h & 0xff) as u8,
    ])
}

fn hash_f64_to_rgb(value: f64) -> Rgb<u8> {
    let h = value.hash(42);

    Rgb([
        (h & 0xff) as u8,
        ((h & 0xff00) >> 8) as u8,
        ((h & 0xff0000) >> 16) as u8,
    ])
}

#[tracing::instrument(skip(island_voronoi))]
fn generate_island_map(
    from_key: IVec2,
    from_center: Center,
    island_voronoi: &impl Noise2d<Output = VoronoiOutput>,
) -> IslandMap {
    IslandMap::build(from_key, from_center, |center| {
        island_voronoi.get(center.to_point()).nearest_cell == from_key
    })
}

struct SurfaceGenerator {
    radius: i32,
    voronoi_cell_size: i32,
    seed: u64,
    island_voronoi: ScalePoint<RepeatingVoronoi>,
    island_maps: HashMap<IVec2, IslandMap>,
}

impl SurfaceGenerator {
    fn new(radius: i32, voronoi_cell_size: i32, seed: u64) -> Self {
        let noise = ScalePoint::new(
            1.0 / voronoi_cell_size as f64,
            RepeatingVoronoi::new(2 * radius / voronoi_cell_size, seed),
        );
        Self {
            radius,
            voronoi_cell_size,
            seed,
            island_voronoi: noise,
            island_maps: HashMap::new(),
        }
    }

    fn color(&mut self, point: IVec2) -> Rgb<u8> {
        let center = Center::from_point(point.as_dvec2());

        let center_point = center.to_point();
        let center_noise_output = self.island_voronoi.get(center_point);
        let key = center_noise_output.nearest_cell;

        let island_map = self
            .island_maps
            .entry(key)
            .or_insert_with(|| generate_island_map(key, center, &self.island_voronoi));

        if island_map.centroid == center {
            Rgb([0; 3])
        } else {
            let Rgb([r, g, b]) = hash_center_to_rgb(island_map.centroid);
            let Some(attrs) = island_map.center_attrs.get(&center) else {
                return Rgb([r, g, b]);
            };
            if attrs.is_border {
                Rgb([255, 255, 255])
            } else {
                match attrs.kind {
                    tsama_terrain::mapgen::HexKind::Ocean => Rgb([0, 0, 255]),
                    tsama_terrain::mapgen::HexKind::Land => Rgb([0, 255, 0]),
                }
                // let brightness = attrs.dist_to_border as f64 / island_map.max_dist_to_border as f64;
                // Rgb([
                //     (r as f64 * brightness).round() as u8,
                //     (g as f64 * brightness).round() as u8,
                //     (b as f64 * brightness).round() as u8,
                // ])
            }
        }
    }
}

fn init_logger() {
    let default_env_filter = tracing_subscriber::EnvFilter::from_default_env();
    tracing_subscriber::fmt()
        .with_env_filter(default_env_filter)
        .with_span_events(tracing_subscriber::fmt::format::FmtSpan::CLOSE)
        .finish()
        .init();
}
