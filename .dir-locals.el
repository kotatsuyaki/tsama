((nil
  . ((compile-multi-config
      . ((t
          ("build:debug" . "cargo build")
          ("build:release" . "cargo build --release")
          ("run:debug" . "cargo run")
          ("run:release" . "cargo run --release")
          ("test:tests" . "cargo test")
          ("test:bench" . "cargo bench")
          ("lint:check" . "cargo check")
          ("link:clippy" . "cargo clippy")
          ("format" . "cargo fmt")))))))
