{
  stdenv,
  lib,
  fetchurl,
  autoPatchelfHook,
}:

let
  pname = "wgsl-analyzer-bin";
  version = "0.8.1";
in
stdenv.mkDerivation {
  inherit pname version;

  src = fetchurl {
    url = "https://github.com/wgsl-analyzer/wgsl-analyzer/releases/download/v${version}/wgsl_analyzer-linux-x64";
    hash = "sha256-WRc9fHUw9/KHD5BzUX3nd+0sRkc7i8axsXhHUZFOUeY=";
  };

  dontUnpack = true;

  buildInputs = [
    (stdenv.cc.cc.libgcc or null)
  ];

  nativeBuildInputs = [
    autoPatchelfHook
  ];

  installPhase = ''
    install -m755 -D $src $out/bin/wgsl_analyzer
  '';

  meta = with lib; {
    description = "A language server implementation for the WGSL shading language";
    homepage = "https://github.com/wgsl-analyzer/wgsl-analyzer";
  };
}
