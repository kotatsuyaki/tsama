{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    utils.url = "github:numtide/flake-utils";

    # For stable Rust toolchains
    fenix.url = "github:nix-community/fenix";
    fenix.inputs.nixpkgs.follows = "nixpkgs";

    # For building Rust packages
    naersk.url = "github:nix-community/naersk/master";
    naersk.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs =
    {
      self,
      nixpkgs,
      utils,
      naersk,
      fenix,
    }:
    utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
        lib = pkgs.lib;

        # Stable rust toolchain for the native platform
        stable-toolchain = fenix.packages."${system}".stable;
        stable-build-tools = stable-toolchain.withComponents [
          "cargo"
          "rustc"
          "rust-std"
        ];
        stable-dev-tools = stable-toolchain.withComponents [
          "rust-analyzer"
          "rustfmt"
          "clippy"
          "rust-src"
        ];

        all-toolchains = fenix.packages."${system}";

        # Stable rust toolchain for the Windows platform
        windows-build-tools =
          with all-toolchains;
          combine [
            stable.cargo
            stable.rustc
            targets.x86_64-pc-windows-gnu.stable.rust-std
          ];

        # Stable rust toolchain for the wasm platform.
        # As of the time of this change, porting tsama to wasm requires much effort and thus will
        # not happen within a foreseeable future.
        wasm-build-tools =
          with all-toolchains;
          combine [
            stable.cargo
            stable.rustc
            targets.wasm32-unknown-unknown.stable.rust-std
          ];

        # Naersk libraries
        naersk-lib = naersk.lib."${system}".override {
          cargo = stable-build-tools;
          rustc = stable-build-tools;
        };

        naersk-lib-windows = naersk.lib."${system}".override {
          cargo = windows-build-tools;
          rustc = windows-build-tools;
        };

        # Runtime libraries
        runtime-libs =
          (with pkgs.xorg; [
            libX11
            libXcursor
            libXrandr
            libXi
          ])
          ++ (with pkgs; [
            libxkbcommon
            vulkan-loader
            wayland
          ]) ++ (with pkgs.xorg; [
            libX11
            libXcursor
            libXi
            libXrandr
          ]);

        # Derivations
        native-package = naersk-lib.buildPackage {
          src = ./.;
          buildInputs = runtime-libs;
        };

        windows-package = naersk-lib-windows.buildPackage {
          src = ./.;
          strictDeps = true;
          CARGO_BUILD_TARGET = "x86_64-pc-windows-gnu";
          depsBuildBuild = [
            pkgs.pkgsCross.mingwW64.stdenv.cc
            pkgs.pkgsCross.mingwW64.windows.pthreads
          ];
        };

        # WGSL LSP server for the dev shell
        wgsl-analyzer-bin = pkgs.callPackage ./nix/wgsl-analyzer-bin.nix { };
      in
      {
        packages.default = native-package;
        packages.tsama = native-package;
        packages.tsama-windows = windows-package;
        packages.wgsl-analyzer-bin = wgsl-analyzer-bin;

        apps.default = utils.lib.mkApp {
          drv = self.packages."${system}".default;
        };

        devShells.default = pkgs.mkShell {
          nativeBuildInputs =
            with pkgs;
            [
              # Rust toolchain
              stable-build-tools
              stable-dev-tools

              # Nix LSP
              nil
              # TOML LSP
              taplo-cli
              # WGSL LSP
              wgsl-analyzer-bin

              # Graphics debugger
              renderdoc
              mesa-demos
            ]
            ++ runtime-libs;
          LD_LIBRARY_PATH = lib.makeLibraryPath runtime-libs;
          RUST_SRC_PATH = "${stable-dev-tools}/lib/rustlib/src/rust/library";
          VK_LAYER_PATH = "${pkgs.vulkan-validation-layers}/share/vulkan/explicit_layer.d";
        };

        devShells.wasm = pkgs.mkShell {
          nativeBuildInputs =
            with pkgs;
            [
              wasm-build-tools
            ];
          RUST_SRC_PATH = "${stable-dev-tools}/lib/rustlib/src/rust/library";
        };

        formatter = pkgs.nixfmt-rfc-style;
      }
    );
}
