use anyhow::{Context, Result};
use futures::{SinkExt, StreamExt, TryStreamExt};
use std::collections::HashMap;
use std::env;
use tokio::net::tcp::{OwnedReadHalf, OwnedWriteHalf};
use tokio::net::{TcpListener, TcpStream};
use tokio::sync::mpsc::{unbounded_channel, UnboundedReceiver, UnboundedSender};
use tokio_util::codec::{FramedRead, FramedWrite, LengthDelimitedCodec};
use tracing::{debug, info, trace, warn};
use tsama_core::{ClientMessage, ServerMessage};
use uuid::Uuid;

pub trait Remote {
    fn drain_new_clients(&mut self) -> Vec<ClientId>;
    fn drain_client_disconnects(&mut self) -> Vec<ClientId>;
    fn send_message(&self, to_client_id: ClientId, message: ServerMessage) -> Result<()>;
    fn receive_message(&mut self) -> Result<SourcedClientMessage>;
}

impl Remote for ServerNear {
    fn drain_new_clients(&mut self) -> Vec<ClientId> {
        let mut new_client_ids = Vec::new();
        while let Ok((client_id, outgoing_sender)) = self.new_clients_receiver.try_recv() {
            self.all_outgoing_senders.insert(client_id, outgoing_sender);
            new_client_ids.push(client_id)
        }
        new_client_ids
    }

    fn drain_client_disconnects(&mut self) -> Vec<ClientId> {
        let mut disconnected_client_ids = Vec::new();
        while let Ok(client_id) = self.client_disconnect_receiver.try_recv() {
            disconnected_client_ids.push(client_id);
        }
        disconnected_client_ids
    }

    fn send_message(&self, to_client_id: ClientId, message: ServerMessage) -> Result<()> {
        debug!("-> {to_client_id}: {message:.03?}");
        let sender = self
            .all_outgoing_senders
            .get(&to_client_id)
            .context("No outgoing sender matching the requested client id")?;
        sender
            .send(message)
            .context("Failed to send message from near to far")?;
        Ok(())
    }

    fn receive_message(&mut self) -> Result<SourcedClientMessage> {
        self.incoming_receiver
            .try_recv()
            .context("Failed to receive message from far")
            .inspect(|sourced_message| {
                let client_id = sourced_message.client_id;
                let message = &sourced_message.inner;
                debug!("<- {client_id}: {message:.03?}");
            })
    }
}

#[derive(Clone)]
pub struct ServerFar {
    incoming_sender: UnboundedSender<SourcedClientMessage>,
    new_clients_sender: UnboundedSender<(ClientId, UnboundedSender<ServerMessage>)>,
    client_disconnect_sender: UnboundedSender<ClientId>,
}

pub struct ServerNear {
    all_outgoing_senders: HashMap<ClientId, UnboundedSender<ServerMessage>>,
    incoming_receiver: UnboundedReceiver<SourcedClientMessage>,
    new_clients_receiver: UnboundedReceiver<(ClientId, UnboundedSender<ServerMessage>)>,
    client_disconnect_receiver: UnboundedReceiver<ClientId>,
}

pub struct SourcedClientMessage {
    pub client_id: ClientId,
    pub inner: ClientMessage,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct ClientId {
    id: Uuid,
}

pub fn new_server() -> (ServerNear, ServerFar) {
    let (incoming_sender, incoming_receiver) = unbounded_channel();
    let (new_clients_sender, new_clients_receiver) = unbounded_channel();
    let (client_disconnect_sender, client_disconnect_receiver) = unbounded_channel();
    let near = ServerNear {
        all_outgoing_senders: HashMap::new(),
        incoming_receiver,
        new_clients_receiver,
        client_disconnect_receiver,
    };

    let far = ServerFar {
        incoming_sender,
        new_clients_sender,
        client_disconnect_sender,
    };
    tokio::spawn(far.clone().listen());

    (near, far)
}

impl ServerFar {
    async fn listen(self) {
        // Bind to the port or abort the process.
        let address = env::var("TSAMA_SERVER_ADDRESS").unwrap_or("0.0.0.0:1111".to_string());
        let listener = match TcpListener::bind(address).await {
            Ok(listener) => listener,
            Err(err) => {
                warn!("Failed to listen to the port: {:?}", err);
                std::process::exit(1)
            }
        };

        info!("Listening to client connections");

        loop {
            // Accept the stream or continue.
            let client_stream = match listener.accept().await {
                Ok((stream, _)) => stream,
                Err(err) => {
                    warn!("Failed to accept the listener: {:?}", err);
                    continue;
                }
            };

            info!("New client connection accepted");

            tokio::spawn(self.clone().handle_client_stream(client_stream));
        }
    }

    async fn handle_client_stream(self, client_stream: TcpStream) {
        let client_id = ClientId::new();
        let (outgoing_sender, outgoing_receiver) = unbounded_channel();
        if let Err(err) = self.new_clients_sender.send((client_id, outgoing_sender)) {
            warn!(
                "Failed to send the new client to the internal queue: {:?}",
                err
            );

            // Upon drop, the underlying socket within `socket` will be closed as per the
            // documentation of mio.  Thus, there is no need to shutdown the socket manually,
            // which requires either .await'ing or spawning the "shutdown" future.
            return;
        }

        info!(
            "New client connection accepted with client id {}",
            client_id
        );

        let (read_half, write_half) = client_stream.into_split();
        tokio::spawn(self.clone().forward_incoming(read_half, client_id));
        tokio::spawn(self.forward_outgoing(write_half, outgoing_receiver, client_id));
    }

    async fn forward_incoming(self, read_half: OwnedReadHalf, client_id: ClientId) {
        let decoded_stream = FramedRead::new(read_half, LengthDelimitedCodec::new())
            .map_err(|e| e.into())
            .and_then(|encoded_message| async move {
                let message: ClientMessage = ciborium::from_reader(encoded_message.as_ref())?;
                Result::<ClientMessage>::Ok(message)
            });
        let mut decoded_stream = Box::pin(decoded_stream);

        while let Some(message) = decoded_stream.next().await {
            trace!("Received incoming message from client id {}", client_id);

            let message = match message {
                Ok(message) => message,
                Err(err) => {
                    warn!("Failed to receive a message from the client: {:?}", err);
                    continue;
                }
            };
            if let Err(err) = self.incoming_sender.send(SourcedClientMessage {
                client_id,
                inner: message,
            }) {
                warn!(
                    "Failed to forward an incoming message from a client: {:?}",
                    err
                );
                continue;
            }
        }

        info!("Client incoming disconnect: {}", client_id);
        if let Err(err) = self.client_disconnect_sender.send(client_id) {
            warn!("Failed to send client disconnect to the game: {:?}", err);
        }
    }

    async fn forward_outgoing(
        self,
        write_half: OwnedWriteHalf,
        mut outgoing_receiver: UnboundedReceiver<ServerMessage>,
        client_id: ClientId,
    ) {
        let encoding_sink = FramedWrite::new(write_half, LengthDelimitedCodec::new()).with(
            |message: ServerMessage| async move {
                let mut encoded_buffer = vec![];
                ciborium::into_writer(&message, &mut encoded_buffer)?;
                let encoded_message: bytes::Bytes = encoded_buffer.into();
                Result::<bytes::Bytes>::Ok(encoded_message)
            },
        );
        let mut encoding_sink = Box::pin(encoding_sink);

        while let Some(message) = outgoing_receiver.recv().await {
            trace!("Forwarding outgoing message to client id {}", client_id);

            let Err(err) = encoding_sink.send(message).await else {
                continue;
            };
            warn!("Failed to forward a message to the socket: {:?}", err);
        }

        info!("Client outgoing disconnect: {}", client_id);
        if let Err(err) = self.client_disconnect_sender.send(client_id) {
            warn!("Failed to send client disconnect to the game: {:?}", err);
        }
    }
}

impl ClientId {
    fn new() -> Self {
        Self { id: Uuid::new_v4() }
    }
}

impl std::fmt::Display for ClientId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.id.hyphenated())
    }
}
