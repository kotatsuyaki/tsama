use anyhow::{Context, Result};
use glam::{dvec3, DVec3};
use itertools::{iproduct, Itertools};
use std::collections::{HashMap, HashSet};
use tracing::{info, warn};
use tsama_core::{
    consts::{INITIAL_PLAYER_POSITION, VISIBLE_DISTANCE},
    entity_types::{EntityFacing, EntityId, EntityMotion},
    types::{BoundingBox, ChunkLocation, Position, WorldSize},
    ClientMessage, Deer, DummySendPlayerMessage, EntityBehavior, EntityKind, ServerMessage, World,
    WorldStepConfig,
};

use crate::{
    chunk_store::{ChunkStore, ChunkStoreResponse},
    server::{ClientId, Remote, SourcedClientMessage},
};

pub struct Game {
    world: World,
    clients: ClientCollection,
    chunk_store: ChunkStore,
}

struct ClientCollection {
    clients: HashMap<ClientId, Client>,
}

impl ClientCollection {
    fn new() -> Self {
        Self {
            clients: HashMap::new(),
        }
    }

    fn entity_ids(&self) -> impl Iterator<Item = EntityId> + '_ {
        self.clients.values().filter_map(|client| client.entity_id)
    }

    fn logged_in_client_ids(&self) -> impl Iterator<Item = ClientId> + '_ {
        self.clients
            .keys()
            .filter(|client_id| {
                self.clients.get(client_id).unwrap().login_status == LoginStatus::LoggedIn
            })
            .cloned()
    }

    fn logged_in_clients_mut(&mut self) -> impl Iterator<Item = &mut Client> {
        self.clients
            .values_mut()
            .filter(|client| client.login_status == LoginStatus::LoggedIn)
    }

    pub fn insert(&mut self, client_id: ClientId, client: Client) -> Option<Client> {
        self.clients.insert(client_id, client)
    }

    pub fn get(&self, client_id: &ClientId) -> Option<&Client> {
        self.clients.get(client_id)
    }

    pub fn remove(&mut self, client_id: &ClientId) -> Option<Client> {
        self.clients.remove(client_id)
    }

    pub fn get_mut(&mut self, client_id: &ClientId) -> Option<&mut Client> {
        self.clients.get_mut(client_id)
    }
}

struct Client {
    client_id: ClientId,
    login_status: LoginStatus,

    player_name: Option<String>,
    entity_id: Option<EntityId>,

    loaded_chunk_locations: HashSet<ChunkLocation>,
    loaded_entities: HashSet<EntityId>,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum LoginStatus {
    Initial,
    LoggedIn,
}

impl Game {
    pub fn new() -> Self {
        // Add an entity for debugging.
        let mut world = World::new(WorldSize::from_radius_in_chunks(64));
        let _deer_entity_id = world.create_entity(
            EntityKind::Deer(Deer),
            EntityMotion::new(
                Position::new(2.0, 160.0, 2.0),
                dvec3(0.0, 0.0, 0.0),
                Deer.bounding_box_at(Position::new(2.0, 160.0, 2.0)),
                false,
                EntityFacing::default(),
            ),
        );
        let clients = ClientCollection::new();
        let chunk_store = ChunkStore::new();

        Self {
            world,
            clients,
            chunk_store,
        }
    }

    pub fn update_world<RemoteType: Remote>(&mut self, remote: &mut RemoteType) {
        self.handle_new_clients(remote);
        self.handle_client_disconnects(remote);
        self.handle_all_client_messages(remote);

        let world_step_config: WorldStepConfig<'_, DummySendPlayerMessage> = WorldStepConfig {
            player_config: None,
            should_update_entities: true,
        };
        self.world.step(&world_step_config);

        self.load_and_unload_chunks();
        self.send_chunk_load_and_unload_messages(remote);
        self.send_entity_messages(remote);
    }

    fn send_entity_messages<RemoteType: Remote>(&mut self, remote: &mut RemoteType) {
        let client_ids: Vec<_> = self.clients.logged_in_client_ids().collect();
        for client in client_ids {
            self.send_entity_messages_to_client(client, remote);
        }
    }

    fn send_entity_messages_to_client<RemoteType: Remote>(
        &mut self,
        client_id: ClientId,
        remote: &mut RemoteType,
    ) {
        let client = self.clients.get(&client_id).unwrap();
        assert!(client.login_status == LoginStatus::LoggedIn);

        // Get the player position for the client.
        let player_entity_id = client
            .entity_id()
            .expect("send_entity_messages_to_client expects all clients to have player entities.");
        let player_position = self.world.entity_position(player_entity_id).unwrap();

        // Find the `EntityId`s to load, unload, and update.
        let expected_entities: HashSet<_> = self
            .world
            .entities_in_bounding_box(BoundingBox::from_center_and_size(
                player_position,
                dvec3(12.0, 12.0, 12.0) * 16.0,
            ))
            .collect();

        let entities_to_load: Vec<_> = expected_entities
            .difference(&client.loaded_entities)
            .cloned()
            .collect();
        let entities_to_unload: Vec<_> = client
            .loaded_entities
            .difference(&expected_entities)
            .cloned()
            .collect();
        let entities_to_update: Vec<_> = expected_entities
            .intersection(&client.loaded_entities)
            .cloned()
            .collect();

        // Send load, unload, and update messages.
        for entity_id in entities_to_load {
            let motion = self.world.entity_motion(entity_id).unwrap().clone();
            let kind = self.world.entity_kind(entity_id).unwrap();
            let message = ServerMessage::EntityLoad {
                id: entity_id,
                kind,
                motion,
            };
            if let Err(err) = remote.send_message(client_id, message) {
                warn!("Failed to send entity load message to client: {:?}", err);
            }
        }

        for entity_id in entities_to_unload {
            let message = ServerMessage::EntityUnload { id: entity_id };
            if let Err(err) = remote.send_message(client_id, message) {
                warn!("Failed to send entity unload message to client: {:?}", err);
            }
        }

        for entity_id in entities_to_update {
            // Do not "correct" the motion of the player entity controlled by the client itself.
            let is_player_themself = client.entity_id().is_some_and(|id| id == entity_id);
            if is_player_themself {
                continue;
            }

            // Otherwise, send the motion of the entity.
            let motion = self.world.entity_motion(entity_id).unwrap().clone();
            let message = ServerMessage::EntityMotion {
                id: entity_id,
                motion,
            };
            if let Err(err) = remote.send_message(client_id, message) {
                warn!("Failed to send entity motion message to client: {:?}", err);
            }
        }

        // Update the set of loaded entities.
        self.clients.get_mut(&client_id).unwrap().loaded_entities = expected_entities;
    }

    fn load_and_unload_chunks(&mut self) {
        for response in self.chunk_store.drain_responses() {
            if let ChunkStoreResponse::Loaded(chunk) = response {
                self.world.add_chunk(chunk);
            }
        }

        let loaded_chunk_locations: HashSet<_> = self.world.chunk_locations().collect();
        let expected_chunk_locations: HashSet<_> = self
            .clients
            .entity_ids()
            .flat_map(|entity_id| {
                let center = self
                    .world
                    .entity_position(entity_id)
                    .unwrap()
                    .chunk_location();
                iproduct!(-12..12, -12..12).map(move |(sx, sz)| center.offset(sx, sz))
            })
            .map(|chunk_location| chunk_location.wrap(self.world.size()))
            .collect();
        let chunk_locations_to_load: Vec<_> = expected_chunk_locations
            .difference(&loaded_chunk_locations)
            .cloned()
            .collect();
        let chunk_locations_to_unload: Vec<_> = loaded_chunk_locations
            .difference(&expected_chunk_locations)
            .cloned()
            .collect();

        for chunk_location in chunk_locations_to_load {
            self.chunk_store.load_chunk(chunk_location).unwrap();
        }

        for chunk_location in chunk_locations_to_unload {
            let chunk = self.world.remove_chunk(chunk_location);
            self.chunk_store.unload_chunk(chunk).unwrap();
        }
    }

    fn send_chunk_load_and_unload_messages<RemoteType: Remote>(&mut self, remote: &mut RemoteType) {
        let chunk_locations: Vec<_> = self.world.chunk_locations().collect();

        // For each client, find the chunks to load/unload and send them to the client.
        for client in self.clients.logged_in_clients_mut() {
            let player_chunk_location = self
                .world
                .entity_position(client.entity_id().unwrap())
                .unwrap()
                .chunk_location();

            let expected_chunk_locations: HashSet<_> = chunk_locations
                .iter()
                .cloned()
                .filter(|&location| {
                    player_chunk_location.distance_squared_shortest(location, self.world.size())
                        < VISIBLE_DISTANCE.pow(2)
                })
                .collect();
            let chunk_locations_to_load: Vec<_> = expected_chunk_locations
                .difference(&client.loaded_chunk_locations)
                .cloned()
                .collect();
            let chunk_locations_to_unload: Vec<_> = client
                .loaded_chunk_locations
                .difference(&expected_chunk_locations)
                .cloned()
                .collect();

            for chunk_location in chunk_locations_to_load {
                let message = ServerMessage::ChunkLoad {
                    chunk: self.world.chunk(chunk_location).unwrap().clone(),
                };
                if let Err(err) = remote.send_message(client.client_id, message) {
                    warn!("Failed to send chunk load message to client: {:?}", err);
                }
            }

            for chunk_location in chunk_locations_to_unload {
                let message = ServerMessage::ChunkUnload {
                    location: chunk_location,
                };
                if let Err(err) = remote.send_message(client.client_id, message) {
                    warn!("Failed to send chunk unload message to client: {:?}", err);
                }
            }

            // Update the loaded chunks recorded on the server side
            client.loaded_chunk_locations = expected_chunk_locations;
        }
    }

    fn handle_new_clients<RemoteType: Remote>(&mut self, remote: &mut RemoteType) {
        let new_client_ids = remote.drain_new_clients();
        for client_id in new_client_ids {
            self.clients.insert(client_id, Client::new(client_id));
        }
    }

    fn handle_client_disconnects<RemoteType: Remote>(&mut self, remote: &mut RemoteType) {
        let disconnected_client_ids = remote.drain_client_disconnects();
        for client_id in disconnected_client_ids {
            let Some(client) = self.clients.get(&client_id) else {
                continue;
            };
            let Some(entity_id) = client.entity_id() else {
                continue;
            };
            self.world.destroy_entity(entity_id);
            self.clients.remove(&client_id);
        }
    }

    fn handle_all_client_messages<RemoteType: Remote>(&mut self, remote: &mut RemoteType) {
        while let Ok(SourcedClientMessage { client_id, inner }) = remote.receive_message() {
            let res = self.handle_client_message(client_id, inner, remote);
            if let Err(err) = res {
                warn!("Failed to handle client message: {:?}", err);
            }
        }
    }

    fn handle_client_message<RemoteType: Remote>(
        &mut self,
        client_id: ClientId,
        message: ClientMessage,
        remote: &mut RemoteType,
    ) -> Result<()> {
        match message {
            ClientMessage::Login { player_name } => {
                self.login_for_player(client_id, player_name, remote)?;
            }
            ClientMessage::PlayerMotion { id, motion } => {
                let _entity_id = self
                    .clients
                    .get(&client_id)
                    .and_then(|client| client.entity_id)
                    .context(
                        "Failed to get client entity id when processing player motion message",
                    )?;
                self.world.set_entity_motion(id, motion)?;
            }
            ClientMessage::SetBlock { location, kind } => {
                info!("Setting block at location {location:?} to kind {kind:?}");

                self.world.set_block_kind(location, kind);
                let client_ids = self.clients.logged_in_client_ids().collect_vec();
                for &client_id in client_ids.iter() {
                    let chunk_location = location.chunk_location();
                    let is_chunk_loaded = self
                        .clients
                        .get(&client_id)
                        .unwrap()
                        .loaded_chunk_locations
                        .contains(&chunk_location);
                    if !is_chunk_loaded {
                        continue;
                    }

                    if let Err(err) =
                        remote.send_message(client_id, ServerMessage::SetBlock { location, kind })
                    {
                        warn!("Failed to send SetBlock to client: {err:?}");
                    }
                }
            }
        }

        Ok(())
    }

    fn login_for_player<RemoteType: Remote>(
        &mut self,
        client_id: ClientId,
        player_name: String,
        remote: &mut RemoteType,
    ) -> Result<()> {
        let client = self.clients.get_mut(&client_id).context(format!(
            "Cannot find client with id {} for login",
            client_id
        ))?;

        let entity_id = self
            .world
            .create_player(INITIAL_PLAYER_POSITION, DVec3::ZERO);
        let motion = self.world.entity_motion(entity_id).unwrap().clone();

        client.loaded_entities.insert(entity_id);

        client.player_name = Some(player_name);
        client.entity_id = Some(entity_id);
        client.login_status = LoginStatus::LoggedIn;

        let message = ServerMessage::LoginSuccess {
            player_entity_id: entity_id,
            player_motion: motion,
        };
        remote
            .send_message(client_id, message)
            .context("Failed to send login success message to remote")?;
        Ok(())
    }

    pub async fn join_task(&mut self) -> Result<()> {
        self.chunk_store.join_task().await?;
        Ok(())
    }
}

impl Client {
    fn new(client_id: ClientId) -> Self {
        Self {
            client_id,
            login_status: LoginStatus::Initial,

            player_name: None,
            entity_id: None,

            loaded_chunk_locations: HashSet::new(),
            loaded_entities: HashSet::new(),
        }
    }

    fn entity_id(&self) -> Option<EntityId> {
        self.entity_id
    }
}
