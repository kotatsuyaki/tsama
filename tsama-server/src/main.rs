#![allow(clippy::bool_comparison)]

use anyhow::Result;
use std::time::Duration;
use tracing::info;
use tracing_subscriber::{layer::SubscriberExt, Layer};
use tsama_core::timer::Timer;

mod chunk_store;
mod game;
mod server;

use game::Game;

fn main() {
    init_logger();
    let runtime = build_async_runtime();

    runtime
        .block_on(run())
        .expect("Failed to peacefully shutdown the server");
    info!("Server normal exit");
}

async fn run() -> Result<()> {
    let (shutdown_tx, mut shutdown_rx) = tokio::sync::oneshot::channel();
    tokio::spawn(async move {
        tokio::signal::ctrl_c().await.unwrap();
        shutdown_tx.send(()).unwrap();
    });

    let mut game = Game::new();
    let (mut server_near, _server_far) = server::new_server();

    let mut timer = Timer::new_from_target_framerate(20);
    loop {
        timer.record_frame_start();
        let whole_frame_count = timer.accumulated_frame().whole_frame_count();

        for _frame in 0..whole_frame_count {
            game.update_world(&mut server_near);
        }

        if shutdown_rx.try_recv().is_ok() {
            info!("Shutdown");
            break;
        }

        let duration_to_next_frame = timer.find_duration_to_next_frame();
        if duration_to_next_frame > Duration::ZERO {
            spin_sleep::sleep(duration_to_next_frame);
        }
    }

    game.join_task().await?;
    Ok(())
}

fn build_async_runtime() -> tokio::runtime::Runtime {
    tokio::runtime::Builder::new_multi_thread()
        .thread_name("tsama-server-thread")
        .enable_all()
        .build()
        .expect("Failed to build tokio runtime")
}

fn init_logger() {
    let filter = tracing_subscriber::filter::Builder::default()
        .with_default_directive("tsama_server=info".parse().unwrap())
        .from_env()
        .unwrap();
    tracing::subscriber::set_global_default(
        tracing_subscriber::registry().with(tracing_subscriber::fmt::layer().with_filter(filter)),
    )
    .unwrap()
}
