use std::{
    collections::{HashMap, HashSet},
    io::{Read, SeekFrom, Write},
    ops::Range,
    path::{Path, PathBuf},
};

use anyhow::{anyhow, bail, Context, Result};
use serde::{Deserialize, Serialize};
use tokio::{
    io::{AsyncReadExt, AsyncSeekExt, AsyncWriteExt},
    sync::mpsc::{UnboundedReceiver, UnboundedSender},
    task::JoinHandle,
};
use tracing::{debug, error};
use tsama_core::{types::ChunkLocation, Chunk};

pub struct ChunkStore {
    // This is an Option<T> so that we can take and drop the tx to force the task reading rx to end.
    request_tx: Option<UnboundedSender<ChunkStoreRequest>>,
    response_rx: UnboundedReceiver<ChunkStoreResponse>,
    task: Option<JoinHandle<Result<()>>>,
    loads: HashSet<ChunkLocation>,
    unloads: HashSet<ChunkLocation>,
}

enum ChunkStoreRequest {
    Load(ChunkLocation),
    Unload(Chunk),
}

pub enum ChunkStoreResponse {
    Loaded(Chunk),
    Unloaded(ChunkLocation),
    UnloadFailed(ChunkLocation),
}

#[derive(Serialize, Deserialize)]
struct ChunkFileHeader {
    record_locations: HashMap<ChunkLocation, (u64, u64)>,
}

impl ChunkStore {
    pub fn new() -> Self {
        let (request_tx, mut request_rx) =
            tokio::sync::mpsc::unbounded_channel::<ChunkStoreRequest>();
        let (response_tx, response_rx) =
            tokio::sync::mpsc::unbounded_channel::<ChunkStoreResponse>();
        let task = tokio::spawn(async move {
            let mut incoming_requests = Vec::new();
            tokio::fs::create_dir_all("tsama_save").await?;
            loop {
                let requests_count = request_rx.recv_many(&mut incoming_requests, 512).await;
                if requests_count == 0 {
                    break;
                }
                for request in incoming_requests.drain(..) {
                    let response = handle_request(request).await;
                    response_tx.send(response)?;
                }
            }
            Result::<()>::Ok(())
        });
        Self {
            request_tx: Some(request_tx),
            response_rx,
            task: Some(task),
            loads: HashSet::new(),
            unloads: HashSet::new(),
        }
    }

    pub async fn join_task(&mut self) -> Result<()> {
        self.request_tx.take();
        self.task.take().unwrap().await??;
        Ok(())
    }

    pub fn load_chunk(&mut self, location: ChunkLocation) -> Result<()> {
        if self.loads.contains(&location) == false {
            self.request_tx
                .as_mut()
                .unwrap()
                .send(ChunkStoreRequest::Load(location))?;
        }
        Ok(())
    }

    pub fn unload_chunk(&mut self, chunk: Chunk) -> Result<()> {
        if self.unloads.contains(&chunk.location()) == false {
            self.request_tx
                .as_mut()
                .unwrap()
                .send(ChunkStoreRequest::Unload(chunk))?;
        }
        Ok(())
    }

    pub fn drain_responses(&mut self) -> Vec<ChunkStoreResponse> {
        let mut responses = Vec::new();
        while let Ok(response) = self.response_rx.try_recv() {
            match &response {
                ChunkStoreResponse::Loaded(chunk) => {
                    self.loads.remove(&chunk.location());
                }
                ChunkStoreResponse::Unloaded(chunk_location) => {
                    self.unloads.remove(chunk_location);
                }
                ChunkStoreResponse::UnloadFailed(chunk_location) => {
                    self.unloads.remove(chunk_location);
                }
            }
            responses.push(response)
        }
        responses
    }
}

async fn handle_request(request: ChunkStoreRequest) -> ChunkStoreResponse {
    match request {
        ChunkStoreRequest::Load(location) => match load_chunk("tsama_save", location).await {
            Ok(chunk) => {
                debug!("Loaded chunk from file at {}", location);
                ChunkStoreResponse::Loaded(chunk)
            }
            Err(_e) => {
                debug!(
                    "Failed to load chunk from file at {}, generating instead",
                    location
                );
                ChunkStoreResponse::Loaded(Chunk::new(location))
            }
        },
        ChunkStoreRequest::Unload(chunk) => {
            let location = chunk.location();
            match unload_chunk("tsama_save", chunk).await {
                Ok(_) => {
                    debug!("Unloaded chunk to file at {}", location);
                    ChunkStoreResponse::Unloaded(location)
                }
                Err(e) => {
                    error!("Error while unloading chunk, possible data loss:\n{:#?}", e);
                    ChunkStoreResponse::UnloadFailed(location)
                }
            }
        }
    }
}

const TSAMA_MAGIC: [u8; 8] = [b'T', b'S', b'A', b'M', b'A', b'\0', b'\0', b'\0'];

async fn load_chunk(save_dir: impl AsRef<Path>, location: ChunkLocation) -> Result<Chunk> {
    let file_path = {
        let x = location.x().div_euclid(16 * 32);
        let z = location.z().div_euclid(16 * 32);
        [save_dir.as_ref(), Path::new(&format!("{}.{}.bin", x, z))]
            .iter()
            .collect::<PathBuf>()
    };
    let mut file = tokio::fs::File::options()
        .read(true)
        .write(false)
        .create(false)
        .truncate(false)
        .open(&file_path)
        .await?;

    let header = {
        let mut magic_buf = [0u8; 8];
        file.read_exact(&mut magic_buf).await?;
        if magic_buf != TSAMA_MAGIC {
            bail!("Magic bytes mismatch");
        }

        let mut header_len_buf = [0u8; 8];
        file.read_exact(&mut header_len_buf).await?;
        let header_len = u64::from_be_bytes(header_len_buf);

        let mut header_buf = vec![0u8; header_len as usize];
        file.read_exact(&mut header_buf).await?;

        let header: ChunkFileHeader = ciborium::from_reader(&header_buf[..])?;

        header
    };

    let Some((offset, len)) = header.offset_and_len(location) else {
        bail!("No such chunk in the header");
    };

    file.seek(SeekFrom::Start(offset)).await?;
    let mut compressed_buffer = vec![0u8; len as usize];
    file.read_exact(&mut compressed_buffer).await?;

    let chunk = tokio::task::spawn_blocking(move || {
        let mut compression_decoder = flate2::read::DeflateDecoder::new(&compressed_buffer[..]);
        let mut chunk_buffer = vec![];
        compression_decoder.read_to_end(&mut chunk_buffer)?;
        let chunk: Chunk = ciborium::from_reader(&chunk_buffer[..])?;
        Result::<Chunk>::Ok(chunk)
    })
    .await??;

    Ok(chunk)
}

async fn unload_chunk(save_dir: impl AsRef<Path>, chunk: Chunk) -> Result<()> {
    let location = chunk.location();
    let compressed_buffer = tokio::task::spawn_blocking(move || {
        let mut chunk_buffer = vec![];
        ciborium::into_writer(&chunk, &mut chunk_buffer)?;

        let mut compression_encoder =
            flate2::write::DeflateEncoder::new(Vec::new(), flate2::Compression::default());
        compression_encoder.write_all(&chunk_buffer)?;
        let compressed_buffer = compression_encoder.finish()?;

        Result::<Vec<u8>>::Ok(compressed_buffer)
    })
    .await??;

    let file_path = {
        let x = location.x().div_euclid(16 * 32);
        let z = location.z().div_euclid(16 * 32);
        [save_dir.as_ref(), Path::new(&format!("{}.{}.bin", x, z))]
            .iter()
            .collect::<PathBuf>()
    };
    let mut file = tokio::fs::File::options()
        .read(true)
        .write(true)
        .create(true)
        .truncate(false)
        .open(&file_path)
        .await?;
    let metadata = file.metadata().await?;
    let file_len = metadata.len();

    let (mut header, header_len) = if file_len == 0 {
        // New file.
        file.write_all(&TSAMA_MAGIC).await?;
        file.write_all(&[0u8; 8]).await?;

        (ChunkFileHeader::new(), 0)
    } else {
        // Read header.
        let mut magic_buf = [0u8; 8];
        file.read_exact(&mut magic_buf).await?;
        if magic_buf != TSAMA_MAGIC {
            bail!("Magic bytes mismatch");
        }

        let mut header_len_buf = [0u8; 8];
        file.read_exact(&mut header_len_buf).await?;
        let header_len = u64::from_be_bytes(header_len_buf);

        let mut header_buf = vec![0u8; header_len as usize];
        file.read_exact(&mut header_buf).await?;

        let header: ChunkFileHeader = ciborium::from_reader(&header_buf[..])?;

        (header, header_len)
    };

    // Write the chunk to a new offset and update the header.
    if header.contains(location) {
        header.remove(location);
    }
    let offset_to_write =
        header.find_available_offset(header_len, compressed_buffer.len() as u64)?;
    file.seek(SeekFrom::Start(offset_to_write)).await?;
    file.write_all(&compressed_buffer).await?;
    header.insert(location, offset_to_write, compressed_buffer.len() as u64);

    let new_header_buf = loop {
        // Serialize the updated header.
        let mut new_header_buf = Vec::new();
        ciborium::into_writer(&header, &mut new_header_buf)?;

        // Check if the update header will overlap with some existing chunk data, and move the chunk
        // data elsewhere.
        let overlapping_chunk_locations =
            header.overlapping_chunk_locations(new_header_buf.len() as u64);
        if overlapping_chunk_locations.is_empty() {
            break new_header_buf;
        }
        for &location in overlapping_chunk_locations.iter() {
            let (offset, len) = header
                .offset_and_len(location)
                .context("Unable to get chunk offset and len from header")?;
            file.seek(SeekFrom::Start(offset)).await?;
            let mut compressed_buffer = vec![0u8; len as usize];
            file.read_exact(&mut compressed_buffer).await?;

            if header.contains(location) {
                header.remove(location);
            }

            let offset_to_write = header.find_available_offset(
                new_header_buf.len() as u64,
                compressed_buffer.len() as u64,
            )?;
            file.seek(SeekFrom::Start(offset_to_write)).await?;
            file.write_all(&compressed_buffer).await?;

            header.insert(location, offset_to_write, compressed_buffer.len() as u64);
        }
    };

    // Write the header.
    file.seek(SeekFrom::Start(8)).await?;
    let new_header_len_buf = (new_header_buf.len() as u64).to_be_bytes();
    assert_eq!(new_header_len_buf.len(), 8);
    file.write_all(&new_header_len_buf).await?;
    file.write_all(&new_header_buf).await?;

    file.flush().await?;

    Ok(())
}

impl ChunkFileHeader {
    fn new() -> Self {
        Self {
            record_locations: HashMap::new(),
        }
    }

    fn offset_and_len(&self, location: ChunkLocation) -> Option<(u64, u64)> {
        self.record_locations.get(&location).cloned()
    }

    fn contains(&self, location: ChunkLocation) -> bool {
        self.record_locations.contains_key(&location)
    }

    fn remove(&mut self, location: ChunkLocation) {
        self.record_locations.remove(&location);
    }

    fn insert(&mut self, location: ChunkLocation, offset: u64, len: u64) {
        self.record_locations.insert(location, (offset, len));
    }

    fn overlapping_chunk_locations(&self, new_header_len: u64) -> Vec<ChunkLocation> {
        let header_end = align_next(8 + 8 + new_header_len, 4096);
        self.record_locations
            .iter()
            .filter_map(|(location, (offset, _len))| {
                let start = *offset;
                (header_end > start).then_some(*location)
            })
            .collect()
    }

    fn sorted_occupied_ranges(&self, current_header_len: u64) -> Vec<Range<u64>> {
        // 8 bytes for magic + 8 bytes for header length.
        let header_end = align_next(8 + 8 + current_header_len, 4096);
        #[allow(
            clippy::single_range_in_vec_init,
            reason = "This is intended to be a Vec<Range> with 1 element only initially."
        )]
        let mut occupied_ranges = vec![(0..header_end)];

        // Populate occupied_ranges with the ranges for the existing chunks.
        occupied_ranges.extend(
            self.record_locations
                .iter()
                .map(|(_location, &(offset, len))| {
                    assert_eq!(offset % 4096, 0);
                    offset..align_next(offset + len, 4096)
                }),
        );

        // Sort by range start.
        occupied_ranges.sort_by_key(|range| range.start);
        occupied_ranges
    }

    fn checked_hole_ranges(&self, occupied_ranges: &[Range<u64>]) -> Result<Vec<Range<u64>>> {
        // Check every pair of adjacent ranges that they do not overlap.
        // Collect the holes in between each pair, if any.
        let hole_ranges = occupied_ranges
            .windows(2)
            .filter_map(|adj_ranges| -> Option<Result<Range<u64>>> {
                assert_eq!(adj_ranges.len(), 2);
                let a = &adj_ranges[0];
                let b = &adj_ranges[1];
                #[allow(
                    clippy::comparison_chain,
                    reason = "An if-else chain is easier to comprehend."
                )]
                if a.end > b.start {
                    Some(Err(anyhow!("Occupied ranges {:?} and {:?} overlap", a, b)))
                } else if a.end < b.start {
                    Some(Ok(a.end..b.start))
                } else {
                    None
                }
            })
            .collect::<Result<Vec<Range<u64>>>>()?;
        Ok(hole_ranges)
    }

    fn find_available_offset(&self, current_header_len: u64, len: u64) -> Result<u64> {
        let occupied_ranges = self.sorted_occupied_ranges(current_header_len);

        let hole_ranges = self.checked_hole_ranges(&occupied_ranges)?;

        // First-fit find.
        let selected_start = {
            let selected_hole = hole_ranges.iter().find(|hole| {
                let hole_len = hole.end - hole.start;
                hole_len >= len
            });
            if let Some(selected_hole) = selected_hole {
                selected_hole.start
            } else {
                occupied_ranges.last().unwrap().end
            }
        };

        Ok(selected_start)
    }
}

fn align_next(x: u64, n: u64) -> u64 {
    n * ((x - 1).div_euclid(n) + 1)
}

#[cfg(test)]
mod tests {
    use std::collections::HashSet;

    use itertools::iproduct;
    use rand::{seq::SliceRandom, Rng, SeedableRng};
    use tsama_core::{types::LocalBlockLocation, BlockKind};

    use super::*;

    #[tokio::test]
    async fn test_unload_load_chunk_roundtrip() -> Result<()> {
        let save_dir = async_tempfile::TempDir::new().await.unwrap();

        let location = ChunkLocation::new(0, 0);
        let original_chunk = Chunk::new(location);
        unload_chunk("tsama_save", original_chunk.clone()).await?;
        let loaded_chunk = load_chunk(save_dir.dir_path(), location).await?;

        for (x, y, z) in iproduct!(0..16, 0..256, 0..16) {
            let original_block_kind = original_chunk.block_kind(LocalBlockLocation::new(x, y, z));
            let loaded_block_kind = loaded_chunk.block_kind(LocalBlockLocation::new(x, y, z));
            assert_eq!(original_block_kind, loaded_block_kind);
        }

        Ok(())
    }

    #[tokio::test]
    async fn test_unload_load_chunk_multiple() -> Result<()> {
        const ACTIONS_COUNT: usize = 4096;
        const CHUNK_RANGE: Range<i32> = -2..3;

        let save_dir = async_tempfile::TempDir::new().await.unwrap();
        let save_dir = save_dir.dir_path();
        let mut rng = rand::rngs::StdRng::seed_from_u64(42);

        // Build a series of actions to perform in the test.
        enum Action {
            Create(ChunkLocation),
            Mutate(ChunkLocation, LocalBlockLocation, BlockKind),
        }
        let mut actions = vec![];
        let mut created = HashSet::new();
        let block_kinds: Vec<_> = BlockKind::iter().collect();
        for _ in 0..ACTIONS_COUNT {
            let x = rng.gen_range(CHUNK_RANGE);
            let z = rng.gen_range(CHUNK_RANGE);
            let location = ChunkLocation::new(x, z);
            if created.contains(&location) == false {
                actions.push(Action::Create(location));
                created.insert(location);
            } else {
                let x = rng.gen_range(0..16);
                let y = rng.gen_range(0..256);
                let z = rng.gen_range(0..16);
                let block_location = LocalBlockLocation::new(x, y, z);
                let block_kind = block_kinds.choose(&mut rng).unwrap().clone();
                actions.push(Action::Mutate(location, block_location, block_kind));
            }
        }

        // Run the test.
        let mut reference_chunks = HashMap::new();
        for action in actions {
            match action {
                Action::Create(location) => {
                    let chunk = Chunk::new(location);
                    reference_chunks.insert(location, chunk.clone());
                    unload_chunk(&save_dir, chunk).await?;
                }
                Action::Mutate(location, block_location, block_kind) => {
                    reference_chunks
                        .get_mut(&location)
                        .unwrap()
                        .set_block_kind(block_location, block_kind);
                    let mut chunk = load_chunk(&save_dir, location).await?;
                    chunk.set_block_kind(block_location, block_kind);
                    unload_chunk(&save_dir, chunk).await?;
                }
            }
        }

        // Check content of all chunks.
        for (&location, reference_chunk) in reference_chunks.iter_mut() {
            let loaded_chunk = load_chunk(&save_dir, location).await?;
            for (x, y, z) in iproduct!(0..16, 0..256, 0..16) {
                let reference_block_kind =
                    reference_chunk.block_kind(LocalBlockLocation::new(x, y, z));
                let loaded_block_kind = loaded_chunk.block_kind(LocalBlockLocation::new(x, y, z));
                assert_eq!(reference_block_kind, loaded_block_kind);
            }
        }

        Ok(())
    }
}
