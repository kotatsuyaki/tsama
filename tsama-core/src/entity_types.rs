use glam::{dvec3, DVec3};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::{
    types::{BoundingBox, Position, WorldSize},
    util::Vec3Ext,
};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct EntityId {
    id: Uuid,
}

impl EntityId {
    pub fn random() -> Self {
        Self { id: Uuid::new_v4() }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct EntityMotion {
    pub(crate) position: Position,
    pub(crate) velocity: DVec3,
    pub(crate) bounding_box: BoundingBox,
    pub(crate) is_on_ground: bool,
    pub(crate) facing: EntityFacing,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
pub struct EntityFacing {
    pitch: f32,
    yaw: f32,
}

impl Default for EntityFacing {
    fn default() -> Self {
        // Defaults to looking horizontally without yaw.
        Self::new(std::f32::consts::FRAC_PI_2, 0.0)
    }
}

impl EntityFacing {
    pub fn new(pitch: f32, yaw: f32) -> Self {
        Self { pitch, yaw }
    }

    pub fn pitch(&self) -> f32 {
        self.pitch
    }

    pub fn yaw(&self) -> f32 {
        self.yaw
    }

    pub fn interpolate(&self, other: Self, elapsed: f32) -> Self {
        Self {
            pitch: self.pitch + (other.pitch - self.pitch) * elapsed,
            yaw: self.yaw + (other.yaw - self.yaw) * elapsed,
        }
    }
}

impl EntityMotion {
    pub fn new(
        position: Position,
        velocity: DVec3,
        bounding_box: BoundingBox,
        is_on_ground: bool,
        facing: EntityFacing,
    ) -> Self {
        Self {
            position,
            velocity,
            bounding_box,
            is_on_ground,
            facing,
        }
    }

    pub fn position(&self) -> Position {
        self.position
    }

    pub fn velocity(&self) -> DVec3 {
        self.velocity
    }

    pub fn bounding_box(&self) -> BoundingBox {
        self.bounding_box
    }

    pub fn is_on_ground(&self) -> bool {
        self.is_on_ground
    }

    pub fn with_velocity(mut self, velocity: DVec3) -> Self {
        self.velocity = velocity;
        self
    }

    pub fn with_position(mut self, position: Position) -> Self {
        self.position = position;
        self
    }

    pub fn with_bounding_box(mut self, bounding_box: BoundingBox) -> Self {
        self.bounding_box = bounding_box;
        self
    }

    pub fn with_is_on_ground(mut self, is_on_ground: bool) -> Self {
        self.is_on_ground = is_on_ground;
        self
    }

    pub fn with_facing(mut self, facing: EntityFacing) -> Self {
        self.facing = facing;
        self
    }

    pub fn facing(&self) -> EntityFacing {
        self.facing
    }

    pub fn pitch(&self) -> f32 {
        self.facing.pitch
    }

    pub fn yaw(&self) -> f32 {
        self.facing.yaw
    }

    pub fn look(&self) -> DVec3 {
        dvec3(0.0, 1.0, 0.0)
            .apply_rotation(self.facing.pitch as f64, self.facing.yaw as f64)
            .normalize()
    }

    pub fn interpolate(&self, rhs: &Self, elapsed: f64, size: WorldSize) -> Self {
        let interpolated_position = self.position.interpolate(rhs.position, elapsed, size);
        let offset = interpolated_position - self.position;
        let interpolated_bounding_box = self.bounding_box.offset(offset);
        Self::new(
            interpolated_position,
            self.velocity.lerp(rhs.velocity, elapsed),
            interpolated_bounding_box,
            self.is_on_ground,
            self.facing.interpolate(rhs.facing, elapsed as f32),
        )
    }
}

#[cfg(test)]
mod tests {
    use glam::dvec3;

    use super::*;

    #[test]
    fn test_entity_motion_interpolate() {
        let motion = EntityMotion::new(
            Position::new(0.0, 36.0, 0.0),
            dvec3(0.0, 0.0, 0.0),
            BoundingBox::new(
                Position::new(-0.6, 36.0, -0.6),
                Position::new(0.6, 36.0, 0.6),
            ),
            true,
            EntityFacing::new(1.5707964, 0.0),
        );
        assert_eq!(
            motion.interpolate(&motion, 0.5, WorldSize::from_radius_in_chunks(64)),
            motion
        );
    }
}
