use glam::{dvec3, ivec3, DVec3, EulerRot, IVec3, Mat4, Vec3};

pub trait Vec3Ext {
    type Vec3ExtElem;
    fn apply_rotation(self, pitch: Self::Vec3ExtElem, yaw: Self::Vec3ExtElem) -> Self;
    fn horizontal_component(self) -> Self;
}

impl Vec3Ext for Vec3 {
    type Vec3ExtElem = f32;

    fn apply_rotation(self, pitch: f32, yaw: f32) -> Vec3 {
        // Apply `0.0` radians of roll, then apply `pitch` radians of pitch, then finally apply `yaw`
        // radians of yaw.
        Mat4::from_euler(EulerRot::YXZ, yaw, pitch, 0.0).transform_point3(self)
    }

    fn horizontal_component(self) -> Vec3 {
        let mut v = self;
        v.y = 0.0;
        v
    }
}

impl Vec3Ext for DVec3 {
    type Vec3ExtElem = f64;

    fn apply_rotation(self, pitch: f64, yaw: f64) -> DVec3 {
        Mat4::from_euler(EulerRot::YXZ, yaw as f32, pitch as f32, 0.0)
            .transform_point3(self.as_vec3())
            .as_dvec3()
    }

    fn horizontal_component(self) -> DVec3 {
        let mut v = self;
        v.y = 0.0;
        v
    }
}

pub trait VecWith {
    type VecWithElem;
    fn with_x(self, x: Self::VecWithElem) -> Self;
    fn with_y(self, y: Self::VecWithElem) -> Self;
    fn with_z(self, z: Self::VecWithElem) -> Self;
}

impl VecWith for IVec3 {
    type VecWithElem = i32;

    fn with_x(self, x: Self::VecWithElem) -> Self {
        let mut v = self;
        v.x = x;
        v
    }

    fn with_y(self, y: Self::VecWithElem) -> Self {
        let mut v = self;
        v.y = y;
        v
    }

    fn with_z(self, z: Self::VecWithElem) -> Self {
        let mut v = self;
        v.z = z;
        v
    }
}

impl VecWith for DVec3 {
    type VecWithElem = f64;

    fn with_x(self, x: Self::VecWithElem) -> Self {
        let mut v = self;
        v.x = x;
        v
    }

    fn with_y(self, y: Self::VecWithElem) -> Self {
        let mut v = self;
        v.y = y;
        v
    }

    fn with_z(self, z: Self::VecWithElem) -> Self {
        let mut v = self;
        v.z = z;
        v
    }
}

impl Iterator for IVec3RangeIter {
    type Item = IVec3;

    fn next(&mut self) -> Option<Self::Item> {
        self.cur.z += 1;
        if self.cur.z < self.max.z {
            return Some(self.cur);
        }
        self.cur.z = self.min.z + 1;
        self.cur.x += 1;

        if self.cur.x < self.max.x {
            return Some(self.cur);
        }
        self.cur.x = self.min.x;
        self.cur.y += 1;
        if self.cur.y < self.max.y {
            return Some(self.cur);
        }
        None
    }
}

/// Half open range iterator between two grid points `min` and `max`.
#[derive(Debug, Clone, Copy)]
pub struct IVec3RangeIter {
    min: IVec3,
    max: IVec3,
    cur: IVec3,
}

impl IVec3RangeIter {
    pub fn new(min: IVec3, max: IVec3) -> Self {
        let min = ivec3(min.x, min.y, min.z - 1);
        assert!(min.x < max.x, "Unexpected {min} >= {max}");
        assert!(min.y < max.y, "Unexpected {min} >= {max}");
        assert!(min.z < max.z, "Unexpected {min} >= {max}");
        Self { min, max, cur: min }
    }
}

/// `radius` in chunks.
#[deprecated]
pub fn unwrap_dest_near_src(
    /* target */ src: DVec3,
    /* original */ dest: DVec3,
    radius: i32,
) -> DVec3 {
    let r = (radius * 16) as f64;
    let x = if src.x - dest.x > r {
        dest.x + 2.0 * r
    } else if src.x - dest.x < -r {
        dest.x - 2.0 * r
    } else {
        dest.x
    };
    let z = if src.z - dest.z > r {
        dest.z + 2.0 * r
    } else if src.z - dest.z < -r {
        dest.z - 2.0 * r
    } else {
        dest.z
    };
    dvec3(x, dest.y, z)
}

/// `radius` in chunks.
pub fn wrap_position(position: DVec3, radius: i32) -> DVec3 {
    let r = (radius * 16) as f64;
    let wrap = |x: f64| x.rem_euclid(2.0 * r);
    dvec3(wrap(position.x), position.y, wrap(position.z))
}

#[cfg(test)]
mod tests {
    use glam::ivec3;
    use itertools::iproduct;

    use super::*;

    #[test]
    fn test_ivec3_range_iter() {
        let range_iter = IVec3RangeIter::new(ivec3(1, 2, 3), ivec3(10, 10, 10));
        let expected_iter = iproduct!(2..10, 1..10, 3..10).map(|(y, x, z)| ivec3(x, y, z));
        for (a, b) in range_iter.zip(expected_iter) {
            assert_eq!(a, b);
        }
    }
}
