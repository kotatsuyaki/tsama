use std::{
    collections::VecDeque,
    time::{Duration, Instant},
};
use tracing::{debug, warn};

pub struct Timer {
    last_frame_starting_instant: Option<Instant>,
    frame_instants: VecDeque<Instant>,
    accumulated_frame: AccumulatedFrame,
}

#[derive(Debug, Clone, Copy)]
pub struct AccumulatedFrame {
    target_frame_duration: Duration,
    whole_frame_count: u32,
    remainder_duration: Duration,
}

impl Timer {
    pub fn new(target_frame_duration: Duration) -> Self {
        Self {
            last_frame_starting_instant: None,
            frame_instants: VecDeque::with_capacity(60),
            accumulated_frame: AccumulatedFrame {
                target_frame_duration,
                whole_frame_count: 0,
                remainder_duration: Duration::ZERO,
            },
        }
    }

    pub fn new_from_target_framerate(target_framerate: u64) -> Self {
        let target_framerate = Duration::from_nanos(1_000_000_000 / target_framerate);
        Self::new(target_framerate)
    }

    /// Records the current instant as the start of the current frame.
    pub fn record_frame_start(&mut self) {
        // Update the accumulator.
        let current_frame_instant = Instant::now();
        let last_frame_duration = self
            .last_frame_starting_instant
            .map(|x| current_frame_instant - x)
            .unwrap_or(Duration::ZERO);
        if last_frame_duration >= Duration::from_millis(20) {
            warn!("Long frame duration: {}", last_frame_duration.as_millis());
        }
        self.accumulated_frame.increase(last_frame_duration);
        self.last_frame_starting_instant = Some(current_frame_instant);

        // Pop the recorded instants that are more than 1-second-old.
        while let Some(&instant) = self.frame_instants.front() {
            if instant >= current_frame_instant - Duration::from_secs(1) {
                break;
            }
            self.frame_instants.pop_front();
        }

        // Add this instant to the recorded instants.
        self.frame_instants.push_back(current_frame_instant);
    }

    pub fn fps(&self) -> usize {
        self.frame_instants.len()
    }

    /// Gets the accumulated time information for the last recorded frame.
    pub fn accumulated_frame(&self) -> &AccumulatedFrame {
        &self.accumulated_frame
    }

    /// Finds the duration to the start of next frame.
    ///
    /// Returns value of [`Duration::ZERO`] means that the next frame is already overdue.
    /// This function is intended for calculating the amount of time to sleep until the start of the
    /// next frame.
    pub fn find_duration_to_next_frame(&self) -> Duration {
        let current_frame_duration = self
            .last_frame_starting_instant
            .map(|x| Instant::now() - x)
            .unwrap_or(Duration::ZERO);
        let accumulated_duration =
            self.accumulated_frame.remainder_duration + current_frame_duration;

        // Sub<Duration> for Duration panics when the result is below zero, so we have to manually
        // check for the case.
        if accumulated_duration > self.accumulated_frame.target_frame_duration {
            debug!(
                "Overdue frame with accumulated duration = {:?}",
                accumulated_duration
            );
            Duration::ZERO
        } else {
            self.accumulated_frame.target_frame_duration - accumulated_duration
        }
    }
}

impl AccumulatedFrame {
    /// The number of whole frames elapsed between last two records.
    ///
    /// This value is intended to be used as the number of steps to go through the the game logic.
    pub fn whole_frame_count(&self) -> usize {
        self.whole_frame_count as usize
    }

    /// The fraction of a whole frame elapsed between last two records.
    ///
    /// This value is guaranteed to be within the range `[0.0, 1.0]`.
    /// This value is intended to be used for interpolation when rendering the scenes.
    pub fn remainder_frame_fraction(&self) -> f64 {
        self.remainder_duration.as_nanos() as f64 / self.target_frame_duration.as_nanos() as f64
    }

    // Increments the stored duration and recalculate the whole frame count & remainder.
    fn increase(&mut self, frame_duration: Duration) {
        self.remainder_duration += frame_duration;
        self.whole_frame_count = self.remainder_duration
            .as_nanos()
            .div_euclid(self.target_frame_duration.as_nanos())
        // The whole frame count is narrowed from u128 to u32 due to `Duration` only supporting
        // `Mul<u32>`.  Using u32 to represent elapsed frame count allows up to 2^32 frames per
        // timer tick, which is more than enough.
            as u32;
        self.remainder_duration -= self.target_frame_duration * self.whole_frame_count;
    }
}
