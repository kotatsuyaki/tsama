//! A library containing the core data types and logic shared between the client and the server.
//!
//! # Coordinate system
//!
//! The blocks and entities in the world are on a *left-handed* coordinate system as illustrated.
//!
//! ```text
//! +y, pitch = 0
//!      +z, yaw = 0 (into the paper)
//!  │
//!  │  ╱
//!  │ ╱
//!  │╱
//!  └────── +x, yaw = ½π
//! ```
//!
//! Zero [`EntityFacing`] pitch points upwards (+y), and zero [`EntityFacing`] yaw points northwards
//! (+z).  The yaw goes clockwise, so ½π (i.e. 90°) points eastwards (+x).

use ambassador::{delegatable_trait, Delegate};
use anyhow::{bail, Context, Result};
use entity_types::{EntityFacing, EntityId, EntityMotion};
use glam::{dvec3, ivec2, ivec3, swizzles::Vec3Swizzles, DVec3, IVec2, IVec3};
use glam::{BVec2, BVec3, DVec2};
use noise::{MultiFractal, NoiseFn};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use strum::{EnumIter, IntoEnumIterator};
use tracing::{info, warn};
use types::{
    BlockLocation, BoundingBox, ChunkLocation, Face, LocalBlockLocation, Position, SegmentLocation,
    WorldSize,
};

use crate::consts::*;
use crate::util::Vec3Ext;

pub mod consts;
pub mod entity_types;
pub mod timer;
pub mod types;
pub mod util;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum ServerMessage {
    LoginSuccess {
        player_entity_id: EntityId,
        player_motion: EntityMotion,
    },
    ChunkLoad {
        chunk: Chunk,
    },
    ChunkUnload {
        location: ChunkLocation,
    },
    EntityLoad {
        id: EntityId,
        kind: EntityKind,
        motion: EntityMotion,
    },
    EntityUnload {
        id: EntityId,
    },
    EntityMotion {
        id: EntityId,
        motion: EntityMotion,
    },
    SetBlock {
        location: BlockLocation,
        kind: BlockKind,
    },
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum ClientMessage {
    Login {
        player_name: String,
    },
    PlayerMotion {
        id: EntityId,
        motion: EntityMotion,
    },
    SetBlock {
        location: BlockLocation,
        kind: BlockKind,
    },
}

pub struct World {
    size: WorldSize,
    chunks: HashMap<ChunkLocation, Chunk>,
    entity_kinds: HashMap<EntityId, EntityKind>,
    entity_motions: HashMap<EntityId, EntityMotion>,
    /// A cached mapping from segments to entity IDs.
    entity_segments: HashMap<SegmentLocation, Vec<EntityId>>,
    last_entity_motions: HashMap<EntityId, EntityMotion>,
}

impl World {
    pub fn new(size: WorldSize) -> Self {
        Self {
            size,
            chunks: HashMap::new(),
            entity_kinds: HashMap::new(),
            entity_motions: HashMap::new(),
            last_entity_motions: HashMap::new(),
            entity_segments: HashMap::new(),
        }
    }

    pub fn size(&self) -> WorldSize {
        self.size
    }

    pub fn entity_kinds(&self) -> impl Iterator<Item = (EntityId, EntityKind)> + '_ {
        self.entity_kinds.iter().map(|(k, v)| (*k, *v))
    }

    pub fn entity_motions(&self) -> impl Iterator<Item = (EntityId, EntityMotion)> + '_ {
        self.entity_motions.iter().map(|(k, v)| (*k, v.clone()))
    }

    pub fn entity_segments(&self) -> impl Iterator<Item = (&SegmentLocation, &Vec<EntityId>)> + '_ {
        self.entity_segments.iter()
    }

    pub fn entity_kind(&self, entity_id: EntityId) -> Option<EntityKind> {
        self.entity_kinds.get(&entity_id).cloned()
    }

    pub fn chunk_locations(&self) -> impl Iterator<Item = ChunkLocation> + '_ {
        self.chunks.keys().cloned()
    }

    pub fn chunk(&self, location: ChunkLocation) -> Option<&Chunk> {
        assert!(self.size.contains_chunk(location));
        self.chunks.get(&location)
    }

    pub fn chunk_mut(&mut self, location: ChunkLocation) -> Option<&mut Chunk> {
        assert!(self.size.contains_chunk(location));
        self.chunks.get_mut(&location)
    }

    pub fn add_chunk(&mut self, chunk: Chunk) {
        assert!(self.size.contains_chunk(chunk.location()));
        self.chunks.insert(chunk.location, chunk);
    }

    pub fn remove_chunk(&mut self, location: ChunkLocation) -> Chunk {
        assert!(self.size.contains_chunk(location));
        self.chunks.remove(&location).unwrap()
    }

    pub fn block_kind(&self, location: BlockLocation) -> Option<BlockKind> {
        let (chunk_location, local_location) = location.decompose();
        let chunk_location = chunk_location.wrap(self.size());
        self.chunk(chunk_location)
            .map(|chunk| chunk.block_kind(local_location))
    }

    pub fn set_block_kind(&mut self, location: BlockLocation, block_kind: BlockKind) {
        let (chunk_location, local_location) = location.decompose();
        if let Some(chunk) = self.chunk_mut(chunk_location) {
            chunk.set_block_kind(local_location, block_kind);
        }
    }

    pub fn create_entity_with_id(&mut self, id: EntityId, kind: EntityKind, motion: EntityMotion) {
        self.cache_entity_segment(motion.position(), id);
        self.entity_kinds.insert(id, kind);
        self.entity_motions.insert(id, motion);
    }

    pub fn create_entity(&mut self, kind: EntityKind, motion: EntityMotion) -> EntityId {
        let entity_id = EntityId::random();
        self.create_entity_with_id(entity_id, kind, motion);
        entity_id
    }

    fn cache_entity_segment(&mut self, position: Position, id: EntityId) {
        let segment = position.segment_location().wrap(self.size());
        self.entity_segments.entry(segment).or_default().push(id);
    }

    pub fn entities_in_bounding_box(
        &self,
        bounding_box: BoundingBox,
    ) -> impl Iterator<Item = EntityId> + '_ {
        // Find the range of chunk locations.
        let min_segment = bounding_box.min().segment_location();
        let max_segment = bounding_box.max().segment_location();

        let box_center = bounding_box.center();

        // For each segment, return the entities inside that overlaps with the specified bounding box.
        iterate_segments!(min_segment, max_segment)
            // IVec3RangeIter::new(min_segment, max_segment)
            .filter_map(move |segment| {
                let segment_key = segment.wrap(self.size());

                // If there are no entities, skip this segment.
                let entities = self.entity_segments.get(&segment_key)?;

                // For each entity, unwrap its bounding box near the specified bounding box and decide
                // whether they overlap.
                Some(entities.iter().filter_map(move |&entity_id| {
                    let entity_motion = self.entity_motions.get(&entity_id).unwrap();
                    let entity_box_unwrapped = entity_motion
                        .bounding_box()
                        .unwrap_near(box_center, self.size());

                    // If they overlap, return this entity ID.
                    let has_overlap = entity_box_unwrapped.overlaps_with(&bounding_box);
                    has_overlap.then_some(entity_id)
                }))
            })
            .flatten()
    }

    pub fn create_player(&mut self, position: Position, velocity: DVec3) -> EntityId {
        let bounding_box = Player.bounding_box_at(position);
        self.create_entity(
            EntityKind::Player(Player),
            EntityMotion::new(
                position,
                velocity,
                bounding_box,
                false,
                EntityFacing::default(),
            ),
        )
    }

    pub fn destroy_entity(&mut self, entity_id: EntityId) {
        let motion = self.entity_motions.remove(&entity_id).unwrap();
        self.remove_entitiy_id_from_segment(entity_id, motion.position());
        self.entity_kinds.remove(&entity_id);
        self.last_entity_motions.remove(&entity_id);
    }

    fn remove_entitiy_id_from_segment(&mut self, entity_id: EntityId, position: Position) {
        let segment = position.segment_location().wrap(self.size());
        let segment_entities = self.entity_segments.get_mut(&segment).unwrap();
        segment_entities.retain(|&id| id != entity_id);
        if segment_entities.is_empty() {
            self.entity_segments.remove(&segment);
        }
    }

    pub fn entity_motion(&self, entity_id: EntityId) -> Option<&EntityMotion> {
        self.entity_motions.get(&entity_id)
    }

    pub fn entity_position(&self, entity_id: EntityId) -> Option<Position> {
        self.entity_motions
            .get(&entity_id)
            .map(|motion| motion.position())
    }

    pub fn last_entity_motion(&self, entity_id: EntityId) -> Option<&EntityMotion> {
        self.last_entity_motions.get(&entity_id)
    }

    pub fn interlopated_entity_motion(
        &self,
        entity_id: EntityId,
        elapsed: f64,
    ) -> Option<EntityMotion> {
        let entity_motion = self.entity_motion(entity_id)?;
        if let Some(last_entity_motion) = self.last_entity_motion(entity_id) {
            Some(last_entity_motion.interpolate(entity_motion, elapsed, self.size()))
        } else {
            Some(entity_motion.clone())
        }
    }

    pub fn set_entity_motion(
        &mut self,
        entity_id: EntityId,
        entity_motion: EntityMotion,
    ) -> Result<()> {
        let position = entity_motion.position();
        let bounding_box = entity_motion.bounding_box();

        if position.as_inner().signum() != bounding_box.center().as_inner().signum() {
            bail!(
                "Entity box {:.03?} has different sign bits than its position {:.03?}",
                bounding_box,
                position
            );
        }

        // Insert the new and get the old motion.
        let last_entity_motion = self
            .entity_motions
            .insert(entity_id, entity_motion)
            .context(format!(
                "Setting entity motion for nonexistent key {:?}",
                entity_id
            ))?;

        // Update the entity segment cache.
        self.remove_entitiy_id_from_segment(entity_id, last_entity_motion.position());
        self.cache_entity_segment(position, entity_id);

        // Save the last entity motion if it's not there yet.
        //
        // The last_entity_motions field is cleared only once in each game step,
        // which means that *only the first entity motion update* in a game step stores the old motion.
        self.last_entity_motions
            .entry(entity_id)
            .or_insert(last_entity_motion);

        Ok(())
    }

    pub fn step<Sender: SendPlayerMessage>(&mut self, config: &WorldStepConfig<'_, Sender>) {
        self.last_entity_motions.clear();

        if let Some(player_update_config) = config.player_config.as_ref() {
            self.step_player(player_update_config);
        }
        if config.should_update_entities {
            self.step_entity_updates();
        }
        self.step_entity_motions();
    }

    pub fn step_partial(&mut self, config: &WorldPartialStepConfig) {
        if let Some(config) = config.player_config.as_ref() {
            self.step_player_partial(config);
        }
    }

    pub fn step_player_partial(&mut self, config: &PlayerPartialStepConfig) {
        let &PlayerPartialStepConfig {
            entity_id,
            yaw,
            pitch,
        } = config;

        let entity_kind = &self.entity_kinds.get(&entity_id).unwrap();
        assert!(matches!(entity_kind, EntityKind::Player(_)));

        let player_motion = self.entity_motion(entity_id).unwrap().clone();
        let pitch = player_motion.facing().pitch() + PITCH_SPEED * pitch;
        let yaw = player_motion.facing().yaw() + YAW_SPEED * yaw;

        self.set_entity_motion(
            entity_id,
            player_motion.with_facing(EntityFacing::new(pitch, yaw)),
        )
        .unwrap();
    }

    pub fn step_player<Sender: SendPlayerMessage>(
        &mut self,
        config: &PlayerStepConfig<'_, Sender>,
    ) {
        let &PlayerStepConfig {
            entity_id,
            front,
            right,
            jump,
            sneak: _,
            yaw,
            pitch,
            attack,
            dig: _,
            sender,
            utilize,
        } = config;
        let entity_kind = *self.entity_kinds.get(&entity_id).unwrap();
        assert!(matches!(entity_kind, EntityKind::Player(_)));

        self.step_player_velocity_and_facing(entity_id, pitch, yaw, front, right, jump);
        let block_hit = self.find_player_block_hit(entity_id);
        if attack {
            if let Some(block_hit) = &block_hit {
                info!("A block is hit: {block_hit:?}");
                if let Err(err) = sender.send(ClientMessage::SetBlock {
                    location: block_hit.block_location,
                    kind: BlockAir.into(),
                }) {
                    warn!("Failed to send SetBlock to server: {err:?}");
                };
            } else {
                info!("No block hit");
            }
        }
        if utilize {
            if let Some(block_hit) = &block_hit {
                let new_block_location =
                    (block_hit.block_location + block_hit.face.to_offset()).wrap(self.size());
                if self.size().contains_block(new_block_location) {
                    if let Err(err) = sender.send(ClientMessage::SetBlock {
                        location: new_block_location,
                        kind: BlockGlass.into(),
                    }) {
                        warn!("Failed to send SetBlock to server: {err:?}");
                    }
                }
            }
        }
    }

    fn find_player_block_hit(&self, entity_id: EntityId) -> Option<BlockHit> {
        let motion = self.entity_motion(entity_id).unwrap();
        let look = motion.look() * 6.0;
        let begin = Player::eye_position_at(motion.position()).as_inner();
        let end = begin + look;

        let mut current = begin;
        let mut current_block = current.floor().as_ivec3();
        let end_block = end.floor().as_ivec3();

        for _ in 0..100 {
            if current_block == end_block {
                return None;
            }

            if current.is_nan() {
                return None;
            }

            // The next X, Y, and Z planes to collide with depends on the direction of the ray.
            let planes = (current_block
                + IVec3::select(end_block.cmpgt(current_block), IVec3::ONE, IVec3::ZERO))
            .as_dvec3();

            // Calculate the progress (the percentage) marched from `current` to the collision points
            // with the planes.
            let diff = end - current;
            let progress = DVec3::select(
                end_block.cmpeq(current_block),
                DVec3::MAX,
                (planes - current) / diff,
            );
            // Avoid infinite bouncing between two blocks.
            let progress = DVec3::select(
                progress.cmpeq(DVec3::ZERO),
                DVec3::splat(-0.00001),
                progress,
            );

            // The collision happens with the plane corresponding to the lowest progress.
            let (hit_face, hit) = if progress.x < progress.y && progress.x < progress.z {
                (
                    if end_block.x > current_block.x {
                        Face::NegX
                    } else {
                        Face::PosX
                    },
                    dvec3(
                        planes.x,
                        current.y + diff.y * progress.x,
                        current.z + diff.z * progress.x,
                    ),
                )
            } else if progress.y < progress.z {
                (
                    if end_block.y > current_block.y {
                        Face::NegY
                    } else {
                        Face::PosY
                    },
                    dvec3(
                        current.x + diff.x * progress.y,
                        planes.y,
                        current.z + diff.z * progress.y,
                    ),
                )
            } else {
                (
                    if end_block.z > current_block.z {
                        Face::NegZ
                    } else {
                        Face::PosZ
                    },
                    dvec3(
                        current.x + diff.x * progress.z,
                        current.y + diff.y * progress.z,
                        planes.z,
                    ),
                )
            };

            current = hit;
            // The block being hit depends on the direction of the ray.
            current_block = hit.floor().as_ivec3()
                + IVec3::select(
                    BVec3::new(
                        hit_face == Face::PosX,
                        hit_face == Face::PosY,
                        hit_face == Face::PosZ,
                    ),
                    IVec3::NEG_ONE,
                    IVec3::ZERO,
                );

            // Collide with the block.
            let block_kind = self.block_kind(current_block.into());
            let Some(block_kind) = block_kind else {
                continue;
            };
            if block_kind.can_collide() {
                return Some(BlockHit {
                    position: hit,
                    block_location: BlockLocation::from(current_block).wrap(self.size()),
                    face: hit_face,
                });
            }
        }

        None
    }

    fn step_player_velocity_and_facing(
        &mut self,
        entity_id: EntityId,
        pitch: f32,
        yaw: f32,
        front: MovementSign,
        right: MovementSign,
        jump: bool,
    ) {
        let player_motion = self.entity_motion(entity_id).unwrap().clone();
        let pitch = player_motion.facing().pitch() + PITCH_SPEED * pitch;
        let yaw = player_motion.facing().yaw() + YAW_SPEED * yaw;

        // Find the new front direction from the pitch and the yaw.
        let front_direction = UP
            .apply_rotation(pitch, yaw)
            .horizontal_component()
            .normalize();
        let right_direction = UP.cross(front_direction).horizontal_component().normalize();

        // Find the horizontal velocity from input movement signs and the front direction.
        let horizontal_velocity = if front != MovementSign::Zero || right != MovementSign::Zero {
            (front.as_f64() * front_direction.as_dvec3()
                + right.as_f64() * right_direction.as_dvec3())
            .normalize()
            .horizontal_component()
                * MAX_WALK_SPEED
        } else {
            dvec3(0.0, 0.0, 0.0)
        };

        // Find the vertical velocity from jumping and gravity.
        let jump_velocity = if jump && player_motion.is_on_ground() {
            JUMP_SPEED
        } else {
            0.0
        };
        let vertical_velocity = player_motion.velocity().y + jump_velocity + GRAVITY_ACCEL;

        // Sum and clamp the velocity components.
        let velocity = horizontal_velocity
            .with_y(vertical_velocity)
            .clamp(MIN_VELOCITY, MAX_VELOCITY);

        // Update the velocity of the player.
        let new_player_motion = player_motion
            .with_velocity(velocity)
            .with_facing(EntityFacing::new(pitch, yaw));
        self.set_entity_motion(entity_id, new_player_motion)
            .unwrap();
    }

    pub fn step_entity_updates(&mut self) {
        let mut entity_step_actions = Vec::new();
        let entity_kinds = self.entity_kinds.clone();

        for (&entity_id, &entity_kind) in entity_kinds.iter() {
            entity_kind.step(entity_id, self, |action| {
                entity_step_actions.push(action);
            });
        }

        for action in entity_step_actions.into_iter() {
            match action {
                EntityStepAction::SetHorizontalVelocity {
                    entity_id,
                    velocity,
                } => {
                    let Some(mut motion) = self.entity_motion(entity_id).cloned() else {
                        continue;
                    };
                    motion.velocity.x = velocity.x;
                    motion.velocity.z = velocity.y;
                    self.set_entity_motion(entity_id, motion).unwrap();
                }
                EntityStepAction::SetFacing { entity_id, facing } => {
                    let Some(mut motion) = self.entity_motion(entity_id).cloned() else {
                        continue;
                    };
                    motion.facing = facing;
                    self.set_entity_motion(entity_id, motion).unwrap();
                }
            }
        }
    }

    pub fn step_entity_motions(&mut self) {
        let entity_kinds = self.entity_kinds.clone();
        for (&entity_id, &entity_kind) in entity_kinds.iter() {
            if entity_kind != EntityKind::Player(Player) {
                self.step_entity_gravity(entity_id);
            }
            self.step_entity_motion(entity_id, entity_kind);
        }
    }

    fn step_entity_gravity(&mut self, entity_id: EntityId) {
        let entity_motion = self.entity_motion(entity_id).unwrap().clone();
        let horizontal_velocity = entity_motion.velocity().with_y(0.0);
        let vertical_velocity = entity_motion.velocity().y + GRAVITY_ACCEL;
        let velocity = horizontal_velocity
            .with_y(vertical_velocity)
            .clamp(MIN_VELOCITY, MAX_VELOCITY);
        let new_entity_motion = entity_motion.with_velocity(velocity);
        self.set_entity_motion(entity_id, new_entity_motion)
            .unwrap();
    }

    fn step_entity_motion(&mut self, entity_id: EntityId, entity_kind: EntityKind) {
        // Skip this entity if its containing chunk is not loaded yet.
        let chunk_location = self.entity_position(entity_id).unwrap().chunk_location();
        if self.chunk(chunk_location).is_none() {
            return;
        }

        let motion = self.entity_motion(entity_id).unwrap().clone();
        let EntityMotion {
            position: _,
            velocity,
            bounding_box,
            is_on_ground: _,
            facing: _,
        } = motion;

        // Determine the range of block locations to collide with.
        let locations = {
            let enlarged_box = motion.bounding_box.enlarged_by_displacement(velocity);
            let min_location = enlarged_box.min().as_block_location() - ivec3(1, 1, 1);
            let max_location = enlarged_box.max().as_block_location() + ivec3(1, 1, 1);

            iterate_blocks!(min_location, max_location)
        };

        // Collect the potentially colliding block boxes.
        let colliding_boxes: Vec<_> = self.collect_block_boxes_at_locations(locations);

        // Limit the displacement by avoiding the collision boxes.
        let limited_displacement =
            limit_displacements(velocity, &bounding_box, colliding_boxes.iter());

        // Set the new motion.
        let has_collision = limited_displacement.cmpne(velocity);
        let is_on_ground = velocity.y < 0.0 && has_collision.y;

        let new_velocity = DVec3::select(has_collision, DVec3::ZERO, velocity);
        let new_position = (bounding_box.center() + limited_displacement).wrap(self.size());
        let new_bounding_box = entity_kind.bounding_box_at(new_position);
        let new_motion = motion
            .with_is_on_ground(is_on_ground)
            .with_velocity(new_velocity)
            .with_position(new_position)
            .with_bounding_box(new_bounding_box);
        self.set_entity_motion(entity_id, new_motion).unwrap();
    }

    fn collect_block_boxes_at_locations(
        &mut self,
        locations: impl Iterator<Item = BlockLocation>,
    ) -> Vec<BoundingBox> {
        locations
            .filter_map(|location| -> Option<BoundingBox> {
                let block_kind = self.block_kind(location)?;
                block_kind.bounding_box_at(location)
            })
            .collect()
    }

    fn nearest_entity_from(
        &self,
        center: Position,
        candidate_entity_ids: impl Iterator<Item = EntityId>,
    ) -> Option<EntityId> {
        candidate_entity_ids
            .map(|candidate_id| {
                let position = self.entity_motion(candidate_id).unwrap().position();
                (
                    candidate_id,
                    position.sub_shortest(center, self.size()).length_squared(),
                )
            })
            .min_by(|a, b| a.1.total_cmp(&b.1))
            .map(|(candidate_id, _)| candidate_id)
    }
}

#[derive(Debug)]
pub struct PlayerStepConfig<'s, Sender> {
    pub entity_id: EntityId,
    pub front: MovementSign,
    pub right: MovementSign,
    pub jump: bool,
    pub sneak: bool,
    pub yaw: f32,
    pub pitch: f32,
    pub attack: bool,
    pub dig: bool,
    pub utilize: bool,
    pub sender: &'s Sender,
}

#[derive(Debug)]
pub struct PlayerPartialStepConfig {
    pub entity_id: EntityId,
    pub yaw: f32,
    pub pitch: f32,
}

pub trait SendPlayerMessage {
    fn send(&self, message: ClientMessage) -> Result<()>;
}

pub struct DummySendPlayerMessage;
impl SendPlayerMessage for DummySendPlayerMessage {
    fn send(&self, _message: ClientMessage) -> Result<()> {
        unreachable!()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum MovementSign {
    Zero,
    Pos,
    Neg,
}

impl MovementSign {
    fn as_f64(&self) -> f64 {
        match self {
            MovementSign::Zero => 0.0,
            MovementSign::Pos => 1.0,
            MovementSign::Neg => -1.0,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Chunk {
    location: ChunkLocation,
    segments: Vec<Segment>,
}

// TODO: Add palette-based storage for the block kind array once we need more than 256 kinds.
// The palette-based storage may also, as a side effect, allow non-core block kinds to exist.
//
//   u8 raw data
//   -> (via serialized palette) block identifier string
//   -> (via runtime block kind register) block kind enum object / block behavior instance
//
// The result of the block kind enum object / block behavior instance lookup can be cached in an
// array of Either(BlockKind, Box<dyn BlockBehavior>) to avoid consulting the registry on every block
// access.
/// A vertical segment of a complete chunk.
///
/// This is only for abstracting the underlying storage, and thus is not part of the public
/// interface.
#[derive(Clone)]
struct Segment {
    block_kinds: [BlockKind; 16 * 16 * 16],
}

impl Segment {
    fn new() -> Self {
        Self {
            block_kinds: [BlockKind::default(); 16 * 16 * 16],
        }
    }

    fn set_block_kind(&mut self, segment_location: IVec3, block_kind: BlockKind) {
        self.block_kinds[Self::to_index(segment_location)] = block_kind;
    }

    fn get_block_kind(&self, segment_location: IVec3) -> BlockKind {
        self.block_kinds[Self::to_index(segment_location)]
    }

    fn to_index(segment_location: IVec3) -> usize {
        let IVec3 { x, y, z } = segment_location;
        assert!((0..16).contains(&x));
        assert!((0..16).contains(&y));
        assert!((0..16).contains(&z));
        (y * 16 * 16 + x * 16 + z) as usize
    }
}

impl Serialize for Segment {
    fn serialize<S>(&self, serializer: S) -> std::result::Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let block_kinds_buffer = self.block_kinds.map(|block_kind| block_kind.discriminant());
        serializer.serialize_bytes(&block_kinds_buffer)
    }
}

impl<'de> Deserialize<'de> for Segment {
    fn deserialize<D>(deserializer: D) -> std::result::Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        use serde::de::Error;

        let block_kinds_buffer = bytes::Bytes::deserialize(deserializer)?;
        let block_kinds: [BlockKind; 16 * 16 * 16] = block_kinds_buffer
            .into_iter()
            .map(BlockKind::from_discriminant)
            .collect::<Option<Vec<_>>>()
            .ok_or(D::Error::custom(
                "Some bytes in the buffer do not map to valid block kinds",
            ))?
            .try_into()
            .map_err(|e: Vec<BlockKind>| {
                D::Error::invalid_length(e.len(), &"a sequence of 4096 block kinds")
            })?;
        Ok(Segment { block_kinds })
    }
}

impl std::fmt::Debug for Segment {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let block_count = self
            .block_kinds
            .iter()
            .filter(|&block_kind| *block_kind != BlockKind::from(BlockAir))
            .count();
        write!(f, "Segment {{ block_count: {block_count} }}")
    }
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq, Hash, Default)]
pub struct BlockAir;
impl BlockBehavior for BlockAir {
    fn is_opaque(&self) -> bool {
        false
    }

    fn can_collide(&self) -> bool {
        false
    }

    fn bounding_box_at(&self, _location: BlockLocation) -> Option<BoundingBox> {
        None
    }
}

impl From<BlockAir> for BlockKind {
    fn from(value: BlockAir) -> Self {
        BlockKind::BlockAir(value)
    }
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq, Hash, Default)]
pub struct BlockGrass;
impl BlockBehavior for BlockGrass {
    fn is_opaque(&self) -> bool {
        true
    }
}

impl From<BlockGrass> for BlockKind {
    fn from(value: BlockGrass) -> Self {
        BlockKind::BlockGrass(value)
    }
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq, Hash, Default)]
pub struct BlockStone;
impl BlockBehavior for BlockStone {
    fn is_opaque(&self) -> bool {
        true
    }
}

impl From<BlockStone> for BlockKind {
    fn from(value: BlockStone) -> Self {
        BlockKind::BlockStone(value)
    }
}

#[delegatable_trait]
pub trait BlockBehavior {
    /// Returns true if the block fills the whole volume and does not let light through.
    fn is_opaque(&self) -> bool {
        false
    }

    /// Returns true if the block collides with e.g. player's ray.
    fn can_collide(&self) -> bool {
        true
    }

    fn bounding_box_at(&self, location: BlockLocation) -> Option<BoundingBox> {
        Some(BoundingBox::new(
            location.as_inner().as_dvec3().into(),
            (location.as_inner() + 1).as_dvec3().into(),
        ))
    }
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq, Hash, Default)]
pub struct BlockWater;
impl BlockBehavior for BlockWater {
    fn is_opaque(&self) -> bool {
        false
    }
}

impl From<BlockWater> for BlockKind {
    fn from(_: BlockWater) -> Self {
        BlockKind::BlockWater(BlockWater)
    }
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq, Hash, Default)]
pub struct BlockGlass;
impl BlockBehavior for BlockGlass {
    fn is_opaque(&self) -> bool {
        false
    }
}

impl From<BlockGlass> for BlockKind {
    fn from(_: BlockGlass) -> Self {
        BlockKind::BlockGlass(BlockGlass)
    }
}

impl Chunk {
    pub fn new(location: ChunkLocation) -> Self {
        let mut chunk = Self {
            location,
            segments: (0..16).map(|_i| Segment::new()).collect(),
        };
        chunk.fill_block_ids();
        chunk
    }

    pub fn location(&self) -> ChunkLocation {
        self.location
    }

    fn fill_block_ids(&mut self) {
        let noise_function = noise::ScalePoint::new(noise::Add::new(
            noise::Constant::new(TERRAIN_BASE_HEIGHT),
            noise::Multiply::new(
                noise::Constant::new(TERRAIN_AMPLITUDE),
                noise::Fbm::<noise::Simplex>::new(TERRAIN_SEED)
                    .set_frequency(1.0)
                    .set_persistence(0.5)
                    .set_lacunarity(2.1)
                    .set_octaves(TERRAIN_OCTAVES),
            ),
        ))
        .set_scale(TERRAIN_SCALE);

        for x in 0..16 {
            for z in 0..16 {
                let global_noise_location = ivec2(x, z) + self.location.as_inner() * 16;
                let noise_height = noise_function
                    .get(*global_noise_location.as_dvec2().as_ref())
                    .floor() as i32;
                assert!(noise_height < 256);
                assert!(noise_height >= 0);
                for y in 0..noise_height {
                    let local_location = LocalBlockLocation::new(x, y, z);
                    let block_kind: BlockKind = if y >= noise_height - 3 {
                        BlockGrass.into()
                    } else {
                        BlockStone.into()
                    };
                    self.set_block_kind(local_location, block_kind)
                }
            }
        }
    }

    pub fn block_kind(&self, local_location: LocalBlockLocation) -> BlockKind {
        if local_location.y() >= 256 || local_location.y() < 0 {
            BlockAir.into()
        } else {
            let s = local_location.y().div_euclid(16) as usize;
            let sy = local_location.y().rem_euclid(16);
            let segment_location = ivec3(local_location.x(), sy, local_location.z());
            self.segments[s].get_block_kind(segment_location)
        }
    }

    pub fn set_block_kind(
        &mut self,
        local_location: LocalBlockLocation,
        block_kind: impl Into<BlockKind>,
    ) {
        assert!(local_location.is_legal(), "Out of bound local location");

        let s = local_location.y().div_euclid(16) as usize;
        let sy = local_location.y().rem_euclid(16);
        let segment_location = ivec3(local_location.x(), sy, local_location.z());
        self.segments[s].set_block_kind(segment_location, block_kind.into());
    }
}

#[derive(Delegate)]
#[delegate(BlockBehavior)]
#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq, Hash, EnumIter)]
#[repr(u8)]
pub enum BlockKind {
    BlockAir(BlockAir),
    BlockGrass(BlockGrass),
    BlockStone(BlockStone),
    BlockWater(BlockWater),
    BlockGlass(BlockGlass),
}

impl BlockKind {
    pub fn iter() -> impl Iterator<Item = Self> {
        <Self as IntoEnumIterator>::iter()
    }

    pub fn discriminant(&self) -> u8 {
        // SAFETY: Because `Self` is marked `repr(u8)`, its layout is a `repr(C)` `union`
        // between `repr(C)` structs, each of which has the `u8` discriminant as its first
        // field, so we can read the discriminant without offsetting the pointer.
        unsafe { *<*const _>::from(self).cast::<u8>() }
    }

    pub fn from_discriminant(discriminant: u8) -> Option<Self> {
        Some(match discriminant {
            0 => BlockAir.into(),
            1 => BlockGrass.into(),
            2 => BlockStone.into(),
            3 => BlockWater.into(),
            4 => BlockGlass.into(),
            _ => return None,
        })
    }
}

impl Default for BlockKind {
    fn default() -> Self {
        BlockAir.into()
    }
}

pub enum EntityStepAction {
    SetHorizontalVelocity {
        entity_id: EntityId,
        velocity: DVec2,
    },
    SetFacing {
        entity_id: EntityId,
        facing: EntityFacing,
    },
}

#[delegatable_trait]
pub trait EntityBehavior {
    fn bounding_box_at(&self, position: Position) -> BoundingBox {
        BoundingBox::from_center_and_size(position, dvec3(1.0, 1.0, 1.0))
    }

    fn step(
        &self,
        _entity_id: EntityId,
        _world: &World,
        _add_entity_step_action: impl FnMut(EntityStepAction),
    ) {
    }
}

#[derive(Delegate, Clone, Copy, Debug, Serialize, Deserialize, Eq, PartialEq, Hash, EnumIter)]
#[delegate(EntityBehavior)]
pub enum EntityKind {
    Player(Player),
    DummyEntity(DummyEntity),
    Aurochs(Aurochs),
    Deer(Deer),
}

impl EntityKind {
    pub fn iter() -> impl Iterator<Item = Self> {
        <Self as IntoEnumIterator>::iter()
    }
}

#[derive(Default, Debug, Clone, Copy, Serialize, Deserialize, Eq, PartialEq, Hash)]
pub struct Player;
impl EntityBehavior for Player {
    fn bounding_box_at(&self, position: Position) -> BoundingBox {
        BoundingBox::from_center_and_size(position, dvec3(0.8, 1.8, 0.8))
    }
}

impl Player {
    const BOX_SIZE: DVec3 = dvec3(0.8, 1.8, 0.8);
    const EYE_Y_OFFSET: f64 = 1.6 - (Self::BOX_SIZE.y * 0.5);

    pub fn eye_position_at(position: Position) -> Position {
        Position::new(
            position.x(),
            position.y() + Self::EYE_Y_OFFSET,
            position.z(),
        )
    }
}

#[derive(Default, Debug, Clone, Copy, Serialize, Deserialize, Eq, PartialEq, Hash)]
pub struct DummyEntity;
impl EntityBehavior for DummyEntity {
    fn bounding_box_at(&self, position: Position) -> BoundingBox {
        BoundingBox::from_center_and_size(position, dvec3(1.2, 2.5, 1.2))
    }

    fn step(
        &self,
        entity_id: EntityId,
        world: &World,
        mut add_entity_step_action: impl FnMut(EntityStepAction),
    ) {
        let motion = world.entity_motion(entity_id).unwrap();
        let position = motion.position();
        let facing = motion.facing();

        let detect_box = BoundingBox::from_center_and_size(position, dvec3(64.0, 64.0, 64.0));

        // Filter out the entity itself and get the nearest player entity.
        let candidate_entity_ids =
            world
                .entities_in_bounding_box(detect_box)
                .filter(|&candidate_id| {
                    let kind = *world.entity_kinds.get(&candidate_id).unwrap();
                    candidate_id != entity_id && kind == EntityKind::Player(Player)
                });
        let Some(target_id) = world.nearest_entity_from(position, candidate_entity_ids) else {
            return;
        };

        // Set the horizontal velocity towards the target entity with speed of 1 block per second.
        let target_position = world
            .entity_motion(target_id)
            .unwrap()
            .position()
            .unwrap_near(position, world.size());
        let horizontal_velocity = (target_position - position).xz().normalize() * 0.05;

        // Rotate by a fixed speed.
        let new_facing = EntityFacing::new(facing.pitch(), facing.yaw() + 0.05 * 0.1);
        info!("new_facing = {new_facing:?}");

        add_entity_step_action(EntityStepAction::SetHorizontalVelocity {
            velocity: horizontal_velocity,
            entity_id,
        });
        add_entity_step_action(EntityStepAction::SetFacing {
            entity_id,
            facing: new_facing,
        });
    }
}

#[derive(Default, Debug, Clone, Copy, Serialize, Deserialize, Eq, PartialEq, Hash)]
pub struct Aurochs;

impl EntityBehavior for Aurochs {
    fn bounding_box_at(&self, position: Position) -> BoundingBox {
        BoundingBox::from_center_and_size(position, dvec3(1.1, 1.1, 1.1))
    }

    fn step(
        &self,
        entity_id: EntityId,
        world: &World,
        _add_entity_step_action: impl FnMut(EntityStepAction),
    ) {
        let _motion = world.entity_motion(entity_id).unwrap();
    }
}

#[derive(Default, Debug, Clone, Copy, Serialize, Deserialize, Eq, PartialEq, Hash)]
pub struct Deer;

impl Deer {
    pub const BOX_HEIGHT: f64 = 2.0;
}

impl EntityBehavior for Deer {
    fn bounding_box_at(&self, position: Position) -> BoundingBox {
        BoundingBox::from_center_and_size(position, dvec3(1.0, Self::BOX_HEIGHT, 1.0))
    }

    fn step(
        &self,
        _entity_id: EntityId,
        _world: &World,
        _add_entity_step_action: impl FnMut(EntityStepAction),
    ) {
    }
}

fn limit_displacements<'a>(
    displacement: DVec3,
    bounding_box: &BoundingBox,
    colliding_boxes: impl Iterator<Item = &'a BoundingBox> + Clone,
) -> DVec3 {
    let bounding_box = *bounding_box;
    let yd = limit_displacement::<1>(displacement.y, &bounding_box, colliding_boxes.clone());

    let bounding_box = bounding_box.offset(dvec3(0.0, yd, 0.0));
    let xd = limit_displacement::<0>(displacement.x, &bounding_box, colliding_boxes.clone());

    let bounding_box = bounding_box.offset(dvec3(xd, 0.0, 0.0));
    let zd = limit_displacement::<2>(displacement.z, &bounding_box, colliding_boxes);

    dvec3(xd, yd, zd)
}

pub struct WorldStepConfig<'s, Sender> {
    pub player_config: Option<PlayerStepConfig<'s, Sender>>,
    pub should_update_entities: bool,
}

pub struct WorldPartialStepConfig {
    pub player_config: Option<PlayerPartialStepConfig>,
}

fn limit_displacement<'a, const DIM: usize>(
    displacement: f64,
    bounding_box: &BoundingBox,
    colliding_boxes: impl Iterator<Item = &'a BoundingBox>,
) -> f64 {
    let limited_displacements = colliding_boxes
        .map(|colliding_box| bounding_box.limit_displacement::<DIM>(displacement, colliding_box));
    if displacement > 0.0 {
        limited_displacements
            .min_by(|a, b| a.total_cmp(b))
            .unwrap_or(displacement)
    } else {
        limited_displacements
            .max_by(|a, b| a.total_cmp(b))
            .unwrap_or(displacement)
    }
}

pub fn chunk_location_quadrant(chunk_location: IVec2) -> BVec2 {
    chunk_location.cmpge(ivec2(0, 0))
}

#[derive(Debug)]
struct BlockHit {
    #[allow(dead_code)]
    position: DVec3,
    block_location: BlockLocation,
    face: Face,
}

#[cfg(test)]
mod tests {
    use itertools::Itertools;

    use super::*;

    #[test]
    fn test_entities_in_bounding_box() {
        let mut world = World::new(WorldSize::from_radius_in_chunks(64));
        let player_id =
            world.create_player(Position::new(2040.0, 50.0, 2040.0), dvec3(0.0, 0.0, 0.0));
        println!("Entity segments: {:?}", world.entity_segments);

        // This bounding box contains the player position,
        // so the function should return the player entity.
        let entities = world
            .entities_in_bounding_box(BoundingBox::new(
                Position::new(-31.0, 0.0, 1980.0),
                Position::new(32.0, 80.0, 2044.0),
            ))
            .collect_vec();
        assert!(
            entities.contains(&player_id),
            "Entities in bounding box does not contain the player: {entities:?}"
        );
    }

    #[test]
    fn test_block_kind_discriminant_roundtrip() {
        for kind in BlockKind::iter() {
            assert_eq!(BlockKind::from_discriminant(kind.discriminant()), Some(kind),
                "BlockKind {kind:?} does not survive a from_discriminant(discriminant()) roundtrip."
            );
        }
    }
}
