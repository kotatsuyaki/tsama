use glam::{dvec3, vec3, DVec3, Vec3};

use crate::types::Position;

pub const VISIBLE_DISTANCE: i32 = 12;

// Movement configs
pub const UP: Vec3 = vec3(0.0, 1.0, 0.0);
pub const MAX_WALK_SPEED: f64 = 6.0 / 20.0;
pub const GRAVITY_ACCEL: f64 = -10.0 / 400.0;
pub const PITCH_SPEED: f32 = 0.05 / 20.0;
pub const YAW_SPEED: f32 = 0.05 / 20.0;
pub const JUMP_SPEED: f64 = 6.0 / 20.0;
pub const MIN_VELOCITY: DVec3 = dvec3(-10.0 / 20.0, -10.0 / 20.0, -10.0 / 20.0);
pub const MAX_VELOCITY: DVec3 = dvec3(10.0 / 20.0, 10.0 / 20.0, 10.0 / 20.0);

// Terrain generation
pub const INITIAL_PLAYER_POSITION: Position = Position::new(0.5, 150.0, 4.6);
pub const TERRAIN_BASE_HEIGHT: f64 = 130.0;
pub const TERRAIN_AMPLITUDE: f64 = 30.0;
pub const TERRAIN_OCTAVES: usize = 9;
pub const TERRAIN_SEED: u32 = 147;
pub const TERRAIN_SCALE: f64 = 1.0 / 256.0;
