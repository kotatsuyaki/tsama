use std::ops::{Add, AddAssign, Index, Sub, SubAssign};

use enum_map::Enum;
use glam::{dvec2, dvec3, ivec2, ivec3, DVec2, DVec3, IVec2, IVec3, Vec3, Vec3Swizzles};
use itertools::iproduct;
use serde::{Deserialize, Serialize};
use strum::{EnumIter, IntoEnumIterator};

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub struct WorldSize {
    radius: i32,
}

impl WorldSize {
    pub fn from_radius_in_chunks(radius: i32) -> Self {
        Self { radius }
    }

    pub fn radius_in_chunks(&self) -> i32 {
        self.radius
    }

    pub fn diameter_in_chunks(&self) -> i32 {
        self.radius * 2
    }

    pub fn radius_in_blocks(&self) -> i32 {
        self.radius * 16
    }

    pub fn diameter_in_blocks(&self) -> i32 {
        self.radius * 16 * 2
    }

    pub(crate) fn contains_chunk(&self, location: ChunkLocation) -> bool {
        (0..self.diameter_in_chunks()).contains(&location.x())
            && (0..self.diameter_in_chunks()).contains(&location.z())
    }

    pub(crate) fn contains_block(&self, location: BlockLocation) -> bool {
        (0..256).contains(&location.y())
            && (0..self.diameter_in_blocks()).contains(&location.x())
            && (0..self.diameter_in_blocks()).contains(&location.z())
    }

    fn wrap_chunk(&self, original: i32) -> i32 {
        original.rem_euclid(self.diameter_in_chunks())
    }

    fn unwrap_chunk_near(&self, original: i32, target: i32) -> i32 {
        [
            original - self.diameter_in_chunks(),
            original,
            original + self.diameter_in_chunks(),
        ]
        .into_iter()
        .min_by_key(|&coordinate| (target - coordinate).abs())
        .unwrap()
    }

    fn wrap_position(&self, original: f64) -> f64 {
        let d = self.diameter_in_blocks() as f64;
        original.rem_euclid(d)
    }

    fn unwrap_position_near(&self, original: f64, target: f64) -> f64 {
        let r = self.radius_in_blocks() as f64;
        let d = self.diameter_in_blocks() as f64;
        if target - original > r {
            original + d
        } else if target - original < -r {
            original - d
        } else {
            original
        }
    }

    fn wrap_location(&self, original: i32) -> i32 {
        let d = self.diameter_in_blocks();
        original.rem_euclid(d)
    }
}

/* Coordinate types */

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq, Hash)]
#[serde(transparent)]
pub struct ChunkLocation {
    inner: IVec2,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq, Hash)]
#[serde(transparent)]
pub struct SegmentLocation {
    inner: IVec3,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq, Hash)]
#[serde(transparent)]
pub struct BlockLocation {
    inner: IVec3,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq, Hash)]
#[serde(transparent)]
pub struct LocalBlockLocation {
    inner: IVec3,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
#[serde(transparent)]
pub struct Position {
    inner: DVec3,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
#[serde(transparent)]
pub struct HorizontalPosition {
    inner: DVec2,
}

impl ChunkLocation {
    pub fn new(x: i32, z: i32) -> Self {
        Self { inner: ivec2(x, z) }
    }

    pub fn x(&self) -> i32 {
        self.inner.x
    }

    pub fn z(&self) -> i32 {
        self.inner.y
    }

    pub fn offset(&self, sx: i32, sz: i32) -> Self {
        Self {
            inner: ivec2(self.x() + sx, self.z() + sz),
        }
    }

    pub fn segment_locations(&self) -> impl Iterator<Item = SegmentLocation> + '_ {
        (0..16).map(|y| SegmentLocation::new(self.x(), y, self.z()))
    }

    pub fn distance_squared(&self, other: Self) -> i32 {
        self.inner.distance_squared(other.inner)
    }

    pub fn distance_squared_shortest(&self, other: Self, size: WorldSize) -> i32 {
        self.distance_squared(other.unwrap_near(*self, size))
    }

    pub fn wrap(&self, size: WorldSize) -> Self {
        Self::new(size.wrap_chunk(self.x()), size.wrap_chunk(self.z()))
    }

    pub fn unwrap_near(&self, target: Self, size: WorldSize) -> Self {
        let x = size.unwrap_chunk_near(self.x(), target.x());
        let z = size.unwrap_chunk_near(self.z(), target.z());
        Self::new(x, z)
    }

    pub fn as_inner(&self) -> IVec2 {
        self.inner
    }
}

impl SegmentLocation {
    pub fn new(x: i32, y: i32, z: i32) -> Self {
        Self {
            inner: ivec3(x, y, z),
        }
    }

    pub fn x(&self) -> i32 {
        self.inner.x
    }

    pub fn y(&self) -> i32 {
        self.inner.y
    }

    pub fn z(&self) -> i32 {
        self.inner.z
    }

    pub fn global_and_local_block_locations(
        &self,
    ) -> impl Iterator<Item = (BlockLocation, LocalBlockLocation)> + '_ {
        let y_range = (self.y() * 16)..(self.y() * 16 + 16);
        iproduct!(y_range, 0..16, 0..16).map(|(y, x, z)| {
            (
                BlockLocation::new(x + self.x() * 16, y, z + self.z() * 16),
                LocalBlockLocation::new(x, y, z),
            )
        })
    }

    pub fn chunk_location(&self) -> ChunkLocation {
        ChunkLocation::new(self.x(), self.z())
    }

    pub fn from_position(position: DVec3) -> Self {
        position
            .floor()
            .as_ivec3()
            .div_euclid(ivec3(16, 16, 16))
            .into()
    }

    pub fn wrap(&self, size: WorldSize) -> Self {
        let x = size.wrap_chunk(self.x());
        let z = size.wrap_chunk(self.z());
        Self::new(x, self.y(), z)
    }

    pub fn unwrap_near(&self, target: Self, size: WorldSize) -> Self {
        let x = size.unwrap_chunk_near(self.x(), target.x());
        let z = size.unwrap_chunk_near(self.z(), target.z());
        Self::new(x, self.y(), z)
    }

    pub fn distance_squared(&self, other: Self) -> i32 {
        self.inner.distance_squared(other.inner)
    }

    pub fn distance_squared_shortest(&self, other: Self, size: WorldSize) -> i32 {
        self.distance_squared(other.unwrap_near(*self, size))
    }

    pub fn as_inner(&self) -> IVec3 {
        self.inner
    }

    pub fn min_block_location(&self) -> BlockLocation {
        BlockLocation::from(self.as_inner() * 16)
    }
}

impl BlockLocation {
    pub fn new(x: i32, y: i32, z: i32) -> Self {
        Self {
            inner: ivec3(x, y, z),
        }
    }

    pub fn x(&self) -> i32 {
        self.inner.x
    }

    pub fn y(&self) -> i32 {
        self.inner.y
    }

    pub fn z(&self) -> i32 {
        self.inner.z
    }

    pub fn chunk_location(&self) -> ChunkLocation {
        self.inner.xz().div_euclid(ivec2(16, 16)).into()
    }

    pub fn segment_location(&self) -> SegmentLocation {
        self.inner.div_euclid(ivec3(16, 16, 16)).into()
    }

    pub fn local_location(&self) -> LocalBlockLocation {
        let [x, z] = self.inner.xz().rem_euclid(ivec2(16, 16)).to_array();
        LocalBlockLocation::new(x, self.y(), z)
    }

    pub fn decompose(&self) -> (ChunkLocation, LocalBlockLocation) {
        (self.chunk_location(), self.local_location())
    }

    pub fn wrap(&self, size: WorldSize) -> Self {
        Self::new(
            size.wrap_location(self.x()),
            size.wrap_location(self.y()),
            size.wrap_location(self.z()),
        )
    }

    pub(crate) fn as_inner(&self) -> IVec3 {
        self.inner
    }
}

impl LocalBlockLocation {
    pub fn new(x: i32, y: i32, z: i32) -> Self {
        Self {
            inner: ivec3(x, y, z),
        }
    }

    pub fn x(&self) -> i32 {
        self.inner.x
    }

    pub fn y(&self) -> i32 {
        self.inner.y
    }

    pub fn z(&self) -> i32 {
        self.inner.z
    }

    pub fn is_legal(&self) -> bool {
        self.inner.cmpge(IVec3::ZERO).all() && self.inner.cmplt(ivec3(16, 256, 16)).all()
    }

    #[allow(dead_code)]
    pub(crate) fn as_inner(&self) -> IVec3 {
        self.inner
    }
}

impl Position {
    pub const fn new(x: f64, y: f64, z: f64) -> Self {
        Self {
            inner: dvec3(x, y, z),
        }
    }

    pub fn x(&self) -> f64 {
        self.inner.x
    }

    pub fn y(&self) -> f64 {
        self.inner.y
    }

    pub fn z(&self) -> f64 {
        self.inner.z
    }

    pub fn le(&self, other: Self) -> bool {
        self.inner.cmple(other.inner).all()
    }

    pub fn chunk_location(&self) -> ChunkLocation {
        self.inner
            .xz()
            .floor()
            .as_ivec2()
            .div_euclid(ivec2(16, 16))
            .into()
    }

    pub fn segment_location(&self) -> SegmentLocation {
        self.inner
            .floor()
            .as_ivec3()
            .div_euclid(ivec3(16, 16, 16))
            .into()
    }

    pub fn wrap(&self, size: WorldSize) -> Self {
        let x = size.wrap_position(self.x());
        let z = size.wrap_position(self.z());
        Self::new(x, self.y(), z)
    }

    pub fn unwrap_near(&self, other: Position, size: WorldSize) -> Self {
        let x = size.unwrap_position_near(self.x(), other.x());
        let z = size.unwrap_position_near(self.z(), other.z());
        Self::new(x, self.y(), z)
    }

    /// Interpolates from `self` to `other` with `elapsed` frames elapsed.
    ///
    /// This method interpolates the shortest path between the two positions in the wrapped world.
    /// The returned position is wrapped within the world size.
    pub fn interpolate(&self, other: Position, elapsed: f64, size: WorldSize) -> Self {
        let other = other.unwrap_near(*self, size);
        let interpolated: Self = self.inner.lerp(other.inner, elapsed).into();
        interpolated.wrap(size)
    }

    /// Finds the shortest value of `self - other`.
    pub fn sub_shortest(&self, other: Position, size: WorldSize) -> DVec3 {
        let self_unwrapped = self.unwrap_near(other, size);
        self_unwrapped.inner - other.inner
    }

    pub fn as_inner(&self) -> DVec3 {
        self.inner
    }

    pub fn as_block_location(&self) -> BlockLocation {
        self.inner.floor().as_ivec3().into()
    }
}

impl HorizontalPosition {
    pub const fn new(x: f64, z: f64) -> Self {
        Self { inner: dvec2(x, z) }
    }

    pub fn x(&self) -> f64 {
        self.inner.x
    }

    pub fn z(&self) -> f64 {
        self.inner.y
    }

    pub fn wrap(&self, size: WorldSize) -> Self {
        let x = size.wrap_position(self.x());
        let z = size.wrap_position(self.z());
        Self::new(x, z)
    }

    pub fn unwrap_near(&self, other: Self, size: WorldSize) -> Self {
        let x = size.unwrap_position_near(self.x(), other.x());
        let z = size.unwrap_position_near(self.z(), other.z());
        Self::new(x, z)
    }

    pub fn as_inner(&self) -> DVec2 {
        self.inner
    }
}

impl From<IVec2> for ChunkLocation {
    fn from(inner: IVec2) -> Self {
        Self { inner }
    }
}

impl From<IVec3> for SegmentLocation {
    fn from(inner: IVec3) -> Self {
        Self { inner }
    }
}

impl From<IVec3> for BlockLocation {
    fn from(inner: IVec3) -> Self {
        Self { inner }
    }
}

impl Sub<IVec3> for BlockLocation {
    type Output = BlockLocation;

    fn sub(self, rhs: IVec3) -> Self::Output {
        (self.inner - rhs).into()
    }
}

impl Add<IVec3> for BlockLocation {
    type Output = BlockLocation;

    fn add(self, rhs: IVec3) -> Self::Output {
        (self.inner + rhs).into()
    }
}

impl From<DVec3> for Position {
    fn from(inner: DVec3) -> Self {
        Self { inner }
    }
}

impl From<DVec2> for HorizontalPosition {
    fn from(inner: DVec2) -> Self {
        Self { inner }
    }
}

impl Sub<Position> for Position {
    type Output = DVec3;

    fn sub(self, rhs: Position) -> Self::Output {
        self.inner - rhs.inner
    }
}

impl Sub<DVec3> for Position {
    type Output = Position;

    fn sub(self, rhs: DVec3) -> Self::Output {
        (self.inner - rhs).into()
    }
}

impl SubAssign<DVec3> for Position {
    fn sub_assign(&mut self, rhs: DVec3) {
        self.inner -= rhs;
    }
}

impl Add<DVec3> for Position {
    type Output = Position;

    fn add(self, rhs: DVec3) -> Self::Output {
        (self.inner + rhs).into()
    }
}

impl AddAssign<DVec3> for Position {
    fn add_assign(&mut self, rhs: DVec3) {
        self.inner += rhs
    }
}

impl Index<usize> for Position {
    type Output = f64;

    fn index(&self, index: usize) -> &Self::Output {
        &self.inner[index]
    }
}

impl std::fmt::Display for ChunkLocation {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "ChunkLocation {{ x: {}, z: {} }}", self.x(), self.z())
    }
}

impl std::fmt::Display for SegmentLocation {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "SegmentLocation {{ x: {}, y: {}, z: {} }}",
            self.x(),
            self.y(),
            self.z(),
        )
    }
}

impl std::fmt::Display for Position {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Position {{ x: {:.03}, y: {:.03}, z: {:.03} }}",
            self.x(),
            self.y(),
            self.z(),
        )
    }
}

/// Creates an iterator for all the segment locations between `min` and `max` (inclusive).
#[macro_export]
macro_rules! iterate_segments {
    ($min: expr, $max: expr) => {
        ::itertools::iproduct!(
            $min.x()..=$max.x(),
            $min.y()..=$max.y(),
            $min.z()..=$max.z()
        )
        .map(|(x, y, z)| SegmentLocation::new(x, y, z))
    };
}

/// Creates an iterator for all the block locations between `min` and `max` (inclusive).
#[macro_export]
macro_rules! iterate_blocks {
    ($min: expr, $max: expr) => {
        ::itertools::iproduct!(
            $min.x()..=$max.x(),
            $min.y()..=$max.y(),
            $min.z()..=$max.z()
        )
        .map(|(x, y, z)| BlockLocation::new(x, y, z))
    };
}

/* Box and face types */

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
pub struct BoundingBox {
    min: Position,
    max: Position,
}

impl BoundingBox {
    pub fn new(min: Position, max: Position) -> Self {
        assert!(
            min.le(max),
            "BoundingBox::new expects min <= max but got {min} > {max}
; if this is intended, use BoundingBox::new_unordered instead"
        );
        Self { min, max }
    }

    pub fn new_unordered(pos_a: Position, pos_b: Position) -> Self {
        let pos_a = pos_a.as_inner();
        let pos_b = pos_b.as_inner();
        let min = DVec3::select(pos_a.cmple(pos_b), pos_a, pos_b);
        let max = DVec3::select(pos_a.cmpgt(pos_b), pos_a, pos_b);
        Self::new(min.into(), max.into())
    }

    pub fn from_center_and_size(center: Position, size: DVec3) -> Self {
        Self {
            min: center - size * 0.5,
            max: center + size * 0.5,
        }
    }

    pub fn center(&self) -> Position {
        self.min + (self.max - self.min) * 0.5
    }

    pub fn enlarged(&self, size: DVec3) -> Self {
        let min = self.min - size;
        let max = self.max + size;
        Self::new(min, max)
    }

    /// Enlarges the `self` to contain `self` translated by `displacement`.
    pub fn enlarged_by_displacement(&self, displacement: DVec3) -> Self {
        let max =
            self.max + DVec3::select(displacement.cmpge(DVec3::ZERO), displacement, DVec3::ZERO);
        let min =
            self.min + DVec3::select(displacement.cmpge(DVec3::ZERO), DVec3::ZERO, displacement);

        Self::new(min, max)
    }

    /// Limits `displacement` to be applied on `self` to a value with smaller magnitude so that
    /// `self` does not bump into `other`.
    ///
    /// The `DIM` const generic parameter has to be specified.  Values of `0`, `1`, and `2`
    /// corresponds to displacement in the X, Y, and Z axes, respectively.
    pub fn limit_displacement<const DIM: usize>(
        &self,
        displacement: f64,
        other: &BoundingBox,
    ) -> f64 {
        assert!(DIM < 3);
        if self.overlaps_looking_from::<DIM>(other) {
            if displacement > 0.0 && self.max[DIM] <= other.min[DIM] {
                (other.min[DIM] - self.max[DIM]).min(displacement)
            } else if displacement < 0.0 && self.min[DIM] >= other.max[DIM] {
                (other.max[DIM] - self.min[DIM]).max(displacement)
            } else {
                displacement
            }
        } else {
            displacement
        }
    }

    /// Determines if `self` overlaps with `other` when looked from the direction of `DIM`.
    ///
    /// The `DIM` const generic parameter has to be specified.  Values of `0`, `1`, and `2`
    /// corresponds to displacement in the X, Y, and Z axes, respectively.
    fn overlaps_looking_from<const DIM: usize>(&self, other: &BoundingBox) -> bool {
        assert!(DIM < 3);
        // The OR expressions are there so that only two of the three dimensions will be tested.
        (DIM == 0 || (self.max.x() > other.min.x() && self.min.x() < other.max.x()))
            && (DIM == 1 || (self.max.y() > other.min.y() && self.min.y() < other.max.y()))
            && (DIM == 2 || (self.max.z() > other.min.z() && self.min.z() < other.max.z()))
    }

    pub fn overlaps_with(&self, other: &BoundingBox) -> bool {
        (self.max.x() >= other.min.x() && self.min.x() <= other.max.x())
            && (self.max.y() >= other.min.y() && self.min.y() <= other.max.y())
            && (self.max.z() >= other.min.z() && self.min.z() <= other.max.z())
    }

    pub fn offset(&self, offset: DVec3) -> Self {
        Self {
            min: self.min + offset,
            max: self.max + offset,
        }
    }

    pub fn wrap(&self, size: WorldSize) -> Self {
        let wrapped_center = self.center().wrap(size);
        let offset = wrapped_center - self.center();
        Self::new(self.min() + offset, self.max() + offset)
    }

    pub fn unwrap_near(self, position: Position, size: WorldSize) -> Self {
        let offset = self.center().unwrap_near(position, size) - self.center();
        self + offset
    }

    pub fn min(&self) -> Position {
        self.min
    }

    pub fn max(&self) -> Position {
        self.max
    }
}

impl std::ops::Add<DVec3> for BoundingBox {
    type Output = BoundingBox;

    fn add(self, rhs: DVec3) -> Self::Output {
        self.offset(rhs)
    }
}

impl std::ops::Sub<DVec3> for BoundingBox {
    type Output = BoundingBox;

    fn sub(self, rhs: DVec3) -> Self::Output {
        self.offset(-rhs)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Enum, EnumIter)]
pub enum Face {
    PosX,
    NegX,
    PosY,
    NegY,
    PosZ,
    NegZ,
}

impl Face {
    pub fn iter() -> impl Iterator<Item = Self> {
        <Self as IntoEnumIterator>::iter()
    }

    pub fn to_offset(&self) -> IVec3 {
        match self {
            Face::PosX => ivec3(1, 0, 0),
            Face::NegX => ivec3(-1, 0, 0),
            Face::PosY => ivec3(0, 1, 0),
            Face::NegY => ivec3(0, -1, 0),
            Face::PosZ => ivec3(0, 0, 1),
            Face::NegZ => ivec3(0, 0, -1),
        }
    }
}

pub struct Quad {
    points: [Vec3; 4],
}

impl Quad {
    pub fn new(points: [Vec3; 4]) -> Self {
        Self { points }
    }
}

impl Add<Vec3> for Quad {
    type Output = Quad;

    fn add(self, rhs: Vec3) -> Self::Output {
        Self {
            points: self.points.map(|p| p + rhs),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_wrap_chunk_location() {
        let size = WorldSize::from_radius_in_chunks(64);

        // (before wrap, after wrap) pairs
        let cases = [
            (ChunkLocation::new(127, 127), ChunkLocation::new(127, 127)),
            (ChunkLocation::new(128, 127), ChunkLocation::new(0, 127)),
            (ChunkLocation::new(0, 0), ChunkLocation::new(0, 0)),
            (ChunkLocation::new(-1, 0), ChunkLocation::new(127, 0)),
        ];

        for (input, expected) in cases {
            let output = input.wrap(size);
            assert_eq!(output, expected.into());
        }
    }

    #[test]
    fn test_wrap_position() {
        let size = WorldSize::from_radius_in_chunks(64);

        // (before wrap, after wrap) pairs
        let cases = [
            (
                Position::new(127.0 * 16.0, 50.0, 127.0 * 16.0),
                Position::new(127.0 * 16.0, 50.0, 127.0 * 16.0),
            ),
            (
                Position::new(128.0 * 16.0, 50.0, 127.0 * 16.0),
                Position::new(0.0 * 16.0, 50.0, 127.0 * 16.0),
            ),
            (
                Position::new(0.0 * 16.0, 100000.0, 0.0 * 16.0),
                Position::new(0.0 * 16.0, 100000.0, 0.0 * 16.0),
            ),
            (
                Position::new(-1.0 * 16.0, -100000.0, 0.0 * 16.0),
                Position::new(127.0 * 16.0, -100000.0, 0.0 * 16.0),
            ),
        ];

        for (input, expected) in cases {
            let output = input.wrap(size);
            assert_eq!(output, expected);
        }
    }
}
